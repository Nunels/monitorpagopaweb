function setFiltri(id,value) {
	
	//var prefix = id.substring(0,id.indexOf(":"));

	jQuery('input[id*="'+id+':filter"]').val(value);

}

function setPattern(pattern){
	
	 if(pattern=='yyyy'){
	  jQuery('.ui-datepicker-prev').css({  visibility: "hidden" });
	  jQuery('.ui-datepicker-next').css({  visibility: "hidden" });
	  jQuery('.ui-datepicker-month').css({ visibility: "hidden" });
	  jQuery('.ui-datepicker-calendar').css({  visibility: "hidden" });
	 }else if(pattern=='MM/yyyy'){
		 jQuery('.ui-datepicker-prev').css({  visibility: "hidden" });
		 jQuery('.ui-datepicker-next').css({  visibility: "hidden" });
		 jQuery('.ui-datepicker-month').css({   visibility: "visible" });
		 jQuery('.ui-datepicker-calendar').css({   visibility: "hidden" });
		
	 }else{
		 jQuery('.ui-datepicker-prev').css({  visibility: "visible" });
		 jQuery('.ui-datepicker-next').css({  visibility: "visible" });
		 jQuery('.ui-datepicker-month').css({  visibility: "visible" });
		  jQuery('.ui-datepicker-calendar').css({ visibility: "visible" });
	 }

}





function orderedWidget() {
	var orderedWidget = [ [] ];
	var columns = jQuery(".ui-dashboard-column.ui-sortable");

	for (var index = 0; index < columns.length; ++index) {
		console.log(index);
		var value = columns[index];

		var divEle = value.childNodes;

		for (var i = 0; i < divEle.length; i++) {

			//  console.log("iddddddddddddddddddddddddd "+divEle[i].id);
			var id = divEle[i].id;

			id = id.substring(id.indexOf('idPanel-')).replace('idPanel-','widChart-');

			var widget = PrimeFaces.widgets[id];

			if (widget && widget.widgetVar
					&& widget.widgetVar.includes('widChart')) {
				//console.log(widget);
				orderedWidget[index].push(widget);

			}

		}

	}

	return orderedWidget;

}

function exportMultipleChartContent(w) {

	var arr = [];
	var len = w.length;
	for (var i = 0; i < len; i++) {

		var a = {
			image : jQuery(w[i].exportAsImage()).attr('src'),
			width : 520
		}

		arr.push(a);
	}

	return arr;

}

function exportChartInPDF() {
	//orderedWidgetId();
	//var w = getWidgetVarById();

	var w = orderedWidget()[0];
	var docDefinition = {
		content : exportMultipleChartContent(w)
	}

	console.log($("dynamic_dashboard"));
	//	pdfMake.createPdf(docDefinition).open();
	pdfMake.createPdf(docDefinition).download('primefaces-charts.pdf');
}
	   	