Versione = "07/02/2012";
$( document ).ready( fv_init );

 // Determina se i css sono abilitati
function cssenabled() {
 $( "body" ).append( "<div id=\"testcss\"></div>" );
 $( "#testcss" ).css( { "position" : "absolute", "top" : "-10000px", "left" : "-10000px" } );
 var esit = $( "#testcss" ).position().top < 0;
 $( "#testcss" ).remove();
 return esit;
}

// Funzioni varie
function fv_init() {

// Tabelle: sfondi alternati per IE
 $( ".table_shadow table tr:nth-child(even)" ).addClass( "even" );
 
 // Tabelle: sfondi alternati per IE tabella_d
$( "table.tabella_d tr:nth-child(even)" ).addClass( "even" );


// Tabelle: allineamento campi
 $( ".table_shadow table.primo_sin td:first-child" ).addClass( "sin" );





 // Calendario

 if( cssenabled() ) {
  for( count = 1; count <= 4; count++ )
   $( "#selcal" + count ).attr( "href", "#none" );

  $( "#selcal1" ).click( function() { selcal( 2 ); } );
  $( "#selcal2" ).click( function() { selcal( 1 ); } );
  $( "#selcal3" ).click( function() { selcal( 3 ); } );
  $( "#selcal4" ).click( function() { selcal( 2 ); } );
 }

 function selcal( n ) {
  for( count = 1; count <= 3; count++ )
   $( "#calendario" + count + ", #eventi" + count ).css( { "position" : "absolute", "top" : "-10000px", "left" : "-10000px" } );

  $( "#calendario" + n + ", #eventi" + n ).css( { "position" : "static" } );
  return;
 }

 selcal( 1 );

 var mounths = [ "gennaio", "febbraio", "marzo", "aprile", "maggio", "giugno", "luglio", "agosto", "settembre", "ottobre", "novembre", "dicembre" ];

 if( self.today != undefined ) {
  // Seleziona la data corrente sul calendario
  /* Browser side
  var date = new Date();
  var today = ( ( date.getDate() < 10 )? "0" + date.getDate() : date.getDate() ) + " " + mounths[date.getMonth()] + " " + date.getFullYear();
  */

  // Server side
  var stripdate = today.split( "/" );
  var date = stripdate[0] + " " + mounths[stripdate[1] - 1] + " " + stripdate[2];

  $( "#calendario1 td, #calendario2 td, #calendario3 td" ).each(
   function() {
    content = $( this ).html();
    content = content.replace( /[\r\n\t]/g, "" );
    content = content.replace( /<\/a>/gi, "" );
    content = content.replace( /<a[^>]*>/gi, "" );
    content = content.replace( /<span>/gi, "" );
    content = content.replace( /<\/span>/gi, "" );
    content = content.replace( /^[^\d]+/gi, "" );
    content = $.trim( content.replace( /\s+/, " " ) );

    if( date == content ) {
     $( this ).html( "<" + "div class=\"curdate\">" + $( this ).html() + "<" + "/div>" );
     return false;
    }
   }
  );
 }


 	//copiato da breadcrumb.js 

	function addToTiTrovi(data)
	{
		var jobj = jQuery.parseJSON(data);
		
		var currUrl = jobj.currentPage.url;
		
		var allEl = $("#ti_trovi").html().split("-");
		
		if(currUrl != undefined)
		{
		
			if(currUrl.indexOf("?")>=0)
			{
				currUrl += "&amp;CACHE=NONE";
			}
			else
			{
				currUrl += "?CACHE=NONE";
			}
		
			allEl[(allEl.length-1)] = allEl[(allEl.length-1)].replace("&nbsp;","");
			allEl[(allEl.length-1)] = " <a href=\""+currUrl+"\">"+allEl[(allEl.length-1)]+"</a> ";
		}
		
		$(jobj.breadcrumb).each(function()
		{
			var label = "";
			var url = "";
			
			label = $(this)[0].label;
			url = $(this)[0].url;
			
			if(url == undefined)
			{
				allEl.push(" "+label);
			}
			else
			{
				if(url.indexOf("?")>=0)
				{
					url += "&amp;CACHE=NONE";
				}
				else
				{
					url += "?CACHE=NONE";
				}
				
				allEl.push(" <a href=\""+url+"\">"+label+"</a> ");
			}
		});    
		
		$("#ti_trovi").html(allEl.join("-"));
	}
	
	$(document).ready(function(){
		if(typeof cmsTiTrovi != "undefined")    addToTiTrovi(cmsTiTrovi);                
	}); 


 
 // Al click sull'elemento con classe .toggle_link si apre e si chiude il testo sottostante
  $('div[id^="linkdr"]').addClass( "nascondi" );
  
 // OTTIMIZZARE IL SELETTORE CON UN id CONTENITORE DELLA CLASSE O ALMENO SPECIFICARE IL NOME DELL'ELEMENTO
 $( ".apri_link_dr" ).click(
 	function() {
  	var currID = $(this).find("a").attr("id");
  	$('p[class^="apri_link_dr"]').css('font-weight','normal');
  	/*alert (currID);*/
  	if ($( currID ).hasClass( "mostra" ) ) {
  		/*alert ("dentro");*/
		$( currID ).removeClass( "mostra" );
		$( currID ).addClass( "nascondi" );
		$(this).css('font-weight','normal');
	  } else {
   	/*alert ("dentro_else");*/
		$("[id^=linkdr]").removeClass( "mostra" );
		$("[id^=linkdr]").addClass( "nascondi" );
		
		$( currID ).removeClass( "nascondi" );
		$( currID ).addClass( "mostra" );
		$(this).css('font-weight','bold');
	  }
	
   return false;
  }
 );

  

}
