package it.sogei.ssi.util;



import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class Settings {
	private static Properties settings = null;
	
	private static String projectRoot;
	
	public static Properties getSettings() {
		
		if (settings==null){
			settings = new Properties();
			try {
				settings.load(new FileInputStream(new File(AppConf.ROOT_VAR+AppConf.SETTING_PATH)));
			} catch (FileNotFoundException e) {e.printStackTrace();} catch (IOException e) {e.printStackTrace();}
		}
		return settings;
	}
	
	public static String getProjectRoot(){
		
		/*if (System.getProperty(AppConf.ENV_ROOT_VAR).endsWith("/")) {
			return projectRoot =System.getProperty(AppConf.ENV_ROOT_VAR);
		} else {
			return projectRoot=System.getProperty(AppConf.ENV_ROOT_VAR)+"/";
		}*/
		
		return AppConf.ROOT_VAR;
	}
	
	public static String getConfigurator(){
		return getProjectRoot()+AppConf.LOGGING_PATH;
	}
	

	public static boolean isBenchmark(){
		return Boolean.valueOf(getProperty("BENCHMARK"));
	}
	
	
	
	
	public static String getProperty(String key){
		getProjectRoot();
		getSettings();
		String p =  settings.getProperty(key);
		if (p!=null && !"".equals(p))
			return p.trim();
		else return null;
	}
	
	
	public static String getProperty(String key, String _projectRoot){
		if(projectRoot!=null && projectRoot.endsWith("/"))
			projectRoot=_projectRoot;
		else
			projectRoot=_projectRoot+"/";
		
		getSettings();
		return settings.getProperty(key);
	}
	
	public static Map<String,String> prefixedValueProperties( String prefix){
		Map<String,String> list = new HashMap<String,String>();

        @SuppressWarnings("unchecked")
		Enumeration<String> en = (Enumeration<String>) settings.propertyNames();
        while(en.hasMoreElements()){
            String propName = en.nextElement();
            String propValue = settings.getProperty(propName);

            if(propName.startsWith(prefix)){
                String key = propName.substring(prefix.length());
                list.put(key,propValue);
            }
        }
        return list;
	}	
	
}
