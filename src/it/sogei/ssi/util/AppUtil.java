package it.sogei.ssi.util;

import it.sogei.ssi.util.exporter.RawBytesExporter;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Map;

import javax.el.ELContext;
import javax.el.ValueExpression;
import javax.faces.FacesException;
import javax.faces.application.Application;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.portlet.PortletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

public class AppUtil {
	
	private AppUtil(){}
	
	
	public static FacesContext getCurrentInstance(){
		return FacesContext.getCurrentInstance();
	}
	

	
	public static ExternalContext getExternalContext(){
		return getCurrentInstance().getExternalContext();
	}
	
	public static ELContext getELContext(){
		return getCurrentInstance().getELContext();
	}
	
	public static Application getApplication(){
		return getCurrentInstance().getApplication();
	}
	
	public static String getRequestParameter(String name) {
        return (String) getExternalContext().getRequestParameterMap().get(name);
    }
	
	public static Map<String,String> getRequestParameterMap(){
		return getExternalContext().getRequestParameterMap();
	}
	
	public static Map<String,String[]> getRequestParameterMapFromHttpRequest(){
		return getHttpServletRequest().getParameterMap();
	}
	
	public static String getRequestParameterValueMapFromHttpRequest(String key){
		if(getHttpServletRequest().getParameterMap().get(key)!=null)
			return getHttpServletRequest().getParameterMap().get(key)[0];
		else return null;
	}
	
	public static String[] getRequestParameterValuesMapFromHttpRequest(String key){
		if(getHttpServletRequest().getParameterMap().get(key)!=null)
			return getHttpServletRequest().getParameterMap().get(key);
		else return null;
	}
	
	
	public static Object getInRequestMap(String name) {
        return (Object) getExternalContext().getRequestMap().get(name);
    }
	
	public static Object getInSessionMap(String name) {
        return (Object) getExternalContext().getSessionMap().get(name);
    }
	
	
	public static void removeInRequestMap(String key) {
        getExternalContext().getRequestMap().remove(key);
    }
	
	public static void setInRequestMap(String key, Object value) {
        getExternalContext().getRequestMap().put(key,  value);
    }
	
	public static void setInSessionMap(String key, Object value) {
        getExternalContext().getSessionMap().put(key,  value);
    }
	
	public static void removeInSessionMap(String key) {
        getExternalContext().getSessionMap().remove(key);
    }
	
	public static ValueExpression createValueExpression(String expression) {
	    return getApplication().getExpressionFactory().createValueExpression(getELContext(), "#{" + expression + "}", Object.class);
	}
	public static Object getExpressionValue(String expression) {
        return createValueExpression(expression).getValue(getELContext());
    }
	
	public static HttpServletRequest getHttpServletRequest(){
	
		return( HttpServletRequest)getExternalContext().getRequest();
	}
	
	
	public static HttpServletResponse getHttpServletResponse(){
		
		return  (HttpServletResponse)getExternalContext().getResponse();
	}
	

	
    public static String getAppPath(){
    	return  getHttpServletRequest().getSession().getServletContext().getRealPath("/");
    }
    
    @SuppressWarnings("unchecked")
	public static <T> T getManagedBean (Class<T> clazz, String valueExpression ){
		Application application = getCurrentInstance().getApplication();
        ELContext elContext = getELContext();
        return (T) application.getExpressionFactory().createValueExpression(elContext, "#{"+valueExpression+"}", clazz).getValue(elContext);

	}
        
    
    public static String getRequestQueryString() {

		
		String queryString  = (String) getHttpServletRequest().getAttribute("javax.servlet.forward.query_string");
		

		return queryString;
	}
	
	
	public static Object getRequestAttribute(String name) {
		ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
		PortletRequest portletRequest = (PortletRequest) externalContext.getRequest();

		return portletRequest.getAttribute(name);
	}
	
	

	
	
	public static  String getRequestURL() {
		  try {
		   
		    ExternalContext ext = AppUtil.getExternalContext();
		    URI uri = new URI(ext.getRequestScheme(),
		          null, ext.getRequestServerName(), ext.getRequestServerPort(),
		          ext.getRequestContextPath(), getRequestQueryString(), null);
		    return uri.toASCIIString();
		  } catch (URISyntaxException e) {
		    throw new FacesException(e);
		  }
	}
 
	
	public static String makeAbsoluteUrl(HttpServletRequest httpRequest, String relativeUrl) {

		String file = httpRequest.getContextPath() + "/" + relativeUrl;// httpRequest.getRequestURI();

		if (httpRequest.getQueryString() != null) {
			file += '?' + httpRequest.getQueryString();
		}
		URL reconstructedURL = null;
		try {
			reconstructedURL = new URL(httpRequest.getScheme(),
					httpRequest.getServerName(), httpRequest.getServerPort(),
					file);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		return reconstructedURL.toString();

	}

	public static String makeAbsoluteUrl(String relativeUrl) {
		return makeAbsoluteUrl(getHttpServletRequest(), relativeUrl);
	}
	
	public static StreamedContent toDownload(byte[] ba, String contentType, String filename ){
	     return new DefaultStreamedContent(new ByteArrayInputStream(ba), contentType, filename);       
	}
	
	
	public static void doDownload( byte[] data, String contentType, String fileName  ) throws Exception {
	 
		RawBytesExporter re = new RawBytesExporter(fileName);
		re.setByteArray(data);
		re.setContentType(contentType);

		re.export();
		
	}
	
   public static String getDominio(){
	   
	URL url=null;
	try {
		url = new URL(AppUtil.getHttpServletRequest().getRequestURL().toString());
	} catch (MalformedURLException e) {
		e.printStackTrace();
	}
    String host = url.getHost();
    if(host.startsWith("www")){
        host = host.substring("www".length()+1);
    }
    return host;
   }
}
