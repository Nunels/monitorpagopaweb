package it.sogei.ssi.util.exporter;

import it.sogei.ssi.util.exporter.core.DataExporter;

public  class XmlExporter extends DataExporter {
	   
	  private byte[] byteArray;
	   
	   public XmlExporter(String fileName) {
	      super(fileName + ".xml");
	   }
	   
	   
	   
	   public void setBytes(byte[] byteArray){
		   this.byteArray = byteArray;
	   }
	   
	   @Override
	   public String getContentType() {
	      return DataExporter.XmlContentType;
	   }

	   @Override
	   public byte[] getBytes() throws Exception {
		   return  this.byteArray;
	   }

	public byte[] getByteArray() {
		return byteArray;
	}

	public void setByteArray(byte[] byteArray) {
		this.byteArray = byteArray;
	}

	}