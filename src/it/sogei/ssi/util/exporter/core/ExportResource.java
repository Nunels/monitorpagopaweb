package it.sogei.ssi.util.exporter.core;

import it.sogei.ssi.util.AppUtil;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.faces.application.Resource;
import javax.faces.application.ResourceHandler;

import javax.faces.context.FacesContext;

import com.sun.faces.util.Util;




public class ExportResource extends Resource {

	public static final String RESOURCE_NAME = "export";

	public static final String PARAM_NAME_EXPORT_ID = "exportId";
	public static final String PARAM_NAME_EXPORT_TYPE = "exportContentType";
	public static final String PARAM_NAME_EXPORT_FILENAME = "exportFilename";
	public static final String PARAM_NAME_EXPORT_DATA = "exportData";
	
	

	private String requestPath;

	public ExportResource() {
		setLibraryName(CoreResourceHandler.LIBRARY_NAME);
		setResourceName(RESOURCE_NAME);
	}

	@Override
	public boolean userAgentNeedsUpdate(FacesContext context) {
		// Since this is a list that can potentially change dynamically, always
		// return true.
		return true;
	}

	@Override
	public InputStream getInputStream() throws IOException {
		String exportId = PARAM_NAME_EXPORT_DATA + AppUtil.getRequestParameter(PARAM_NAME_EXPORT_ID);
		InputStream is = new ByteArrayInputStream((byte[]) AppUtil.getInSessionMap(exportId));

		AppUtil.removeInSessionMap(exportId);

		return is;
	}

	@Override
	public String getRequestPath() {

		if (requestPath == null) {
			StringBuilder buf = new StringBuilder();
				
			
			buf.append(ResourceHandler.RESOURCE_IDENTIFIER);
			buf.append("/");
			buf.append(getResourceName()+Util.getFacesMapping(AppUtil.getCurrentInstance()));
			buf.append("?ln=");
			buf.append(getLibraryName());

			buf.append("&");
			buf.append(PARAM_NAME_EXPORT_ID);
			buf.append("=");
			buf.append(AppUtil.getInRequestMap(PARAM_NAME_EXPORT_ID));

			buf.append("&");
			buf.append(PARAM_NAME_EXPORT_TYPE);
			buf.append("=");
			buf.append(AppUtil.getInRequestMap(PARAM_NAME_EXPORT_TYPE));

			buf.append("&");
			buf.append(PARAM_NAME_EXPORT_FILENAME);
			buf.append("=");
			buf.append(AppUtil.getInRequestMap(PARAM_NAME_EXPORT_FILENAME));
			
					
			 final FacesContext context = AppUtil.getCurrentInstance();
			 requestPath = context.getApplication().getViewHandler().getResourceURL(context,buf.toString());
			
			
			requestPath = AppUtil.getExternalContext().encodeResourceURL(requestPath);
			
		}

		return requestPath;
	}

	@Override
	public Map<String, String> getResponseHeaders() {
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Expires", "0");
		headers.put("Cache-Control","must-revalidate, post-check=0, pre-check=0");
		headers.put("Pragma", "public");
		headers.put("Content-disposition", "attachment;" + "filename="+ AppUtil.getRequestParameter(PARAM_NAME_EXPORT_FILENAME));

		return headers;
	}

	@Override
	public URL getURL() {
		return null;
	}

	@Override
	public String getContentType() {
		return AppUtil.getRequestParameter(PARAM_NAME_EXPORT_TYPE);
	}

	public void setRequestPath(String requestPath) {
		this.requestPath = requestPath;
	}

}