package it.sogei.ssi.service.app;

import it.sogei.ssi.util.AppUtil;

import java.io.Serializable;
import java.util.Locale;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

@ManagedBean
@SessionScoped
public class LanguageService implements Serializable {

	private static final long serialVersionUID = 1L;

	private Locale locale = AppUtil.getCurrentInstance().getViewRoot().getLocale();

	public Locale getLocale() {

		return locale;

	}

	public String getLanguage() {

		return locale.getLanguage();

	}

	public void changeLanguage(String language) {
		locale = new Locale(language);

		AppUtil.getCurrentInstance().getViewRoot().setLocale(new Locale(language));

	}

}