package it.sogei.ssi.service.app;



import it.dsi.acc.audm.model.DatiMinimiAUDM;
import it.dsi.acc.audm.model.PartitaIvaDto;
import it.dsi.acc.audm.model.SoggettoAnagraficaDTO;
import it.dsi.ssi.client.AUDMClient;
import it.finanze.dogane.npad.ws.model.dto.NpadVerificaResponse;
import it.finanze.dogane.npad.ws.model.dto.types.AutorizzazioneType;
import it.sogei.pagopa.monitor.service.setup.Service;
import it.sogei.ssi.util.AppConf;
import it.sogei.ssi.util.AppUtil;
import it.sogei.ssi.util.SerialClone;
import it.sogei.ssi.util.Settings;

import java.io.Serializable;
import java.security.Principal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 * The bean used to login the user and load the menu
 * 
 * 
 */
@ManagedBean(eager=true)
@SessionScoped
public class UserService  implements Serializable {

	/**
	 * long serialVersionUID
	 */
	private static final long	serialVersionUID		= -8532086011573061280L;

	private static Logger log = null;

	
	private String username;
	
	private String unauthenticated = AppConf.UNAUTHENTICATED;

		
	private Service service=null;
	

	private List<AutorizzazioneType> utenti = null;
	
	private HashMap<String,AutorizzazioneType> utentiMap = null;
	
	
	private String utenteCorrente =null; 

	
	
	private String ambienteSuCuiGiro = null;
	
	private Map<String, String> denominazioneMap = null;
	
	/**
	 * Loads the user data
	 */
	@PostConstruct
	public void init () {
		
		if(log==null){
			PropertyConfigurator.configure(Settings.getConfigurator());
			log = Logger.getLogger(UserService.class);

			
		}
		
		
		ambienteSuCuiGiro = AppConf.getConfig().getAmbienteSuCuiGiro();
		//idServizio  = AppConf.getUtente().getIdServizio();
		
		/*if (FacesContext.getCurrentInstance().getPartialViewContext().isAjaxRequest()) 
			return; // Skip ajax requests.
		*/
		
		if(utenti!= null) return;

		denominazioneMap = new HashMap<>();
		
	    Principal principal = AppUtil.getHttpServletRequest().getUserPrincipal();
        if (principal != null){
            username = principal.getName();
        }else if(AppConf.getConfig().isUtenteDiTest()){
        	username = AppConf.getUtente().getCfPagatore();
        	
        }else{
            username = AppConf.UNAUTHENTICATED;
        }
		
		service=new Service();
		
		//utenteAutorizzato=true;


		utenteCorrente = AppConf.getUtente().getCfPagatore();
	
		if (utenteCorrente== null){
			
			username = username.toUpperCase();
			String exp = AppConf.getConfig().getExpRegUserMatch(); 
			
			if(exp== null){
				exp = "([A-Z]{6}[0-9]{2}[A-Z]{1}[0-9]{2}[A-Z]{1}[0-9]{3}[A-Z]{1}|[0-9]{11})";
			}
			
			
			String user = null;
			Pattern regex = Pattern.compile(exp);
			Matcher regexMatcher = regex.matcher(username);
			if (regexMatcher.find()) {
				user = regexMatcher.group();
			} 
			
			username = user;
			utenteCorrente = user;
			
		}
		log.info("utenteCorrente "+utenteCorrente);
		
		if(utenti==null){
		
			if(AppConf.getConfig().isNpadIsEnable() &&  AppConf.getConfig().getCodiceAutorizzazione()!=null && AppConf.getConfig().getNomeServizio()!=null){
				log.info("NPAD ACTIVE");
			 	
		       NpadVerificaResponse res = service.verificaAutorizzazione(utenteCorrente);
		        
		       if(res!=null){
			        
			       if(res.getLAutorizzazioni().size()>0){
			    	   utenti = new ArrayList<AutorizzazioneType>();
			    	   utentiMap = new HashMap<String,AutorizzazioneType>();
			       }
			    	   
			        
			      //  lClient == null sputare fuori    // nn dovrebbe capitare l'utente non autorizzato non arriva mai all'app
			       // lClient.size()==0
			        // se la lista � > 1 mostrare la scelata per quale delgante lavorare
			        
			        // nel caso lui � autorizzato in proprio il delegante � null e lui � l'utente 
			        
			       
			        for(int i=0; i<res.getLAutorizzazioni().size(); i++){
			        	
		//	        	log.info(utenti.get(i).getDelegato());
		//	        	log.info(utenti.get(i).getDelegante());
		//	        	log.info(utenti.get(i).getUtente());
			        	AutorizzazioneType user =res.getLAutorizzazioni().get(i);
			        			//utenti.get(i);
			        	
			        	if(i==0){
			        		utenteCorrente = user.getUtente();//utenti.get(0).getDelegante();
			        		
			        	}
			        	if(user.getDelegante()==null && user.getUtente()!=null){
			        		
			        		user.setDelegante(user.getUtente());
			        		user.setDeleganteEmail(user.getUtenteEmail());
			        		
			        		
			        		utenti.add(i, user);
			        		utenteCorrente = user.getUtente();
			        	}
			        	utentiMap.put(user.getDelegante(), user);
			        	
			        }
		       }
	        
			}else{
				  utenti = new ArrayList<AutorizzazioneType>();
		    	  utentiMap = new HashMap<String,AutorizzazioneType>();
		    	  AutorizzazioneType authLoacal = new AutorizzazioneType();
		    	   
		    	  authLoacal.setDelegante(AppConf.getUtente().getCfPagatore()); 
		    	  authLoacal.setDeleganteEmail(AppConf.getUtente().getEmailPagatore());
		    	  log.info(authLoacal.getDeleganteEmail());
		    	  //authLoacal.setDenominazione(AppConf.getUtente().getDenominazione());
		    	  denominazioneMap.put(authLoacal.getDelegante(), AppConf.getUtente().getDenominazione());
		    	  utenti.add(authLoacal);
		    	  utentiMap.put(authLoacal.getDelegante(), authLoacal);
		    	   
				
			}
	        
	        
		}
		
		if(AppConf.getConfig().isAudmIsEnable()){
			log.info("AUDM ACTIVE");
			
			
			AUDMClient client = service.getAUDMClient();
			
			if(client!=null){
				log.info("audm on");
				
				
				if(utenti!=null && utenti.size()>0){
					List<String> cf = new ArrayList<String>();
					
					for (AutorizzazioneType u :utenti ){
						cf.add(u.getDelegante());
					}
					
					
					
					DatiMinimiAUDM datiMinimi = null;
					try {
						datiMinimi = client.getDatiMinimi(cf,AppConf.getConfig().getAudmIstance()); //Arrays.asList("PLNLGU57T26L050T"), "35");
					
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			
					if (datiMinimi != null) {
			
						List<SoggettoAnagraficaDTO> so = datiMinimi.getElencoSoggetti();
							
						if(so!=null){
							for (SoggettoAnagraficaDTO s : so) {
				
								List<PartitaIvaDto> pivaList = s.getElencoPiva();
								
								if(pivaList!=null){
									
									AutorizzazioneType nu  = SerialClone.clone(utentiMap.get(s.getCodiceFiscale()));
									
									if(nu == null){
										nu = new AutorizzazioneType();
									}
									
									if (s.getDenominazione() != null && !s.getDenominazione().equals("")) {
										// denominiazione
										//nu.setDenominazione(s.getDenominazione());
									
									}
				
									for (PartitaIvaDto piva : pivaList) {
										// controllo piva attiva o cessata   
										
										// 1 cf 1 piva attiva????
										if((piva!=null && piva.getStato()!=null && !"".equals(piva.getStato()))){
											
											if(!piva.getStato().equalsIgnoreCase("ATTIVA") ){
												continue;
											}else{
												
												
												
												nu.setDelegante(piva.getNumeroPartitaIva());
												utenti.add(nu);
												
												denominazioneMap.put(nu.getDelegante(), s.getDenominazione()); //??
												utentiMap.put(nu.getDelegante(), nu);
												
												
												log.info(piva.getNumeroPartitaIva());
												break;
											}
										
										}
										
									}
								}
							}
						}
					}
				}
			}
		}
		
	}

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
	
	
	public String getUtenteCorrente() {
		return utenteCorrente;
	}
	
	public AutorizzazioneType getUtenteCorrente(String cf) {
		return utentiMap.get(cf);
	}

	public void setUtenteCorrente(String utenteCorrente) {
		if(utentiMap.get(utenteCorrente)== null) return ;
		this.utenteCorrente = utenteCorrente;
		AppUtil.setInSessionMap("utente", utenteCorrente);
	}
	
	public List<AutorizzazioneType> getUtenti() {
		return utenti;
	}

	public void setUtenti(List<AutorizzazioneType> utenti) {
		this.utenti = utenti;
	}

	public String getAmbienteSuCuiGiro() {
		return ambienteSuCuiGiro;
	}

	public void setAmbienteSuCuiGiro(String ambienteSuCuiGiro) {
		this.ambienteSuCuiGiro = ambienteSuCuiGiro;
	}

	public String getUnauthenticated() {
		return unauthenticated;
	}

	public String getUtenteDenominazione(String utente) {
		return denominazioneMap.get(utente);
	}

	


	

	
}
