package it.sogei.ssi.handler;


import it.sogei.ssi.util.AppUtil;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;


@ManagedBean
@RequestScoped
public class ErrorHandler  {

	private String statusCode;
    private String message;
    private String requestURI;

	@PostConstruct
	public void init(){
        statusCode = AppUtil.getInRequestMap("javax.servlet.error.status_code") != null ? String.valueOf(AppUtil.getInRequestMap("javax.servlet.error.status_code")) : "";
        message = AppUtil.getInRequestMap("javax.servlet.error.message") != null ? (String)AppUtil.getInRequestMap("javax.servlet.error.message") : "";
        requestURI = AppUtil.getInRequestMap("javax.servlet.error.request_uri") != null ? (String)AppUtil.getInRequestMap("javax.servlet.error.request_uri") : "";
	}
	
	public String getStatusCode(){
		return statusCode;
	}

	public String getMessage(){
		return message;
	}

	public String getRequestURI(){
		return requestURI;
	}

	public String getBlank(){
		return "";
	}
}
