package it.sogei.pagopa.monitor.service;

import it.gov.digitpa.schemas._2011.psp.CtInformativaDetail;
import it.gov.digitpa.schemas._2011.psp.CtInformativaPSP;
import it.gov.digitpa.schemas._2011.psp.CtListaInformativePSP;
import it.gov.digitpa.schemas._2011.psp.StParoleChiave;
import it.sogei.data.DataUtilFactory;
import it.sogei.pagopa.monitor.service.setup.Service;
import it.sogei.pay.system.model.external.catalogo.PspCatalogo;
import it.sogei.pay.system.state.SogeiPaySystemException;
import it.sogei.ssi.util.DateUtil;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseId;
import javax.xml.bind.DatatypeConverter;

import org.apache.log4j.Logger;
import org.primefaces.event.FlowEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

@ManagedBean
@SessionScoped
public class CatalogoPspService implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6566900845907234647L;
	private static Logger log = Logger.getLogger(CatalogoPspService.class);
	
	
	private CtListaInformativePSP listaInformativaPsp;
	
	private List<CtInformativaPSP> catalogoPsp = null;

	private List<CtInformativaPSP> catalogoPspFiltrati = null;
	
//	private PagoPaDao dao = new PagoPaDao();
	
	private Date dataCatalogo= DateUtil.getDate("dd/MM/yyyy");

	private Date dataCorrente = DateUtil.getDate();
	
	private Integer selectedCtInformativaPSP = null;
	
	private CtInformativaPSP informativaPSP = null;
	
	
	private String parolaChiave= null;
	private String ragioneSociale= null;
	private String identificativoCanale= null;
	
	private HashSet<String> paroleChiaveSet = new HashSet<String>();
	private HashSet<String> ragioneSocialeSet = new HashSet<String>();
	private HashSet<String> identificativoCanaleSet = new HashSet<String>();
	
	private String tipoFiltro = "0";    
	private List<CtInformativaPSP> listaFiltrata = null;


	private Service service = null;
	
	@PostConstruct
	private void init(){
		caricaCatalogoPsp();
		
	}
	
	
	public void caricaCatalogoPsp(){
		
			log.info("caricaCatalogoPsp");
			if(listaInformativaPsp== null){
				service = new Service();
				
				/*PspCatalogo catalogo = new PspCatalogo();
				catalogo.setDATA_RICEZIONE(dataCatalogo);
				
				catalogo = dao.selectPspCatalogo(catalogo);*/
				
				PspCatalogo input =  new PspCatalogo();
				
				//SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
				
				
				input.setDataFiltro(DateUtil.toXMLGregorianCalendarWithoutTimeStamp(dataCatalogo));
				
				
				
				try {
					input = service.nodoClient(false).selezionaInformativaPSPxData(input);
				} catch (SogeiPaySystemException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
				
				if(input!=null){
					String listaInformativePSPString = new String (input.getXml());
					System.out.println(listaInformativePSPString);
					listaInformativePSPString = listaInformativePSPString.replaceFirst("<listaInformativePSP ", "<listaInformativePSP xmlns=\"http://www.digitpa.gov.it/schemas/2011/psp/\" targetNamespace=\"http://www.digitpa.gov.it/schemas/2011/psp/\" ");	
					System.out.println(listaInformativePSPString);
					
					it.gov.digitpa.schemas._2011.psp.ObjectFactory pspf = new it.gov.digitpa.schemas._2011.psp.ObjectFactory();
					
					
					
					
					
					
					listaInformativaPsp = new CtListaInformativePSP();
					listaInformativaPsp = (CtListaInformativePSP) DataUtilFactory.populateObjectFromXml(listaInformativaPsp,  listaInformativePSPString.getBytes() ,pspf.createListaInformativePSP(listaInformativaPsp).getName());
						
					if(listaInformativaPsp!=null){
					  creaFlitri();
					
					catalogoPsp = listaInformativaPsp.getInformativaPSP();
					}else{
						catalogoPsp = null;	
					}
				}else{
					catalogoPsp = null;
				}
				
			}
	
		
		
	}
	
	
	public void eliminaFiltro(){
		
		identificativoCanale = null;
		listaFiltrata = null;
		tipoFiltro= "0";
		ragioneSociale= null;
		parolaChiave= null;
	
		if(listaInformativaPsp!=null && listaInformativaPsp.getInformativaPSP()!=null){
			catalogoPsp = listaInformativaPsp.getInformativaPSP();
		}else{
			catalogoPsp= null;
		}
	}
	
	public void onItemSelectParolaChiave(SelectEvent event) {
		log.info(event.getObject().toString());
		
		parolaChiave = event.getObject().toString();
    }
	
	public void onItemSelectIdentificativoCanale(SelectEvent event) {
		log.info(event.getObject().toString());
		
		identificativoCanale = event.getObject().toString();
    }
	
	public void onItemSelectRagioneSociale(SelectEvent event) {
		log.info(event.getObject().toString());
		
		ragioneSociale = event.getObject().toString();
    }
	
	
	
	public void filtro(){
		
		
	 	listaFiltrata = new ArrayList<CtInformativaPSP>();
	 	
	
		boolean added = false;
		
	//	log.info(1);
		if(listaInformativaPsp!=null && listaInformativaPsp.getInformativaPSP()!= null){
			
			listaFiltrata = new ArrayList<CtInformativaPSP>();
	
			for(CtInformativaPSP informativaPSP : listaInformativaPsp.getInformativaPSP()){
				
			//	CtInformativaPSP informativaPSP = listaInformativaPsp.getInformativaPSP().get(i);
				if(ragioneSociale!=null && informativaPSP!=null && informativaPSP.getRagioneSociale()!=null)
				//log.info(ragioneSociale+ " "+informativaPSP.getRagioneSociale().contains(ragioneSociale) + " "+informativaPSP.getRagioneSociale());
				//log.info(2);
				if(ragioneSociale!=null &&  !"".equals(ragioneSociale) && informativaPSP!=null && informativaPSP.getRagioneSociale()!=null &&informativaPSP.getRagioneSociale().contains(ragioneSociale)){
					listaFiltrata.add(informativaPSP);
					log.info(3);
					break;
				}
				
				log.info(identificativoCanale);
				if(  (parolaChiave!=null && !parolaChiave.equals("")) || (identificativoCanale!=null && !identificativoCanale.equals("") )){	
					
					if (informativaPSP!=null ){
					for ( int j=0; j< informativaPSP.getListaInformativaDetail().getInformativaDetail().size(); j++){
						CtInformativaDetail informativaDetail =  informativaPSP.getListaInformativaDetail().getInformativaDetail().get(j);
						//log.info(4);
						//log.info(informativaDetail.getIdentificativoCanale());
						if(informativaDetail!=null && informativaDetail.getIdentificativoCanale()!=null && informativaDetail.getIdentificativoCanale().equalsIgnoreCase(identificativoCanale)){
							//log.info(5);
							listaFiltrata.add(informativaPSP);	
							
							break;
						}
						
						
						if(informativaDetail!=null && informativaDetail.getListaParoleChiave()!= null && informativaDetail.getListaParoleChiave().getParoleChiave()!=null){
						
							for (StParoleChiave  paroleChiave  :informativaDetail.getListaParoleChiave().getParoleChiave()){
								//log.info(6);		
								if(paroleChiave.value().equalsIgnoreCase(parolaChiave)){
									//log.info(7);	
									
									listaFiltrata.add(informativaPSP);		
									added = true;
										
									
								}
								
							}
						}
					}
					}
					
				}
					
				//if(ragioneSociale==null || ragioneSociale.equals("")){
									
					if(!tipoFiltro.equals("0") && !added ){
					
						if(tipoFiltro.equals("1")){
							
							
							if(informativaPSP!=null && informativaPSP.getInformativaMaster().getMarcaBolloDigitale()==1  ){
								// filtro
								listaFiltrata.add(informativaPSP);	
								
							}
					
						}else if(tipoFiltro.equals("2")){
							if(  informativaPSP!=null && informativaPSP.getInformativaMaster().getStornoPagamento()==1 ){
								// filtro
								listaFiltrata.add(informativaPSP);
								
														
							}
						}
					
					}else{
						
						if (parolaChiave==null && identificativoCanale==null && ragioneSociale==null ){
						 listaFiltrata.add(informativaPSP);
						}
					}
			
			
					
				//}
			}
		
			catalogoPsp = listaFiltrata;
		
		}
		
	}
	
	private void creaFlitri(){
				
		for(int i=0; i<listaInformativaPsp.getInformativaPSP().size(); i++){
			
			CtInformativaPSP informativaPSP = listaInformativaPsp.getInformativaPSP().get(i);
						
			ragioneSocialeSet.add(informativaPSP.getRagioneSociale());
			
			//log.info(informativaPSP.getRagioneSociale());
		
			
			if(informativaPSP.getInformativaMaster().getStornoPagamento()==1){
				// filtro
				//log.info("storno");
				//log.info(informativaPSP.getInformativaMaster().getStornoPagamento());
										
			}
			
			
			if(informativaPSP.getInformativaMaster().getMarcaBolloDigitale()==1){
				// filtro
				//log.info("marca");
				//log.info(informativaPSP.getInformativaMaster().getMarcaBolloDigitale());
											
			}
		
			
			for (CtInformativaDetail informativaDetail : informativaPSP.getListaInformativaDetail().getInformativaDetail()){
				
				
				identificativoCanaleSet.add(informativaDetail.getIdentificativoCanale());
				
				if(informativaDetail!=null && informativaDetail.getListaParoleChiave()!=null && informativaDetail.getListaParoleChiave().getParoleChiave()!=null ){
					for (StParoleChiave  paroleChiave  :informativaDetail.getListaParoleChiave().getParoleChiave()){
												
						paroleChiaveSet.add(paroleChiave.value());
						
					}
				}
			}
			
		}
	}
	
	
	
	public static boolean containsIgnoreCase(String str, String searchStr)     {
	    if(str == null || searchStr == null) return false;

	    final int length = searchStr.length();
	    if (length == 0)
	        return true;

	    for (int i = str.length() - length; i >= 0; i--) {
	        if (str.regionMatches(true, i, searchStr, 0, length))
	            return true;
	    }
	    return false;
	}
	
	public List<String> completeParolaChiave(String query) {
        List<String> results = new ArrayList<String>(paroleChiaveSet);
        for(int i= results.size()-1; i>=0 ; i--){
        	if(!containsIgnoreCase(results.get(i),query)){
        		results.remove(i);
        	}
        }
        
        
        return results;
    }
	
	public List<String> completeRagioneSociale(String query) {
        List<String> results = new ArrayList<String>(ragioneSocialeSet);
        for(int i= results.size()-1; i>=0 ; i--){
        	if(!containsIgnoreCase(results.get(i),query)){
        		results.remove(i);
        	}
        }
        
        
        return results;
    }
	
	public List<String> completeIdentificativoCanale(String query) {
        List<String> results = new ArrayList<String>(identificativoCanaleSet);
        for(int i= results.size()-1; i>=0 ; i--){
        	if(!containsIgnoreCase(results.get(i),query)){
        		results.remove(i);
        	}
        }
        
      
        return results;
    }
	
	
	
	public StreamedContent getImage() throws IOException {
		FacesContext context = FacesContext.getCurrentInstance();
		 String id = context.getExternalContext().getRequestParameterMap().get("imgIndex");
		
		
		if (context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE ) {
			return new DefaultStreamedContent();
		} 
		
			 
		if(id!=null && catalogoPsp!=null){	
			CtInformativaPSP  psp = catalogoPsp.get(Integer.valueOf(id).intValue());
			//log.info(id+" cazzo");
			// Reading image from database assuming that product image (bytes)
			// of product id I1 which is already stored in the database.
			if(psp!=null){
				byte[] imageData = DatatypeConverter.parseBase64Binary(new String(psp.getInformativaMaster().getLogoPSP()));
				return new DefaultStreamedContent(new ByteArrayInputStream(imageData), 	"image/png");
			}else{
				return new DefaultStreamedContent();
			}
			//FileUtils.writeByteArrayToFile(new File("D:/"+psp.getIdentificativoPSP()+".png"), imageData);
			
			
		}else{
			return new DefaultStreamedContent();
		}
		
	}
	
	public StreamedContent getImageLogoServizio() throws IOException {
		FacesContext context = FacesContext.getCurrentInstance();
			 
		
		String param = context.getExternalContext().getRequestParameterMap().get("rowIndexImgIndex");
		
		String id= null;
		if(selectedCtInformativaPSP!=null && param==null)
			id = selectedCtInformativaPSP+"";
		else	id = param;
			
		String datail = context.getExternalContext().getRequestParameterMap().get("rowIndexInformativaDetail");
	
		
		if (context.getCurrentPhaseId() == PhaseId.RENDER_RESPONSE ) {
			return new DefaultStreamedContent();
		} 
		
		
		 
		if(id!=null && datail!=null && catalogoPsp!=null){	
			CtInformativaPSP  psp = catalogoPsp.get(Integer.valueOf(id).intValue());
			//log.info(id+" selectedCtInformativaPSP ");
			//log.info(datail+" ImageLogoServizio");
			// Reading image from database assuming that product image (bytes)
			// of product id I1 which is already stored in the database.
			if(psp!=null){
				if(Integer.valueOf(datail).intValue()< psp.getListaInformativaDetail().getInformativaDetail().size()){
					if(psp.getListaInformativaDetail().getInformativaDetail().get(Integer.valueOf(datail).intValue()).getIdentificazioneServizio().getLogoServizio()!=null){
						byte[] imageData = DatatypeConverter.parseBase64Binary(new String(psp.getListaInformativaDetail().getInformativaDetail().get(Integer.valueOf(datail).intValue()).getIdentificazioneServizio().getLogoServizio()));
						
						//FileUtils.writeByteArrayToFile(new File("D:/"+id+"-"+datail+".png"), imageData);
						
						return new DefaultStreamedContent(new ByteArrayInputStream(imageData), 	"image/png");
					}
				}
			}
		}
			
		return new DefaultStreamedContent();
		
		
	}
	
	
	
	
	
	 public void onDateSelect(SelectEvent event) {
		
		 listaInformativaPsp = null;
		
		 dataCatalogo = (Date) event.getObject();
		 
		 caricaCatalogoPsp();
	 }
	
	
	
	public List<CtInformativaPSP> getCatalogoPsp() {
		return catalogoPsp;
	}


	public void setCatalogoPsp(List<CtInformativaPSP> catalogoPsp) {
		this.catalogoPsp = catalogoPsp;
	}


	public List<CtInformativaPSP> getCatalogoPspFiltrati() {
		return catalogoPspFiltrati;
	}


	public void setCatalogoPspFiltrati(List<CtInformativaPSP> catalogoPspFiltrati) {
		this.catalogoPspFiltrati = catalogoPspFiltrati;
	}

	
	public CtListaInformativePSP getListaInformativaPsp() {
		return listaInformativaPsp;
	}


	public void setListaInformativaPsp(CtListaInformativePSP listaInformativaPsp) {
		this.listaInformativaPsp = listaInformativaPsp;
	}


	public Date getDataCatalogo() {
		return dataCatalogo;
	}


	public void setDataCatalogo(Date dataCatalogo) {
		this.dataCatalogo = dataCatalogo;
	}


	public Date getDataCorrente() {
		return dataCorrente;
	}


	public void setDataCorrente(Date dataCorrente) {
		this.dataCorrente = dataCorrente;
	}


	public CtInformativaPSP getInformativaPSP() {
		if(selectedCtInformativaPSP!=null){
			if(listaFiltrata!=null){
				informativaPSP = listaFiltrata.get(selectedCtInformativaPSP.intValue());
			}else{
				informativaPSP = listaInformativaPsp.getInformativaPSP().get(selectedCtInformativaPSP.intValue());
			}
		}
		return informativaPSP;
	}


	public void setInformativaPSP(CtInformativaPSP informativaPSP) {
		this.informativaPSP = informativaPSP;
	}
	
	
	 public String onFlowProcess(FlowEvent event) {
	        //if(skip) {
	        //    skip = false;   //reset in case user goes back
	        //    return "confirm";
	        //}
	        //else {
	            return event.getNewStep();
	       // }
	    }


	public String getTipoFiltro() {
		return tipoFiltro;
	}


	public void setTipoFiltro(String tipoFiltro) {
		this.tipoFiltro = tipoFiltro;
	}


	public String getParolaChiave() {
		return parolaChiave;
	}


	public void setParolaChiave(String parolaChiave) {
		this.parolaChiave = parolaChiave;
	}


	public String getRagioneSociale() {
		return ragioneSociale;
	}


	public void setRagioneSociale(String ragioneSociale) {
		this.ragioneSociale = ragioneSociale;
	}


	public String getIdentificativoCanale() {
		return identificativoCanale;
	}


	public void setIdentificativoCanale(String identificativoCanale) {
		this.identificativoCanale = identificativoCanale;
	}
	
	
	public Integer getSelectedCtInformativaPSP() {
		return selectedCtInformativaPSP;
	}


	public void setSelectedCtInformativaPSP(Integer selectedCtInformativaPSP) {
		this.selectedCtInformativaPSP = selectedCtInformativaPSP;
	}


}
