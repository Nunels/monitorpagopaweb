package it.sogei.pagopa.monitor.service.setup;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean
@SessionScoped
public class MenuService implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int activeIndex = 0;
	private String currentPage;

	private static final String unSelectColor = "";
	private static final String selectColor = "-selected";

	private String coloreAnalitics;
	private String coloreElencoFlussi;
	private String coloreGestioneEventi;
	private String coloreCatalogoPSP;
	private String coloreLegenda;
	private String coloreInformativa;
	private String coloreTest;
	private String coloreDownload;

	public int getActiveIndex() {
		return activeIndex;
	}

	public void setActiveIndex(Long activeIndex) {
		int tabMenu = activeIndex.intValue();
		resetColor();

		switch (tabMenu) {
		case 0:
			coloreAnalitics = selectColor;
			break;
		case 1:
			coloreElencoFlussi = selectColor;
			break;
		case 2:
			coloreGestioneEventi = selectColor;
			break;
		case 3:
			coloreCatalogoPSP = selectColor;
			break;
		case 4:
			coloreLegenda = selectColor;
			coloreAnalitics = "";
			coloreElencoFlussi = "";
			coloreGestioneEventi = "";
			coloreCatalogoPSP = "";
			break;
		case 5:
			coloreInformativa = selectColor;
			coloreAnalitics = "";
			coloreElencoFlussi = "";
			coloreGestioneEventi = "";
			coloreCatalogoPSP = "";
			break;
		case 10:
			coloreTest = selectColor;
			break;
		case 11:
			coloreDownload = selectColor;
			break;

		default:
			break;
		}

		this.activeIndex = activeIndex.intValue();
	}

	public String getCurrentPage() {

		switch (activeIndex) {
		case 0:
			currentPage = Page.monitorStat;
			coloreAnalitics = selectColor;

			break;
		case 1:
			currentPage = Page.storicoPagamenti;
			break;
		case 2:
			currentPage = Page.giornaleEventi;
			break;
		case 3:
			currentPage = Page.storicoPSP;
			break;
		case 4:
			currentPage = Page.legenda;
			break;
		case 5:
			currentPage = Page.informativa;
			break;
		case 10:
			currentPage = Page.testPage;
			break;
		case 11:
			currentPage = Page.docPage;

		default:
			break;
		}

		return currentPage;
	}

	private void resetColor() {
		coloreAnalitics = "";
		coloreCatalogoPSP = "";
		coloreElencoFlussi = "";
		coloreGestioneEventi = "";
		coloreInformativa = "";
		coloreLegenda = "";
		coloreTest = "";
		coloreDownload = "";
	}

	public String getColoreAnalitics() {
		return coloreAnalitics;
	}

	public void setColoreAnalitics(String coloreAnalitics) {
		this.coloreAnalitics = coloreAnalitics;
	}

	public String getColoreElencoFlussi() {
		return coloreElencoFlussi;
	}

	public void setColoreElencoFlussi(String coloreElencoFlussi) {
		this.coloreElencoFlussi = coloreElencoFlussi;
	}

	public String getColoreGestioneEventi() {
		return coloreGestioneEventi;
	}

	public void setColoreGestioneEventi(String coloreGestioneEventi) {
		this.coloreGestioneEventi = coloreGestioneEventi;
	}

	public String getColoreCatalogoPSP() {
		return coloreCatalogoPSP;
	}

	public void setColoreCatalogoPSP(String coloreCatalogoPSP) {
		this.coloreCatalogoPSP = coloreCatalogoPSP;
	}

	public String getColoreLegenda() {
		return coloreLegenda;
	}

	public void setColoreLegenda(String coloreLegenda) {
		this.coloreLegenda = coloreLegenda;
	}

	public String getColoreInformativa() {
		return coloreInformativa;
	}

	public void setColoreInformativa(String coloreInformativa) {
		this.coloreInformativa = coloreInformativa;
	}

	public String getColoreTest() {
		return coloreTest;
	}

	public void setColoreTest(String coloreTest) {
		this.coloreTest = coloreTest;
	}

	public String getColoreDownload() {
		return coloreDownload;
	}

	public void setColoreDownload(String coloreDownload) {
		this.coloreDownload = coloreDownload;
	}

}
