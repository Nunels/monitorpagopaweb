package it.sogei.pagopa.monitor.service.setup;

public class Page {

	public static final String storicoPagamenti = "storicoPagamenti.xhtml";

	public static final String giornaleEventi = "giornaleEventi.xhtml";
	public static final String legenda = "legenda.xhtml";
	public static final String informativa = "informativa.xhtml";
	public static final String storicoPSP = "storicoPSP.xhtml";
	public static final String monitorStat = "monitorStat.xhtml";

	public static final String testPage = "testPage.xhtml";

	public static final String docPage = "docPage.xhtml";

}
