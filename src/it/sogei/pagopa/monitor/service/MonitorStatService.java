package it.sogei.pagopa.monitor.service;

import it.sogei.doc.api.PaySystemDocumentRServiceApiTest;
import it.sogei.pagopa.monitor.model.FiltroRicerca;
import it.sogei.pagopa.monitor.service.setup.MenuService;
import it.sogei.pay.system.model.external.servizi.Servizi;
import it.sogei.pay.system.model.external.servizi.ServiziData;
import it.sogei.paysystem.accountability.PaySystemAccountabilityServiceApiTest;
import it.sogei.paysystem.monitor.PaySystemMonitorServiceApiTest;
import it.sogei.paysystem.monitor.stat.PaySystemMonitorStatServiceApiTest;
import it.sogei.ssi.util.AppConf;
import it.sogei.ssi.util.AppUtil;
import it.sogei.test.TestElementiServiceApiTest;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import org.apache.log4j.Logger;
import org.primefaces.event.TabChangeEvent;
import org.primefaces.model.charts.ChartData;
import org.primefaces.model.charts.pie.PieChartDataSet;
import org.primefaces.model.charts.pie.PieChartModel;

@ManagedBean
@SessionScoped
public class MonitorStatService implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6609076186196171930L;

	private static Logger log = Logger.getLogger(MonitorStatService.class);

	private PaySystemMonitorStatServiceApiTest apiMonitorStat = new PaySystemMonitorStatServiceApiTest();
	private PaySystemMonitorServiceApiTest apiMonitor = new PaySystemMonitorServiceApiTest();
	private PaySystemAccountabilityServiceApiTest apiAccountability = new PaySystemAccountabilityServiceApiTest();
	private TestElementiServiceApiTest apiTest = new TestElementiServiceApiTest();
	private PaySystemDocumentRServiceApiTest apiDoc = new PaySystemDocumentRServiceApiTest();

	private static final String contentTypeJson = "application/json";
	private static final String acceptLanguage = "it";

	// private String colori
	// ="58BA27,FFCC33,00FF00,0000FF,FF0000,FF00FF,FFFF00,00FFFF,33CCCC,CC6633";

	private ServiziData serviziData = new ServiziData();
	private List<SelectItem> serviziList;
	private List<SelectItem> areaList;
	private Map<String, List<SelectItem>> mapAreaCategorie = new HashMap<String, List<SelectItem>>();

	private boolean viewTab = false;

	public static final String allAree = "ALL";

	private PieChartModel livePieModelMonitor;
	private PieChartModel livePieModelMonitorStat;
	private PieChartModel livePieModelAccountability;
	private PieChartModel livePieModelTest;
	private PieChartModel livePieModelDoc;

	private FiltroRicerca filtroR;

	private String layout;
	private String content;

	// private DominioStyle dominioStyle;

	private Map<String, String> ec = null;
	private List<SelectItem> listItem = null;
	private String selectedEC;
	private long activeIndexSogei;

	@ManagedProperty("#{menuService}")
	private MenuService menuService;

	public MonitorStatService() {
		System.out.println("inizio monitor");
		if (log == null) {
			// PropertyConfigurator.configure(AppConf.getLogConfFile());
			log = Logger.getLogger(MonitorStatService.class);

		}

		filtroR = new FiltroRicerca();

		// ec = AppConf.tuttiIdomini();
		listItem = AppConf.tuttiIdomini();
		activeIndexSogei = AppConf.getActiveIndexSogei(listItem);
		layout = AppConf.getDominio().getLayout();
		content = AppConf.getDominio().getContent();
		AppUtil.setInSessionMap("selectedEC", AppConf.getDominio()
				.getIdDominio());
		selectedEC = AppConf.getDominio().getIdDominio();
		combo();
		System.out.println("_monitorstat" + layout);

	}

	private void combo() {
		System.out.println("_monitorstat" + layout);

		serviziData = apiMonitor.getServizi(contentTypeJson, acceptLanguage);
		serviziList = new ArrayList<SelectItem>();
		areaList = new ArrayList<SelectItem>();

		mapAreaCategorie = new HashMap<String, List<SelectItem>>();
		// service = new Service();

		for (Servizi serviziObj : serviziData.getServiziList()) {

			if (!serviziObj.getIdDominio().equalsIgnoreCase(
					(String) AppUtil.getInSessionMap("selectedEC")))
				continue;// AppConf.getDominio().getIdDominio())) continue;

			ArrayList<SelectItem> si = null;
			if (mapAreaCategorie.containsKey(serviziObj.getArea())) {

				si = (ArrayList<SelectItem>) mapAreaCategorie.get(serviziObj
						.getArea());

			} else {
				si = new ArrayList<>();

			}

			SelectItem item = new SelectItem();
			item.setValue(serviziObj.getServizio());
			item.setLabel(serviziObj.getServizio());
			si.add(item);

			mapAreaCategorie.put(serviziObj.getArea(), si);

			serviziList.add(item);

		}

		for (String area : mapAreaCategorie.keySet()) {
			SelectItem item = new SelectItem();
			item.setValue(area);
			item.setLabel(area);

			areaList.add(item);
		}

		mapAreaCategorie.put(allAree, serviziList);
	}

	public void onTabChange(TabChangeEvent event) {
		AppUtil.setInSessionMap("selectedEC", event.getTab().getAriaLabel());
		filtroR = new FiltroRicerca();
		combo();
	}

	public void changeArea(ValueChangeEvent event) {/* ValueChangeEvent */// AjaxBehaviorEvent
																			// event){

		if (event.getNewValue() != null)
			filtroR.setArea(event.getNewValue().toString());
		filtroR.setCategoria(0);
		serviziList = new ArrayList<SelectItem>();
		serviziList = mapAreaCategorie.get(filtroR.getArea());// event.getNewValue());
	}

	public boolean isViewTab() {
		return viewTab;
	}

	public void setViewTab(boolean viewTab) {
		this.viewTab = viewTab;
	}

	public ServiziData getServiziData() {
		return serviziData;
	}

	public void setServiziData(ServiziData serviziData) {
		this.serviziData = serviziData;
	}

	public List<SelectItem> getServiziList() {
		return serviziList;
	}

	public void setServiziList(List<SelectItem> serviziList) {
		this.serviziList = serviziList;
	}

	public List<SelectItem> getAreaList() {
		return areaList;
	}

	public void setAreaList(List<SelectItem> areaList) {
		this.areaList = areaList;
	}

	public PieChartModel getLivePieModelMonitor() {

		livePieModelMonitor = new PieChartModel();

		boolean welcomeTest = true;

		try {
			// welcomeTest = service.nodoClient().welcomeTest();
			welcomeTest = apiMonitor.welcomeTest(contentTypeJson,
					acceptLanguage);
		} catch (Exception e1) {
			welcomeTest = false;
			e1.printStackTrace();
		}

		PieChartDataSet dataSet = new PieChartDataSet();
		List<Number> values = new ArrayList<>();
		List<String> bgColors = new ArrayList<>();
		List<String> labels = new ArrayList<>();
		ChartData data = new ChartData();
		if (welcomeTest) {
			bgColors.add("rgb(0, 255, 63)");
			labels.add("Monitor - Stato OK");

		} else {
			bgColors.add("rgb(255, 67, 0)");
			labels.add("Monitor - Stato KO");
		}

		values.add(100);
		dataSet.setData(values);

		dataSet.setData(values);
		dataSet.setBackgroundColor(bgColors);
		data.addChartDataSet(dataSet);
		data.setLabels(labels);
		livePieModelMonitor.setData(data);

		return livePieModelMonitor;
	}

	public PieChartModel getLivePieModelMonitorStat() {
		livePieModelMonitorStat = new PieChartModel();

		boolean welcomeTest = true;

		try {
			// welcomeTest = service.nodoClient().welcomeTest();
			welcomeTest = apiMonitorStat.welcomeTest(contentTypeJson,
					acceptLanguage);

		} catch (Exception e1) {
			welcomeTest = false;
			e1.printStackTrace();
		}

		PieChartDataSet dataSet = new PieChartDataSet();
		List<Number> values = new ArrayList<>();
		List<String> bgColors = new ArrayList<>();
		List<String> labels = new ArrayList<>();
		ChartData data = new ChartData();
		if (welcomeTest) {
			bgColors.add("rgb(0, 255, 63)");
			labels.add("Monitor Stat- Stato OK");

		} else {
			bgColors.add("rgb(255, 67, 0)");
			labels.add("Monitor Stat- Stato KO");
		}

		values.add(100);
		dataSet.setData(values);

		dataSet.setData(values);
		dataSet.setBackgroundColor(bgColors);
		data.addChartDataSet(dataSet);
		data.setLabels(labels);
		livePieModelMonitorStat.setData(data);

		return livePieModelMonitorStat;

	}

	public PieChartModel getLivePieModelAccountability() {

		livePieModelAccountability = new PieChartModel();

		boolean welcomeTest = true;

		try {
			// welcomeTest = service.nodoClient().welcomeTest();
			welcomeTest = apiAccountability.welcomeTest(contentTypeJson,
					acceptLanguage);
		} catch (Exception e1) {
			welcomeTest = false;
			e1.printStackTrace();
		}

		PieChartDataSet dataSet = new PieChartDataSet();
		List<Number> values = new ArrayList<>();
		List<String> bgColors = new ArrayList<>();
		List<String> labels = new ArrayList<>();
		ChartData data = new ChartData();
		if (welcomeTest) {
			bgColors.add("rgb(0, 255, 63)");
			labels.add("Accountability - Stato OK");

		} else {
			bgColors.add("rgb(255, 67, 0)");
			labels.add("Accountability - Stato KO");
		}

		values.add(100);
		dataSet.setData(values);

		dataSet.setData(values);
		dataSet.setBackgroundColor(bgColors);
		data.addChartDataSet(dataSet);
		data.setLabels(labels);
		livePieModelAccountability.setData(data);

		return livePieModelAccountability;
	}

	public PieChartModel getLivePieModelTest() {
		livePieModelTest = new PieChartModel();
		boolean welcomeTest = true;
		System.out.println(welcomeTest);

		try {
			// welcomeTest = service.nodoClient().welcomeTest();
			welcomeTest = apiTest.welcomeTest(contentTypeJson, acceptLanguage);
			System.out.println("_" + welcomeTest);

		} catch (Exception e1) {
			welcomeTest = false;
			e1.printStackTrace();
		}

		PieChartDataSet dataSet = new PieChartDataSet();
		List<Number> values = new ArrayList<>();
		List<String> bgColors = new ArrayList<>();
		List<String> labels = new ArrayList<>();
		ChartData data = new ChartData();
		if (welcomeTest) {
			bgColors.add("rgb(0, 255, 63)");
			labels.add("Test - Stato OK");

		} else {
			bgColors.add("rgb(255, 67, 0)");
			labels.add("Test - Stato KO");
		}

		values.add(100);
		dataSet.setData(values);

		dataSet.setData(values);
		dataSet.setBackgroundColor(bgColors);
		data.addChartDataSet(dataSet);
		data.setLabels(labels);
		livePieModelTest.setData(data);

		return livePieModelTest;
	}

	public PieChartModel getLivePieModelDoc() {
		livePieModelDoc = new PieChartModel();
		boolean welcomeTest = true;
		System.out.println(welcomeTest);

		try {
			// welcomeTest = service.nodoClient().welcomeTest();
			welcomeTest = apiDoc.welcomeTest(contentTypeJson, acceptLanguage);
			System.out.println("_" + welcomeTest);

		} catch (Exception e1) {
			welcomeTest = false;
			e1.printStackTrace();
		}

		PieChartDataSet dataSet = new PieChartDataSet();
		List<Number> values = new ArrayList<>();
		List<String> bgColors = new ArrayList<>();
		List<String> labels = new ArrayList<>();
		ChartData data = new ChartData();
		if (welcomeTest) {
			bgColors.add("rgb(0, 255, 63)");
			labels.add("Download - Stato OK");

		} else {
			bgColors.add("rgb(255, 67, 0)");
			labels.add("Download - Stato KO");
		}

		values.add(100);
		dataSet.setData(values);

		dataSet.setData(values);
		dataSet.setBackgroundColor(bgColors);
		data.addChartDataSet(dataSet);
		data.setLabels(labels);
		livePieModelDoc.setData(data);

		return livePieModelDoc;
	}

	public String getLayout() {
		return layout;
	}

	public void setLayout(String layout) {
		this.layout = layout;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Map<String, String> getEc() {
		return ec;
	}

	public void setEc(Map<String, String> ec) {
		this.ec = ec;
	}

	public String getSelectedEC() {
		return selectedEC;
	}

	public void setSelectedEC(String selectedEC) {
		this.selectedEC = selectedEC;
	}

	public List<SelectItem> getListItem() {
		return listItem;
	}

	public void setListItem(List<SelectItem> listItem) {
		this.listItem = listItem;
	}

	public MenuService getMenuService() {
		return menuService;
	}

	public void setMenuService(MenuService menuService) {
		this.menuService = menuService;
	}

	public long getActiveIndexSogei() {
		return activeIndexSogei;
	}

	public void setActiveIndexSogei(long activeIndexSogei) {
		this.activeIndexSogei = activeIndexSogei;
	}

	public Map<String, List<SelectItem>> getMapAreaCategorie() {
		return mapAreaCategorie;
	}

	public void setMapAreaCategorie(
			Map<String, List<SelectItem>> mapAreaCategorie) {
		this.mapAreaCategorie = mapAreaCategorie;
	}

	public void setLivePieModelTest(PieChartModel livePieModelTest) {
		this.livePieModelTest = livePieModelTest;
	}

	public void setLivePieModelDoc(PieChartModel livePieModelDoc) {
		this.livePieModelDoc = livePieModelDoc;
	}

}
