package it.sogei.pagopa.monitor.service;

import it.sogei.pagopa.monitor.model.GiornaleEventi;

import java.util.Date;
import java.util.List;

public class ListUtility {

	public ListUtility() {
	}

	GiornaleEventi g = new GiornaleEventi();

	public List<GiornaleEventi> aggiungi(List<GiornaleEventi> l) {
		createGiornaleEventi();
		l.add(g);
		return l;
	}

	public it.sogei.pagopa.monitor.model.GiornaleEventi createGiornaleEventi() {
		g.setID_EVENTO(123L);
		g.setDATA_ORA_EVENTO(new Date());
		g.setESITO("OK");
		g.setPARAM_INTERFACCIA("param");
		g.setCANALE_PAGAMENTO("carta");
		g.setID_STAZIONE_INT_PA("idstaz");
		g.setID_EROGATORE("id_erog");
		g.setID_FRUITORE("id_fruit");
		g.setSOTTOTIPO_EVENTO("sototipo");
		g.setTIPO_EVENTO("tipo");
		g.setCATEGORIA_EVENTO("categori");
		g.setCOMPONENTE("compo");
		g.setTIPO_VERSAMENTO("tipo_vers");
		g.setID_PSP("id_psp");
		g.setCOD_CONTESTO("cod_cont");
		g.setIUV("iuv");
		g.setID_DOMINIO("id_dom");
		return g;
	}
}
