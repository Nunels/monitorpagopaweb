package it.sogei.pagopa.monitor.service;

import gov.telematici.pagamenti.ws.EsitoChiediStatoRPT;
import gov.telematici.pagamenti.ws.NodoChiediCopiaRT;
import gov.telematici.pagamenti.ws.NodoChiediCopiaRTRisposta;
import gov.telematici.pagamenti.ws.NodoChiediStatoRPT;
import gov.telematici.pagamenti.ws.join.ANodoChiediCopiaRTCarrello;
import gov.telematici.pagamenti.ws.join.ASelezionaStatoRPTCopiaRTCarrello;
import gov.telematici.pagamenti.ws.join.AStatoRPTCopiaRT;
import gov.telematici.pagamenti.ws.join.AStatoRPTCopiaRTCarrello;
import gov.telematici.pagamenti.ws.ppthead.IntestazioneCarrelloPPT;
import it.sogei.pagopa.monitor.model.Dominio;
import it.sogei.pagopa.monitor.model.TuttiStati.EsitoPagamento;
import it.sogei.pagopa.monitor.model.TuttiStati.Stati;
import it.sogei.pagopa.monitor.service.setup.Service;
import it.sogei.pay.system.model.external.FiltroFlusso;
import it.sogei.pay.system.model.external.Flusso;
import it.sogei.pay.system.model.external.FlussoData;
import it.sogei.pay.system.state.SogeiPaySystemException;
import it.sogei.paysystem.monitor.PaySystemMonitorServiceApiTest;
import it.sogei.paysystem.monitor.stat.PaySystemMonitorStatServiceApiTest;
import it.sogei.ssi.util.AppConf;
import it.sogei.ssi.util.AppUtil;
import it.sogei.ssi.util.DateUtil;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.model.SelectItem;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.lowagie.text.Document;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.PageSize;
import com.lowagie.text.Phrase;

@ManagedBean
@SessionScoped
public class StoricoPagamentiService implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6609076186196171930L;

	private static Logger log = Logger.getLogger(StoricoPagamentiService.class);

	private Dominio dominio = null;
	private Service service = null;

	private String pagatore = "";
	private String versante = "";

	private boolean filtroAttivo = false;

	public static final String allAree = "ALL";

	private List<Flusso> listaStorico = null;

	private Flusso dettaglioFlusso = null;

	private LazyDataModel<Flusso> listaStoricoPagamenti = null;

	// private PagoPaDao dao = null;

	private PaySystemMonitorServiceApiTest api = new PaySystemMonitorServiceApiTest();
	private PaySystemMonitorStatServiceApiTest api2 = new PaySystemMonitorStatServiceApiTest();

	private static final String contentTypeJson = "application/json";
	private static final String acceptLanguage = "it";

	@ManagedProperty("#{giornaleDegliEventiService}")
	private GiornaleDegliEventiService giornaleDegliEventiService;

	@ManagedProperty("#{monitorStatService}")
	private MonitorStatService monitorStatService;

	private Map<Long, EsitoChiediStatoRPT> esitoStatoRPTMap = new HashMap<Long, EsitoChiediStatoRPT>();
	private Map<String, List<AStatoRPTCopiaRT>> esitoStatoRPTCarrelloMap = new HashMap<String, List<AStatoRPTCopiaRT>>();
	// private List<Servizo> servizi = null;

	private HashSet<String> stati = null;

	public List<SelectItem> getStati() {
		stati = new HashSet<>();
		Stati[] statiArray = Stati.values();
		List<SelectItem> result = new ArrayList<SelectItem>();

		for (Stati stato : statiArray) {
			SelectItem item = new SelectItem();
			item.setLabel(stato.toString().replaceAll("_", " "));
			item.setValue(stato);
			stati.add(stato.toString());
			result.add(item);
		}

		return result;
	}

	public List<SelectItem> getEsiti() {

		EsitoPagamento[] esitiArray = EsitoPagamento.values();
		List<SelectItem> result = new ArrayList<SelectItem>();

		for (EsitoPagamento esito : esitiArray) {
			SelectItem item = new SelectItem();

			item.setLabel(esito.toString().replaceAll("_|PAGAMENTO", " "));

			// item.setLabel(esito.toString().replaceAll("PAGAMENTO", " "));
			item.setValue(esito);

			result.add(item);
		}

		result.addAll(getStati());

		return result;
	}

	@PostConstruct
	private void init() {
		dominio = AppConf.getDominio();// new Dominio();
		service = new Service();

		listaStoricoPagamenti = new LazyDataModel<Flusso>() {
			@Override
			public List<Flusso> load(int first, int pageSize, String sortField,
					SortOrder sortOrder, Map<String, Object> filters) {

				Object dRichiesta = filters.get("dataRichiesta");
				Object dRicevuta = filters.get("dataRicevuta");
				filters.remove("dataRichiesta");
				filters.remove("dataRicevuta");

				FiltroFlusso filtro = new FiltroFlusso();

				Flusso flussoFilter = new Flusso();
				if (filters != null && filters.size() > 0) {

					try {
						BeanUtils.populate(flussoFilter,
								new HashMap<String, Object>(filters));
					} catch (IllegalAccessException | InvocationTargetException e) {
						e.printStackTrace();
					}

				}

				if (dRichiesta != null) {
					// log.info((Date)dRichiesta);
					flussoFilter.setDataRichiesta(DateUtil
							.toXmlGregorianCalendar((Date) dRichiesta));
				} else {
					flussoFilter.setDataRichiesta(null);
				}

				if (dRicevuta != null) {
					// log.info((Date)dRicevuta);
					flussoFilter.setDataRicevuta(DateUtil
							.toXmlGregorianCalendar((Date) dRicevuta));
				} else {
					flussoFilter.setDataRicevuta(null);
				}

				if (flussoFilter != null
						&& flussoFilter.getEsitoPagamento() != null) {
					if (stati != null && flussoFilter != null
							&& stati.contains(flussoFilter.getEsitoPagamento())) {
						flussoFilter.setStatoPagamento(flussoFilter
								.getEsitoPagamento());
						flussoFilter.setEsitoPagamento(null);
					}
				}

				if (sortField != null) {

					switch (sortField) {
					case "iuv":
						sortField = "IUV";
						break;
					case "area":
						sortField = "AREA";
						break;
					case "servizio":
						sortField = "SIGLA_SERVIZIO";
						break;
					case "codiceContesto":
						sortField = "CODICE_CONTESTO";
						break;
					case "idDominio":
						sortField = "ID_DOMINIO";
						break;
					case "statoPagamento":
						sortField = "STATO_PAGAMENTO";
						break;
					case "esitoPagamento":
						sortField = "ESITO_PAGAMENTO";
						break;
					case "idPagatore":
						sortField = "ID_PAGATORE";
						break;
					case "idVersante":
						sortField = "ID_VERSANTE";
						break;
					case "dataRicevuta":
						sortField = "DATA_RICEVUTA";
						break;
					case "dataRichiesta":
						sortField = "DATA_RICHIESTA";
						break;
					default:
						break;
					}

					filtro.setOrderBy(sortField);
					if (sortOrder == SortOrder.ASCENDING) {
						filtro.setOrderType("ASC");
					} else if (sortOrder == SortOrder.DESCENDING) {
						filtro.setOrderType("DESC");
					}
				}

				if (flussoFilter != null
						&& flussoFilter.getStatoPagamento() != null) {

					// log.info(flussoFilter.getStatoPagamento());
					flussoFilter.setStatoPagamento(flussoFilter
							.getStatoPagamento().replaceAll(" ", "_"));

				}

				if (flussoFilter != null
						&& flussoFilter.getEsitoPagamento() != null) {

					// log.info(flussoFilter.getEsitoPagamento());
					flussoFilter.setEsitoPagamento(flussoFilter
							.getEsitoPagamento().replaceAll(" ", "_"));

				}

				filtro.setDa(first + 1);
				filtro.setA(first + pageSize);

				if (pagatore != null && !pagatore.equals("")) {
					flussoFilter.setIdPagatore(pagatore);
					filtroAttivo = true;
				}

				if (versante != null && !versante.equals("")) {
					flussoFilter.setIdVersante(versante);
					filtroAttivo = true;

				}

				flussoFilter.setIdDominio((String) AppUtil
						.getInSessionMap("selectedEC"));// AppConf.getDominio().getIdDominio());

				filtro.setFlusso(flussoFilter);

				// List<Flusso> flussi = dao.selectFlussi(filtro);

				// listaStoricoPagamenti.setRowCount(dao.selectFlussiCount(filtro));

				// listaStoricoPagamenti.setRowCount(flussi.size());

				// listaStorico = new ArrayList<Flusso>(flussi);
				// //dao.selectFlussi(flist);

				FlussoData flussoData = new FlussoData();

				flussoData.setFiltroFlusso(filtro);

				flussoData = api.getFlussi(contentTypeJson, acceptLanguage,
						flussoData);

				listaStoricoPagamenti.setRowCount(flussoData.getFiltroFlusso()
						.getCount());

				listaStorico = flussoData.getFlussoList();

				return listaStorico;

			}

		};

		/*
		 * if(listaStoricoPagamenti!=null){ FiltroFlusso filtro = new
		 * FiltroFlusso();
		 * 
		 * Flusso flussoFilter = new Flusso();
		 * 
		 * filtro.setFlusso(flussoFilter);
		 * 
		 * listaStoricoPagamenti.setRowCount(dao.selectFlussiCount(filtro)); }
		 */

	}

	public void aggiornaStato(Flusso flusso, Integer index) {

		// log.info(flusso+" - "+index);

		// index non viene usato , se venisse usato dato il lazy load
		// bisognerebbe fare index = index %
		// listaStoricoPagamenti.getPageSize();

		AStatoRPTCopiaRT stato = null;

		try {
			NodoChiediStatoRPT arg0 = new NodoChiediStatoRPT();
			arg0.setCodiceContestoPagamento(flusso.getCodiceContesto());
			arg0.setIdentificativoDominio(flusso.getIdDominio());
			arg0.setIdentificativoIntermediarioPA(dominio
					.getIdIntermediarioPA());
			arg0.setIdentificativoStazioneIntermediarioPA(dominio
					.getIdStazioneIntermediarioPA());
			arg0.setIdentificativoUnivocoVersamento(flusso.getIuv());
			arg0.setPassword(dominio.getPwdPA());

			stato = service.nodoClient(true).nodoChiediStatoRPT(arg0);// (header,
																		// dominio.getPwdPA());
			log.info("fkjdsbhòkfjdshfòdsh");
			if (stato != null) {
				log.info(stato);
				esitoStatoRPTMap.put(flusso.getIdFlusso(),
						stato.getEsitoChiediStatoRPT());

			}

			if (stato != null && stato.getFaultBean() == null
					&& stato.getNodoChiediCopiaRTRisposta() != null) {
				NodoChiediCopiaRTRisposta ricevuta = stato
						.getNodoChiediCopiaRTRisposta();

				if (ricevuta.getFault() == null) {
					// if(ricevuta.getTipoFirma()!=null &&
					// ((ricevuta.getTipoFirma().equals("3") ||
					// ricevuta.getTipoFirma().equals("0"))) ){
					// String esito = elaboraEsito(ricevuta);
					aggiornaStorico();
					AppUtil.getCurrentInstance()
							.addMessage(
									"statoGenerale",
									new FacesMessage(
											FacesMessage.SEVERITY_INFO,
											"Info:",
											"Rivecuta aquisita, � possibile consultare l'esito nel pannello dello storico"));
					// }
					/*
					 * else if(ricevuta.getTipoFirma()!=null &&
					 * ricevuta.getTipoFirma().contains("DB-")){
					 * 
					 * AppUtil.getCurrentInstance().addMessage("statoGenerale",
					 * new FacesMessage(FacesMessage.SEVERITY_INFO, "Info:",
					 * "Rivecuta gi� elaborata, � possibile consultare l'esito nel pannello dello storico"
					 * ));
					 * 
					 * }
					 */
				} else {
					AppUtil.getCurrentInstance().addMessage(
							"statoGenerale",
							new FacesMessage(FacesMessage.SEVERITY_FATAL,
									"Errore di sistema:",
									"Errore nel recupero della ricevuta"));
				}

			} else if (stato != null && stato.getFaultBean() == null) {
				aggiornaStorico();
				AppUtil.getCurrentInstance().addMessage(
						"statoGenerale",
						new FacesMessage(FacesMessage.SEVERITY_INFO, "Info:",
								"Stato aggiornato"));

			}

			/*
			 * 
			 * if(listaPendenzeFiltrate == null) listaPendenze.set(index,
			 * flusso); else listaPendenzeFiltrate.set(index, flusso);
			 * 
			 * aggiornaPendenze();
			 */

		} catch (Exception e) {
			log.error(e, e);
			AppUtil.getCurrentInstance().addMessage(
					"statoGenerale",
					new FacesMessage(FacesMessage.SEVERITY_FATAL,
							"Errore di sistema:",
							"Riprovare in un altro momento"));
		}

	}

	public void aggiornaStatoCarrello(Flusso flusso) {

		AStatoRPTCopiaRTCarrello stato = null;

		try {
			ASelezionaStatoRPTCopiaRTCarrello arg0 = new ASelezionaStatoRPTCopiaRTCarrello();
			IntestazioneCarrelloPPT intestazioneCarrelloPPT = new IntestazioneCarrelloPPT();
			intestazioneCarrelloPPT.setIdentificativoCarrello(flusso
					.getIdCarrello());
			intestazioneCarrelloPPT.setIdentificativoIntermediarioPA(dominio
					.getIdIntermediarioPA());
			intestazioneCarrelloPPT
					.setIdentificativoStazioneIntermediarioPA(dominio
							.getIdStazioneIntermediarioPA());
			arg0.setIntestazioneCarrelloPPT(intestazioneCarrelloPPT);
			arg0.setPassword(dominio.getPwdPA());

			stato = service.nodoClient(true).nodoChiediStatoRPTCarrello(arg0);// (header,
																				// dominio.getPwdPA());

			if (stato != null) {

				esitoStatoRPTCarrelloMap.put(flusso.getIdCarrello(),
						stato.getStatoRPTCopiaRT());

			}

			if (stato != null && stato.getStatoRPTCopiaRT().size() > 0) {

				boolean isFaultFirst = true;
				boolean isNotFaultSecond = false;

				for (int i = 0; i < stato.getStatoRPTCopiaRT().size(); i++) {
					AStatoRPTCopiaRT statoECopia = stato.getStatoRPTCopiaRT()
							.get(i);

					if (statoECopia != null
							&& statoECopia.getFaultBean() == null
							&& statoECopia.getNodoChiediCopiaRTRisposta() != null) {

						if (statoECopia.getNodoChiediCopiaRTRisposta()
								.getFault() != null) {
							isFaultFirst = false;
						}

					} else if (statoECopia != null
							&& statoECopia.getFaultBean() == null) {
						isNotFaultSecond = true;

					}

				}

				if (!isFaultFirst) {
					AppUtil.getCurrentInstance().addMessage(
							"statoGenerale",
							new FacesMessage(FacesMessage.SEVERITY_FATAL,
									"Errore di sistema:",
									"Errore nel recupero della ricevute"));
				} else {
					aggiornaStorico();
					AppUtil.getCurrentInstance()
							.addMessage(
									"statoGenerale",
									new FacesMessage(
											FacesMessage.SEVERITY_INFO,
											"Info:",
											"Rivecute aquisite, � possibile consultare l'esito nel pannello dello storico"));
				}

				if (isNotFaultSecond) {
					aggiornaStorico();
					AppUtil.getCurrentInstance().addMessage(
							"statoGenerale",
							new FacesMessage(FacesMessage.SEVERITY_INFO,
									"Info:", "Stati aggiornati"));
				}

			}

		} catch (Exception e) {
			log.error(e, e);
			AppUtil.getCurrentInstance().addMessage(
					"statoGenerale",
					new FacesMessage(FacesMessage.SEVERITY_FATAL,
							"Errore di sistema:",
							"Riprovare in un altro momento"));
		}

	}

	public void chiediRicevuta(Flusso flusso, Integer index) {
		// index non viene usato , se venisse usato dato il lazy load
		// bisognerebbe fare index = index %
		// listaStoricoPagamenti.getPageSize();

		NodoChiediCopiaRTRisposta ricevuta = null;

		try {
			NodoChiediCopiaRT rt = new NodoChiediCopiaRT();

			rt.setCodiceContestoPagamento(flusso.getCodiceContesto());
			rt.setIdentificativoDominio(flusso.getIdDominio());
			rt.setIdentificativoIntermediarioPA(dominio.getIdIntermediarioPA());
			rt.setIdentificativoStazioneIntermediarioPA(dominio
					.getIdStazioneIntermediarioPA());
			rt.setIdentificativoUnivocoVersamento(flusso.getIuv());
			rt.setPassword(dominio.getPwdPA());
			ricevuta = service.nodoClient(true).nodoChiediCopiaRT(rt);// header,
																		// dominio.getPwdPA());

			if (ricevuta.getFault() == null) {
				// if(ricevuta.getTipoFirma()!=null &&
				// ((ricevuta.getTipoFirma().equals("3") ||
				// ricevuta.getTipoFirma().equals("0"))) ){
				// String esito = elaboraEsito(ricevuta);
				aggiornaStorico();
				AppUtil.getCurrentInstance()
						.addMessage(
								"statoGenerale",
								new FacesMessage(
										FacesMessage.SEVERITY_INFO,
										"Info:",
										"Rivecuta aquisita, � possibile consultare l'esito nel pannello dello storico"));
				// }

				/*
				 * else if(ricevuta.getTipoFirma()!=null &&
				 * ricevuta.getTipoFirma().contains("DB-")){
				 * 
				 * AppUtil.getCurrentInstance().addMessage("statoGenerale", new
				 * FacesMessage(FacesMessage.SEVERITY_INFO, "Info:",
				 * "Rivecuta gi� elaborata, � possibile consultare l'esito nel pannello dello storico"
				 * ));
				 * 
				 * }
				 */
			} else {
				AppUtil.getCurrentInstance()
						.addMessage(
								"statoGenerale",
								new FacesMessage(FacesMessage.SEVERITY_FATAL,
										"Errore:",
										"Errore nel recupero della ricevuta"));
			}

		} catch (SogeiPaySystemException e) {
			log.error(e, e);
			AppUtil.getCurrentInstance().addMessage(
					"statoGenerale",
					new FacesMessage(FacesMessage.SEVERITY_FATAL,
							"Errore di sistema:", e.getDescrizione()));
		} catch (Exception e) {
			log.error(e, e);
			AppUtil.getCurrentInstance().addMessage(
					"statoGenerale",
					new FacesMessage(FacesMessage.SEVERITY_FATAL,
							"Errore di sistema:",
							"Riprovare in un altro momento"));
		}
	}

	public void chiediRicevutaCarrello(Flusso flusso) {

		ANodoChiediCopiaRTCarrello ricevuta = null;

		try {

			ASelezionaStatoRPTCopiaRTCarrello arg0 = new ASelezionaStatoRPTCopiaRTCarrello();
			IntestazioneCarrelloPPT intestazioneCarrelloPPT = new IntestazioneCarrelloPPT();
			intestazioneCarrelloPPT.setIdentificativoCarrello(flusso
					.getIdCarrello());
			intestazioneCarrelloPPT.setIdentificativoIntermediarioPA(dominio
					.getIdIntermediarioPA());
			intestazioneCarrelloPPT
					.setIdentificativoStazioneIntermediarioPA(dominio
							.getIdStazioneIntermediarioPA());
			arg0.setIntestazioneCarrelloPPT(intestazioneCarrelloPPT);
			arg0.setPassword(dominio.getPwdPA());

			ricevuta = service.nodoClient(true).nodoChiediCopiaRTCarrello(arg0);// header,
																				// dominio.getPwdPA());

			if (ricevuta != null
					&& ricevuta.getNodoChiediCopiaRTRisposta().size() > 0) {

				boolean isFault = true;

				for (int i = 0; i < ricevuta.getNodoChiediCopiaRTRisposta()
						.size(); i++) {

					NodoChiediCopiaRTRisposta nodoChiediCopiaRTRisposta = ricevuta
							.getNodoChiediCopiaRTRisposta().get(i);

					if (!(nodoChiediCopiaRTRisposta != null && nodoChiediCopiaRTRisposta
							.getFault() == null)) {

						isFault = false;

					}

				}

				if (!isFault) {
					AppUtil.getCurrentInstance().addMessage(
							"statoGenerale",
							new FacesMessage(FacesMessage.SEVERITY_FATAL,
									"Errore di sistema:",
									"Errore nel recupero della ricevute"));
				} else {
					aggiornaStorico();
					AppUtil.getCurrentInstance()
							.addMessage(
									"statoGenerale",
									new FacesMessage(
											FacesMessage.SEVERITY_INFO,
											"Info:",
											"Rivecute aquisite, � possibile consultare l'esito nel pannello dello storico"));
				}
			}

		} catch (SogeiPaySystemException e) {
			log.error(e, e);
			AppUtil.getCurrentInstance().addMessage(
					"statoGenerale",
					new FacesMessage(FacesMessage.SEVERITY_FATAL,
							"Errore di sistema:", e.getDescrizione()));
		} catch (Exception e) {
			log.error(e, e);
			AppUtil.getCurrentInstance().addMessage(
					"statoGenerale",
					new FacesMessage(FacesMessage.SEVERITY_FATAL,
							"Errore di sistema:",
							"Riprovare in un altro momento"));
		}
	}

	public void aggiornaStorico() {
		log.info("aggiornaStorico");
		listaStoricoPagamenti = null;
		listaStorico = null;
		init();
	}

	public void preProcessPDF(Object document) {
		Document doc = (Document) document;

		doc.setPageSize(PageSize.A4.rotate());

		HeaderFooter footer = new HeaderFooter(new Phrase(""), true);
		doc.setFooter(footer);

	}

	public LazyDataModel<Flusso> getListaStoricoPagamenti() {
		return listaStoricoPagamenti;
	}

	public void setListaStoricoPagamenti(
			LazyDataModel<Flusso> listaStoricoPagamenti) {
		this.listaStoricoPagamenti = listaStoricoPagamenti;
	}

	public GiornaleDegliEventiService getGiornaleDegliEventiService() {
		return giornaleDegliEventiService;
	}

	public void setGiornaleDegliEventiService(
			GiornaleDegliEventiService giornaleDegliEventiService) {
		this.giornaleDegliEventiService = giornaleDegliEventiService;
	}

	public EsitoChiediStatoRPT getEsitoStatoRPT(Long idFlusso) {

		// giornaleDegliEventiService.setRenderStati(false);
		// log.info(idFlusso);
		EsitoChiediStatoRPT erpt = esitoStatoRPTMap.get(idFlusso);

		log.info(erpt);

		return erpt;

	}

	public void cerca() {

		listaStoricoPagamenti = null;
		listaStorico = null;

		init();

	}

	public void eliminaFiltri() {

		monitorStatService.setServiziList(monitorStatService
				.getMapAreaCategorie().get(allAree));
		filtroAttivo = false;
		listaStoricoPagamenti = null;
		listaStorico = null;
		pagatore = "";
		versante = "";
		init();

	}

	/**
	 * @return the dettaglioFlosso
	 */
	public Flusso getDettaglioFlusso() {
		return dettaglioFlusso;
	}

	/**
	 * @param dettaglioFlosso
	 *            the dettaglioFlosso to set
	 */
	public void setDettaglioFlusso(Flusso dettaglioFlusso) {
		this.dettaglioFlusso = dettaglioFlusso;
	}

	/**
	 * @return the pagatore
	 */
	public String getPagatore() {
		return pagatore;
	}

	/**
	 * @param pagatore
	 *            the pagatore to set
	 */
	public void setPagatore(String pagatore) {
		this.pagatore = pagatore;
	}

	/**
	 * @return the versante
	 */
	public String getVersante() {
		return versante;
	}

	/**
	 * @param versante
	 *            the versante to set
	 */
	public void setVersante(String versante) {
		this.versante = versante;
	}

	/**
	 * @return monitorStatService
	 */
	public MonitorStatService getMonitorStatService() {
		return monitorStatService;
	}

	/**
	 * @param monitorStatService
	 *            monitorStatService da impostare
	 */
	public void setMonitorStatService(MonitorStatService monitorStatService) {
		this.monitorStatService = monitorStatService;
	}

	/*
	 * public List<Servizo> getServizi() { if(servizi==null){ servizi=
	 * dao.selectServizi(); }
	 * 
	 * return servizi; }
	 */

}
