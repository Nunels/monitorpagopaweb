package it.sogei.pagopa.monitor.service.handler;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.ConverterException;
import javax.faces.convert.DateTimeConverter;
import javax.faces.convert.FacesConverter;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

@FacesConverter("XMLGregorianCalendarConverter")
public class XMLGregorianCalendarConverter extends DateTimeConverter {
	private static final TimeZone DEFAULT_TIME_ZONE = TimeZone.getDefault();
	private String dateStyle = "default";
	private Locale locale = null;
	private String pattern = null;
	private String timeStyle = "default";
	private TimeZone timeZone = DEFAULT_TIME_ZONE;
	private String type = "date";
	

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if(value == null || value.equals("")) {
            return null;
        }
        
        
        
        Date date = (Date) super.getAsObject(context, component, value);
        GregorianCalendar gc = new GregorianCalendar();
        gc.setTime(date);
        XMLGregorianCalendar xgc = null;
		try {
			xgc = DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
		} catch (DatatypeConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        if(xgc == null) {
            throw new ConverterException(new FacesMessage("Error converting to XMLGregorianCalendar."));
        }
        return xgc;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (!(value instanceof XMLGregorianCalendar) || (value == null)) {
            return null;
        }
        
        Map<String, Object> attributes = component.getAttributes();
        if(attributes.containsKey("pattern")){
            pattern = (String) attributes.get("pattern");
        }
        setPattern(pattern);
        if(attributes.containsKey("locale")){
            locale = (Locale) attributes.get("locale");
        }
        setLocale(locale);
        if(attributes.containsKey("timeZone")){
            timeZone = (TimeZone) attributes.get("timeZone");
        }
        setTimeZone(timeZone);
        if(attributes.containsKey("dateStyle")){
            dateStyle = (String) attributes.get("dateStyle");
        }
        setDateStyle(dateStyle);
        if(attributes.containsKey("timeStyle")){
            timeStyle = (String) attributes.get("timeStyle");
        }
        setTimeStyle(timeStyle);
        if(attributes.containsKey("type")){
            type = (String) attributes.get("type");
        }
        setType(type);
        
       
        Date date = ((XMLGregorianCalendar)value).toGregorianCalendar().getTime();
        date = DateUtil.getDate(date);
        return super.getAsString(context, component, date);
    }

}