package it.sogei.pagopa.monitor.service.handler;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.lang3.time.DateUtils;

@ManagedBean
@SessionScoped
public class DateUtil implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4738909188391688952L;
	
	public TimeZone getTimeZoneRome(){
		return TimeZone.getTimeZone("Europe/Rome");
	}
	
	public static String getStringDate() {

		SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
		return dateFormat.format(getDate());
	}
	
	
	public static String getStringDateMinus(Date data) {

		SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
		return dateFormat.format(data);
	}
	
	

	public static int getMonthInt(Date date) {

		SimpleDateFormat dateFormat = new SimpleDateFormat("MM");
		return Integer.parseInt(dateFormat.format(date));
	}

	/**
	 * Make an int Year from a date
	 */
	public static int getYearInt(Date date) {

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy");
		return Integer.parseInt(dateFormat.format(date));
	}

	public static XMLGregorianCalendar toXmlGregorianCalendar(Date data) {
		GregorianCalendar gregory = new GregorianCalendar();
		// gregory.setTimeZone(TimeZone.getTimeZone("Europe/Rome"));
		gregory.setTime(data);

		XMLGregorianCalendar calendar = null;
		try {
			calendar = DatatypeFactory.newInstance().newXMLGregorianCalendar( gregory);
		} catch (DatatypeConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return calendar;
	}

	public static XMLGregorianCalendar toXmlGregorianCalendar(Date data, String pattern) {

		SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);

		String strDate = dateFormat.format(data);

		GregorianCalendar gregory = new GregorianCalendar();
		// gregory.setTimeZone(TimeZone.getTimeZone("Europe/Rome"));
		try {
			gregory.setTime(dateFormat.parse(strDate));
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		XMLGregorianCalendar calendar = null;
		try {
			calendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregory);
		} catch (DatatypeConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return calendar;
	}

	public static XMLGregorianCalendar toXMLGregorianCalendarWithoutTimeStamp( Date data) {

		GregorianCalendar cal = new GregorianCalendar();
		XMLGregorianCalendar xmlGregorianCalendar = null;
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		String strDate = df.format(data);

		try {
			cal.setTime(df.parse(strDate));
			xmlGregorianCalendar = DatatypeFactory.newInstance()
					.newXMLGregorianCalendarDate(cal.get(Calendar.YEAR),
							cal.get(Calendar.MONTH) + 1,
							cal.get(Calendar.DAY_OF_MONTH),
							DatatypeConstants.FIELD_UNDEFINED);

		} catch (DatatypeConfigurationException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return xmlGregorianCalendar;
	}

	public static Date toDate(XMLGregorianCalendar mVXMLCal) {
		return mVXMLCal.toGregorianCalendar().getTime();
	}

	/**
	 * Restituisce in un oggetto java.util.Date la data odierna nel TimeZone
	 * ITALIANO.
	 * 
	 * @return
	 */
	public static Date getDate() {

		String jvmTimeZone = TimeZone.getDefault().getID();
		Date dataConvertita = new Date(System.currentTimeMillis());

		if (!"Europe/Rome".equals(jvmTimeZone)) {

			dataConvertita = convertTimeZone(dataConvertita, TimeZone.getDefault(), TimeZone.getTimeZone("Europe/Rome"));
		}

		return dataConvertita;
	}

	/**
	 * Converte in un oggetto java.util.Date la data in input secondo il pattern
	 * in input nel TimeZone ITALIANO.
	 * 
	 * @param sData
	 * @param pattern
	 * @return
	 * @throws ParseException
	 */
	public static Date getDate(String sData, String pattern)
			throws ParseException {

		String jvmTimeZone = TimeZone.getDefault().getID();
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		sdf.setTimeZone(TimeZone.getTimeZone("Europe/Rome"));

		Date dataConvertita = sdf.parse(sData);

		if (!"Europe/Rome".equals(jvmTimeZone)) {

			dataConvertita = convertTimeZone(dataConvertita,
					TimeZone.getDefault(), TimeZone.getTimeZone("Europe/Rome"));
		}

		return dataConvertita;
	}

	/**
	 * Restituisce in un oggetto java.util.Date la data odierna nel TimeZone
	 * ITALIANO.
	 * 
	 * @return
	 */
	public static Date getDate(Date dataDaConvertire) {

		String jvmTimeZone = TimeZone.getDefault().getID();

		if (!"Europe/Rome".equals(jvmTimeZone)) {

			dataDaConvertire = convertTimeZone(dataDaConvertire, TimeZone.getDefault(), TimeZone.getTimeZone("Europe/Rome"));
		}

		return dataDaConvertire;
	}

	/**
	 * Converts the given <code>date</code> from the <code>fromTimeZone</code>
	 * to the <code>toTimeZone</code>. Since java.util.Date has does not really
	 * store time zome information, this actually converts the date to the date
	 * that it would be in the other time zone.
	 * 
	 * @param date
	 * @param fromTimeZone
	 * @param toTimeZone
	 * @return
	 */
	private static Date convertTimeZone(Date date, TimeZone fromTimeZone, TimeZone toTimeZone) {
		long fromTimeZoneOffset = getTimeZoneUTCAndDSTOffset(date, fromTimeZone);
		long toTimeZoneOffset = getTimeZoneUTCAndDSTOffset(date, toTimeZone);

		return new Date(date.getTime() + (toTimeZoneOffset - fromTimeZoneOffset));
	}

	/**
	 * Calculates the offset of the <code>timeZone</code> from UTC, factoring in
	 * any additional offset due to the time zone being in daylight savings time
	 * as of the given <code>date</code>.
	 * 
	 * @param date
	 * @param timeZone
	 * @return
	 */
	private static long getTimeZoneUTCAndDSTOffset(Date date, TimeZone timeZone) {
		long timeZoneDSTOffset = 0;
		if (timeZone.inDaylightTime(date)) {
			timeZoneDSTOffset = timeZone.getDSTSavings();
		}

		return timeZone.getRawOffset() + timeZoneDSTOffset;
	}

	public boolean filterByDate(Object value, Object filter, Locale locale) {

		if (filter == null) {
			return true;
		}

		if (value == null) {
			return false;
		}

		return DateUtils.truncatedEquals((Date) filter, (Date) value, Calendar.DATE);
	}


	

}
