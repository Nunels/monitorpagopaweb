package it.sogei.pagopa.monitor.service;


import it.sogei.ssi.util.DateUtil;

import java.io.Serializable;
import java.util.Date;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.apache.log4j.Logger;


@ManagedBean
@ViewScoped
public class PagoPaService implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8768370770427044050L;
	private static Logger log = Logger.getLogger(PagoPaService.class);
	
	
	private Date dataCorrente = DateUtil.getDate();
	
	private String codiceContesto =null;


	private String iuv = null;
	
	private String dominio = null;




	public Date getDataCorrente() {
		return dataCorrente;
	}

	public void setDataCorrente(Date dataCorrente) {
		this.dataCorrente = dataCorrente;
	}

	public String getIuv() {
		return iuv;
	}

	public void setIuv(String iuv) {
		this.iuv = iuv;
	}

	public String getCodiceContesto() {
		return codiceContesto;
	}

	public void setCodiceContesto(String codiceContesto) {
		this.codiceContesto = codiceContesto;
	}

	public String getDominio() {
		return dominio;
	}

	public void setDominio(String dominio) {
		this.dominio = dominio;
	}




	
	
	

}
