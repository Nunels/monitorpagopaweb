package it.sogei.pagopa.monitor.service;

import it.sogei.pagopa.monitor.model.Elemento;
import it.sogei.pagopa.monitor.model.FiltroElemento;
import it.sogei.pagopa.monitor.model.Sottoelemento;
import it.sogei.test.TestElementiServiceApiTest;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;

@ManagedBean
@SessionScoped
public class TestService implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7000931289803372755L;

	private static Logger log = Logger.getLogger(TestService.class);

	private String intError = "Errore: massimo 100 elementi";

	// private List<Elemento> listaElementi = ElementoInitializer.initializer();
	private List<Elemento> listaElementi = new ArrayList<Elemento>();
	private List<Sottoelemento> listaSottoelementi = new ArrayList<>();
	private String test = "fail";
	private boolean rendered = false;
	private Elemento elementoSelezionato = new Elemento();
	private Elemento nuovoElemento = new Elemento();
	private Elemento elementoDaEliminare = new Elemento();
	private Elemento elementoDaInserire = new Elemento();
	private Elemento elementoDaAggiornare = new Elemento();

	private FiltroElemento filtroElemento = new FiltroElemento();

	private boolean result = true;

	private Integer numeroElementi;
	private Date dataCorrente = new Date();

	private String nome = null;
	private String descrizione = null;
	private Long id = null;
	private Date data;

	private TestElementiServiceApiTest api = new TestElementiServiceApiTest();

	private static final String contentTypeJson = "application/json";
	private static final String acceptLanguage = "it";

	@PostConstruct
	private void init() {
		// listaElementi = ElementoInitializer.initializer();
		if (api.welcomeTest(contentTypeJson, acceptLanguage)) {
			test = "OK";
			listaElementi = api.getElementi(contentTypeJson, acceptLanguage);
		}

		// listaElementi = api.generaElementi(contentTypeJson, acceptLanguage,
		// 10);
		// System.out.println(listaElementi.get(0).getData().toString());

	}

	public void add() {
		result = true;
		elementoDaInserire = new Elemento();
		elementoDaInserire.setNome(nome);
		elementoDaInserire.setData(data);
		elementoDaInserire.setDescrizione(descrizione);

		result = api.addElemento(contentTypeJson, acceptLanguage,
				elementoDaInserire);

		added(result);
		init();

	}

	public void added(boolean result) {
		if (result) {
			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_INFO, "Success",
							"Elemento aggiunto correttamente."));

		} else {
			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Fail",
							"Errore nell'aggiunta dell'elemento."));

		}
	}

	public void update() {
		result = true;
		result = api.updateElemento(contentTypeJson, acceptLanguage,
				elementoDaAggiornare);
		updated(result);

	}

	public void updated(boolean result) {
		if (result) {
			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_INFO, "Success",
							"Elemento modificato correttamente."));

		} else {
			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Fail",
							"Errore nella modifica dell'elemento."));

		}
	}

	public void delete() {
		result = true;
		result = api.deleteElemento(contentTypeJson, acceptLanguage,
				elementoDaEliminare);
		deleted(result);
		elementoDaEliminare = new Elemento();
		init();
	}

	public void deleted(boolean result) {
		if (result) {
			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_INFO, "Success",
							"Elemento eliminato."));

		} else {
			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Fail",
							"Errore nell'eliminazione dell'elemento."));

		}
	}

	public void svuotaElemento() {
		elementoSelezionato = new Elemento();
	}

	public void getDettagli() {
		nuovoElemento = api.getDettagli(contentTypeJson, acceptLanguage,
				elementoSelezionato);
		for (Elemento el : listaElementi) {
			if (el.getId().equals(nuovoElemento.getId())) {
				el.setDescrizione(nuovoElemento.getDescrizione());
				el = nuovoElemento;
			}

		}
	}

	public void generaElementi() {
		listaElementi = api.generaElementi(contentTypeJson, acceptLanguage,
				numeroElementi);
	}

	public void eliminaFiltri() {
		filtroElemento = new FiltroElemento();
		init();
	}

	public void cerca() {

		if (!filtroElemento.getDescrizione().equals("")
				|| !filtroElemento.getNome().equals("")
				|| !filtroElemento.get_idString().equals("")
				|| filtroElemento.getData() != null) {

			if (!filtroElemento.get_idString().equals(""))
				filtroElemento.setId(Long.parseLong(filtroElemento
						.get_idString()));

			listaElementi = api.getElementiFilter(contentTypeJson,
					acceptLanguage, filtroElemento);
		} else {
			init();
		}

		System.out.println("cerca_" + filtroElemento.getId());
		System.out.println("cerca_" + filtroElemento.getNome());
		System.out.println("cerca_" + filtroElemento.getDescrizione());
		System.out.println("cerca_" + filtroElemento.getData());

	}

	public void getSottoelementiById() {
		listaSottoelementi = api.getSottoelementiById(contentTypeJson,
				acceptLanguage, elementoSelezionato);

	}

	public void buttonAction() {
		addMessage("Ciao!");
	}

	public void intError() {
		addMessage("Errore: � possibile generare da 0 a 100 elementi");
	}

	public void addMessage(String summary) {
		FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO,
				summary, null);
		FacesContext.getCurrentInstance().addMessage(null, message);
	}

	public List<Elemento> getListaElementi() {
		return listaElementi;
	}

	public void setListaElementi(List<Elemento> listaElementi) {
		this.listaElementi = listaElementi;
	}

	public String getTest() {
		return test;
	}

	public void setTest(String test) {
		this.test = test;
	}

	public boolean isRendered() {
		return rendered;
	}

	public void setRendered(boolean rendered) {
		this.rendered = rendered;
	}

	public Elemento getElementoSelezionato() {
		return elementoSelezionato;
	}

	public void setElementoSelezionato(Elemento elementoSelezionato) {
		this.elementoSelezionato = elementoSelezionato;
	}

	public Integer getNumeroElementi() {
		return numeroElementi;
	}

	public void setNumeroElementi(Integer numeroElementi) {
		this.numeroElementi = numeroElementi;
	}

	public String getIntError() {
		return intError;
	}

	public void setIntError(String intError) {
		this.intError = intError;
	}

	public Date getDataCorrente() {
		return dataCorrente;
	}

	public void setDataCorrente(Date dataCorrente) {
		this.dataCorrente = dataCorrente;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescrizione() {
		return descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Elemento getNuovoElemento() {
		return nuovoElemento;
	}

	public void setNuovoElemento(Elemento nuovoElemento) {
		this.nuovoElemento = nuovoElemento;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public List<Sottoelemento> getListaSottoelementi() {
		return listaSottoelementi;
	}

	public void setListaSottoelementi(List<Sottoelemento> listaSottoelementi) {
		this.listaSottoelementi = listaSottoelementi;
	}

	public Elemento getElementoDaEliminare() {
		return elementoDaEliminare;
	}

	public void setElementoDaEliminare(Elemento elementoDaEliminare) {
		this.elementoDaEliminare = elementoDaEliminare;
	}

	public boolean isResult() {
		return result;
	}

	public void setResult(boolean result) {
		this.result = result;
	}

	public Elemento getElementoDaInserire() {
		return elementoDaInserire;
	}

	public void setElementoDaInserire(Elemento elementoDaInserire) {
		this.elementoDaInserire = elementoDaInserire;
	}

	public Elemento getElementoDaAggiornare() {
		return elementoDaAggiornare;
	}

	public void setElementoDaAggiornare(Elemento elementoDaAggiornare) {
		this.elementoDaAggiornare = elementoDaAggiornare;
	}

	public FiltroElemento getFiltroElemento() {
		return filtroElemento;
	}

	public void setFiltroElemento(FiltroElemento filtroElemento) {
		this.filtroElemento = filtroElemento;
	}

}
