package it.sogei.pagopa.monitor.service;

import it.sogei.pay.system.model.external.Flusso;
import it.sogei.pay.system.model.external.giornale.FiltroflussoGiornaleEventi;
import it.sogei.pay.system.model.external.giornale.FlussoGiornaleEventi;
import it.sogei.pay.system.model.external.giornale.GiornaleEventi;
import it.sogei.paysystem.monitor.PaySystemMonitorServiceApiTest;
import it.sogei.ssi.util.AppUtil;
import it.sogei.ssi.util.DateUtil;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.primefaces.PrimeFaces;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.lowagie.text.Document;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.PageSize;
import com.lowagie.text.Phrase;

@ManagedBean
@SessionScoped
public class GiornaleDegliEventiService implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6609076186196171931L;

	private static Logger log = Logger
			.getLogger(GiornaleDegliEventiService.class);

	// private String utente = null;

	private List<GiornaleEventi> listaGiornale = null;

	private List<GiornaleEventi> listaGiornaleDegliEventiPerPagamento = null;

	private LazyDataModel<GiornaleEventi> listaGiornaleDegliEventi = null;

	//
	private List<it.sogei.pagopa.monitor.model.GiornaleEventi> lista = null;

	//

	private GiornaleEventi dettaglioEvento = null;

	private Flusso dettaglioFlusso = null;

	PrimeFaces instance = PrimeFaces.current();

	private boolean renderStati = false;

	private PaySystemMonitorServiceApiTest api = new PaySystemMonitorServiceApiTest();

	private String esito = "";
	private String tipoEvento = "";
	private boolean filtroAttivo = false;

	private static final String contentTypeJson = "application/json";
	private static final String acceptLanguage = "it";

	@ManagedProperty("#{monitorStatService}")
	private MonitorStatService monitorStatService;

	public GiornaleDegliEventiService() {
	}

	// /

	@PostConstruct
	private void init() {

		listaGiornaleDegliEventi = new LazyDataModel<GiornaleEventi>() {
			@Override
			// public List<GiornaleEventi> load(int first, int pageSize,
			public List<GiornaleEventi> load(int first, int pageSize,
					String sortField, SortOrder sortOrder,
					Map<String, Object> filters) {

				Object dOraEvento = filters.get("dataOraEvento");
				filters.remove("dataOraEvento");

				FiltroflussoGiornaleEventi giornaleFilter = new FiltroflussoGiornaleEventi();
				GiornaleEventi giornaleEventi = new GiornaleEventi();
				if (filters != null && filters.size() > 0) {

					try {
						BeanUtils.populate(giornaleEventi,
								new HashMap<String, Object>(filters));
					} catch (IllegalAccessException | InvocationTargetException e) {
						e.printStackTrace();
					}

				}

				if (dOraEvento != null) {
					log.info((Date) dOraEvento);
					giornaleEventi.setDataOraEvento(DateUtil
							.toXmlGregorianCalendar((Date) dOraEvento));
				} else {
					giornaleEventi.setDataOraEvento(null);
				}

				if (sortField != null) {

					switch (sortField) {
					case "esito":
						sortField = "ESITO";
						break;
					case "tipoEvento":
						sortField = "TIPO_EVENTO";
						break;
					case "dataOraEvento":
						sortField = "DATA_ORA_EVENTO";
						break;
					default:
						break;
					}

					giornaleFilter.setOrderBy(sortField);
					if (sortOrder == SortOrder.ASCENDING) {
						giornaleFilter.setOrderType("ASC");
					} else if (sortOrder == SortOrder.DESCENDING) {
						giornaleFilter.setOrderType("DESC");
					}
				}

				if (giornaleEventi != null && giornaleEventi.getEsito() != null) {

					// log.info(flussoFilter.getEsitoPagamento());
					giornaleEventi.setEsito(giornaleEventi.getEsito()
							.replaceAll(" ", "_"));

				}

				giornaleFilter.setDa(first + 1);
				giornaleFilter.setA(first + pageSize);

				// listaGiornaleDegliEventi.setRowCount(dao.selectGiornaleCount(giornaleFilter));

				// listaGiornale = dao.selectGiornale(giornaleFilter);

				// listaGiornaleDegliEventi.setRowCount(listaGiornale.size());

				if (esito != null && !esito.equals("")) {

					esito = esito.replaceAll(" ", "_");
					giornaleEventi.setEsito(esito);
					setFiltroAttivo(true);
				}

				if (tipoEvento != null && !tipoEvento.equals("")) {
					giornaleEventi.setTipoEvento(tipoEvento);

					setFiltroAttivo(true);

				}

				giornaleEventi.setIdDominio((String) AppUtil
						.getInSessionMap("selectedEC"));

				giornaleFilter.setGiornaleEventi(giornaleEventi);

				FlussoGiornaleEventi flussoGiornaleEventi = new FlussoGiornaleEventi();
				flussoGiornaleEventi
						.setFiltroflussoGiornaleEventi(giornaleFilter);

				flussoGiornaleEventi = api.getGiornale(contentTypeJson,
						acceptLanguage, flussoGiornaleEventi);

				listaGiornaleDegliEventi.setRowCount(flussoGiornaleEventi
						.getFiltroflussoGiornaleEventi().getCount());

				instance.scrollTo("statoGenerale");

				return flussoGiornaleEventi.getGiornaleList();

			}

		};

		/*
		 * FiltroflussoGiornaleEventi giornaleFilter = new
		 * FiltroflussoGiornaleEventi(); GiornaleEventi giornaleEventi=new
		 * GiornaleEventi(); giornaleFilter.setGiornaleEventi(giornaleEventi);
		 * 
		 * FlussoGiornaleEventi flussoGiornaleEventi=new FlussoGiornaleEventi();
		 * flussoGiornaleEventi.setFiltroflussoGiornaleEventi(giornaleFilter);
		 * flussoGiornaleEventi= api.getGiornale(contentTypeJson,
		 * acceptLanguage, flussoGiornaleEventi);
		 * listaGiornaleDegliEventi.setRowCount
		 * (flussoGiornaleEventi.getFiltroflussoGiornaleEventi().getCount());
		 */
	}

	// @PostConstruct
	private void _init() {

		//
		lista = new ArrayList<>();
		ListUtility listUtility = new ListUtility();
		lista = listUtility.aggiungi(lista);
		//

		/*
		 * FiltroflussoGiornaleEventi giornaleFilter = new
		 * FiltroflussoGiornaleEventi(); GiornaleEventi giornaleEventi=new
		 * GiornaleEventi(); giornaleFilter.setGiornaleEventi(giornaleEventi);
		 * 
		 * FlussoGiornaleEventi flussoGiornaleEventi=new FlussoGiornaleEventi();
		 * flussoGiornaleEventi.setFiltroflussoGiornaleEventi(giornaleFilter);
		 * flussoGiornaleEventi= api.getGiornale(contentTypeJson,
		 * acceptLanguage, flussoGiornaleEventi);
		 * listaGiornaleDegliEventi.setRowCount
		 * (flussoGiornaleEventi.getFiltroflussoGiornaleEventi().getCount());
		 */
	}

	public void preProcessPDF(Object document) {
		Document doc = (Document) document;

		doc.setPageSize(PageSize.A4.rotate());

		HeaderFooter footer = new HeaderFooter(new Phrase(""), true);
		doc.setFooter(footer);

	}

	public void dataDialog(Flusso flusso) {
		// log.info("Row State " + event.getVisibility());
		// log.info("dsklhfldskhbflidskflkhvjfdspghdsoih");

		GiornaleEventi giornale = new GiornaleEventi();
		giornale.setIuv(flusso.getIuv());
		giornale.setCodContesto(flusso.getCodiceContesto());
		giornale.setIdDominio(flusso.getIdDominio());

		dettaglioFlusso = flusso;

		renderStati = true;

		// listaGiornaleDegliEventiPerPagamento =
		// dao.selectGiornalePerPagamento(giornale);

		listaGiornaleDegliEventiPerPagamento = api.getGiornalePerPagamento(
				contentTypeJson, acceptLanguage, giornale);

		/*
		 * }else{
		 * 
		 * renderStati = false; }
		 */

	}

	public void eliminaFiltri() {

		listaGiornaleDegliEventi = null;
		listaGiornaleDegliEventiPerPagamento = null;
		esito = "";
		tipoEvento = "";
		init();

	}

	public void cerca() {

		listaGiornaleDegliEventi = null;
		listaGiornaleDegliEventiPerPagamento = null;

		init();

	}

	public List<GiornaleEventi> getListaGiornaleDegliEventiPerPagamento() {
		return listaGiornaleDegliEventiPerPagamento;
	}

	public void setListaGiornaleDegliEventiPerPagamento(
			List<GiornaleEventi> listaGiornaleDegliEventiPerPagamento) {
		this.listaGiornaleDegliEventiPerPagamento = listaGiornaleDegliEventiPerPagamento;
	}

	public LazyDataModel<GiornaleEventi> getListaGiornaleDegliEventi() {
		return listaGiornaleDegliEventi;
	}

	public void setListaGiornaleDegliEventi(
			LazyDataModel<GiornaleEventi> listaGiornaleDegliEventi) {
		this.listaGiornaleDegliEventi = listaGiornaleDegliEventi;
	}

	public boolean isRenderStati() {
		return renderStati;
	}

	public void setRenderStati(boolean renderStati) {
		this.renderStati = renderStati;
	}

	/**
	 * @return the dettaglioEvento
	 */
	public GiornaleEventi getDettaglioEvento() {
		return dettaglioEvento;
	}

	/**
	 * @param dettaglioEvento
	 *            the dettaglioEvento to set
	 */
	public void setDettaglioEvento(GiornaleEventi dettaglioEvento) {
		this.dettaglioEvento = dettaglioEvento;
	}

	/**
	 * @return the dettaglioFlusso
	 */
	public Flusso getDettaglioFlusso() {
		return dettaglioFlusso;
	}

	/**
	 * @param dettaglioFlusso
	 *            the dettaglioFlusso to set
	 */
	public void setDettaglioFlusso(Flusso dettaglioFlusso) {
		this.dettaglioFlusso = dettaglioFlusso;
	}

	/**
	 * @return the tipoEvento
	 */
	public String getTipoEvento() {
		return tipoEvento;
	}

	/**
	 * @param tipoEvento
	 *            the tipoEvento to set
	 */
	public void setTipoEvento(String tipoEvento) {
		this.tipoEvento = tipoEvento;
	}

	public boolean isFiltroAttivo() {
		return filtroAttivo;
	}

	public void setFiltroAttivo(boolean filtroAttivo) {
		this.filtroAttivo = filtroAttivo;
	}

	/**
	 * @return the esito
	 */
	public String getEsito() {
		return esito;
	}

	/**
	 * @param esito
	 *            the esito to set
	 */
	public void setEsito(String esito) {
		this.esito = esito;
	}

	/**
	 * @return monitorStatService
	 */
	public MonitorStatService getMonitorStatService() {
		return monitorStatService;
	}

	/**
	 * @param monitorStatService
	 *            monitorStatService da impostare
	 */
	public void setMonitorStatService(MonitorStatService monitorStatService) {
		this.monitorStatService = monitorStatService;
	}

}
