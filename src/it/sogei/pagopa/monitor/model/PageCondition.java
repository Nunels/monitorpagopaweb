package it.sogei.pagopa.monitor.model;

import java.io.Serializable;

import javax.persistence.Transient;

public class PageCondition implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 475083298037905729L;

	
	@Transient
	private Integer NUM_RIGA = null;
	@Transient
	private Integer DA =null;
	@Transient
	private Integer A = null;	
	@Transient 
	private String ORDER_BY = null;	
	@Transient
	private String ORDER_TYPE = null;
	
	public Integer getNUM_RIGA() {
		return NUM_RIGA;
	}
	public void setNUM_RIGA(Integer nUM_RIGA) {
		NUM_RIGA = nUM_RIGA;
	}
	public Integer getDA() {
		return DA;
	}
	public void setDA(Integer dA) {
		DA = dA;
	}
	public Integer getA() {
		return A;
	}
	public void setA(Integer a) {
		A = a;
	}
	public String getORDER_BY() {
		return ORDER_BY;
	}
	public void setORDER_BY(String oRDER_BY) {
		ORDER_BY = oRDER_BY;
	}
	public String getORDER_TYPE() {
		return ORDER_TYPE;
	}
	public void setORDER_TYPE(String oRDER_TYPE) {
		ORDER_TYPE = oRDER_TYPE;
	}
}
