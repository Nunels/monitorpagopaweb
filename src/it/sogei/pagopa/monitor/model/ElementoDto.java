package it.sogei.pagopa.monitor.model;

import java.util.List;

public class ElementoDto {

	private List<Elemento> listElemento;

	public List<Elemento> getListElemento() {
		return listElemento;
	}

	public void setListElemento(List<Elemento> listElemento) {
		this.listElemento = listElemento;
	}

}
