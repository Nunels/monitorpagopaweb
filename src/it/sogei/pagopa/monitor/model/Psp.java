package it.sogei.pagopa.monitor.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Psp implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private Date DATA_PUBBLICAZIONE = null;
	private Date DATA_INIZIO_VALIDITA = null;
	private Date DATA_REGISTRAZIONE = null;
	private Long FLAG_STORNO_PAGAMENTO = null;
	private Long FLAG_MARCA_BOLLO = null;
	private Long STATO_PSP;
	private String URL_INFO_PSP = null;
	private String RAGIONALE_SOCIALE_PSP = null;
	private String ID_FLUSSO_PSP = null;
	private String ID_PSP = null;
	
	private List<DettaglioPsp> DETTAGLI = null;
	
	private String ID_CANALE = null;
	private String TIPO_VERSAMENTO = null;
	private String date = null;
	
	
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public Date getDATA_PUBBLICAZIONE() {
		return DATA_PUBBLICAZIONE;
	}
	public void setDATA_PUBBLICAZIONE(Date dATA_PUBBLICAZIONE) {
		DATA_PUBBLICAZIONE = dATA_PUBBLICAZIONE;
	}
	public Date getDATA_INIZIO_VALIDITA() {
		return DATA_INIZIO_VALIDITA;
	}
	public void setDATA_INIZIO_VALIDITA(Date dATA_INIZIO_VALIDITA) {
		DATA_INIZIO_VALIDITA = dATA_INIZIO_VALIDITA;
	}
	public Long getFLAG_STORNO_PAGAMENTO() {
		return FLAG_STORNO_PAGAMENTO;
	}
	public void setFLAG_STORNO_PAGAMENTO(Long fLAG_STORNO_PAGAMENTO) {
		FLAG_STORNO_PAGAMENTO = fLAG_STORNO_PAGAMENTO;
	}
	public Long getFLAG_MARCA_BOLLO() {
		return FLAG_MARCA_BOLLO;
	}
	public void setFLAG_MARCA_BOLLO(Long fLAG_MARCA_BOLLO) {
		FLAG_MARCA_BOLLO = fLAG_MARCA_BOLLO;
	}
	public String getURL_INFO_PSP() {
		return URL_INFO_PSP;
	}
	public void setURL_INFO_PSP(String uRL_INFO_PSP) {
		URL_INFO_PSP = uRL_INFO_PSP;
	}
	public String getRAGIONALE_SOCIALE_PSP() {
		return RAGIONALE_SOCIALE_PSP;
	}
	public void setRAGIONALE_SOCIALE_PSP(String rAGIONALE_SOCIALE_PSP) {
		RAGIONALE_SOCIALE_PSP = rAGIONALE_SOCIALE_PSP;
	}
	public String getID_FLUSSO_PSP() {
		return ID_FLUSSO_PSP;
	}
	public void setID_FLUSSO_PSP(String iD_FLUSSO_PSP) {
		ID_FLUSSO_PSP = iD_FLUSSO_PSP;
	}
	public String getID_PSP() {
		return ID_PSP;
	}
	public void setID_PSP(String iD_PSP) {
		ID_PSP = iD_PSP;
	}
	public List<DettaglioPsp> getDETTAGLI() {
		if(DETTAGLI==null){
			DETTAGLI= new ArrayList<DettaglioPsp>();
		}
		return DETTAGLI;
	}
	public void setDETTAGLI(List<DettaglioPsp> dettagli) {
		this.DETTAGLI = dettagli;
	}
	public Long getSTATO_PSP() {
		return STATO_PSP;
	}
	public void setSTATO_PSP(Long sTATO_PSP) {
		STATO_PSP = sTATO_PSP;
	}
	public Date getDATA_REGISTRAZIONE() {
		return DATA_REGISTRAZIONE;
	}
	public void setDATA_REGISTRAZIONE(Date dATA_REGISTRAZIONE) {
		DATA_REGISTRAZIONE = dATA_REGISTRAZIONE;
	}
	
	public String getID_CANALE() {
		return ID_CANALE;
	}
	public void setID_CANALE(String iD_CANALE) {
		ID_CANALE = iD_CANALE;
	}
	public String getTIPO_VERSAMENTO() {
		return TIPO_VERSAMENTO;
	}
	public void setTIPO_VERSAMENTO(String tIPO_VERSAMENTO) {
		TIPO_VERSAMENTO = tIPO_VERSAMENTO;
	}
	
	
	@Override
	public String toString() {
		return "Psp [DATA_PUBBLICAZIONE=" + DATA_PUBBLICAZIONE
				+ ", DATA_INIZIO_VALIDITA=" + DATA_INIZIO_VALIDITA
				+ ", DATA_REGISTRAZIONE=" + DATA_REGISTRAZIONE
				+ ", FLAG_STORNO_PAGAMENTO=" + FLAG_STORNO_PAGAMENTO
				+ ", FLAG_MARCA_BOLLO=" + FLAG_MARCA_BOLLO + ", STATO_PSP="
				+ STATO_PSP + ", URL_INFO_PSP=" + URL_INFO_PSP
				+ ", RAGIONALE_SOCIALE_PSP=" + RAGIONALE_SOCIALE_PSP
				+ ", ID_FLUSSO_PSP=" + ID_FLUSSO_PSP + ", ID_PSP=" + ID_PSP
				+ ", DETTAGLI=" + DETTAGLI + ", ID_CANALE=" + ID_CANALE
				+ ", TIPO_VERSAMENTO=" + TIPO_VERSAMENTO + ", date=" + date
				+ "]";
	}
	
	

}
