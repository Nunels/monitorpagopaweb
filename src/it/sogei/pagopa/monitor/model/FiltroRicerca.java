package it.sogei.pagopa.monitor.model;

import java.io.Serializable;
import java.util.Date;

public class FiltroRicerca implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String idDominio;
	private Date daDate;
	private Date aDate;
	private int categoria;
	private String area;
	private String pattern;
	
	

	public String getIdDominio() {
		return idDominio;
	}

	public void setIdDominio(String idDominio) {
		this.idDominio = idDominio;
	}

	public Date getDaDate() {
		return daDate;
	}

	public void setDaDate(Date daDate) {
		this.daDate = daDate;
	}

	public Date getaDate() {
		return aDate;
	}

	public void setaDate(Date aDate) {
		this.aDate = aDate;
	}


	public int getCategoria() {
		return categoria;
	}

	public void setCategoria(int categoria) {
		this.categoria = categoria;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}


	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

}
