package it.sogei.pagopa.monitor.model;

import it.gov.digitpa.schemas._2011.pagamenti.StTipoVersamento;

import java.io.Serializable;
import java.util.Date;

public class DettaglioPsp implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	 
	private Long PRIORITA = null;
	private Long MODELLO_PAGAMENTO = null;
	private Long PROG_DETTAGLIO = null;
	private Date DATA_REGISTRAZIONE = null;
	private String DISPONIBILITA_SERVIZIO = null;
	private String DESCRIZIONE_SERVIZIO = null;
	private String COND_ECONOMICHE_MAX = null;
	private String URL_INFO_CANALE = null;
	private String ID_FLUSSO_PSP = null;
	private String ID_PSP = null;
	private String TIPO_VERSAMENTO = null;
	private String ID_CANALE = null;
	private String ID_INTERMEDIARIO = null;
	private String TIPO_VERSAMENTO_DESCRIZIONE = null;
	private String MODELLO_PAGAMENTO_DESCRIZIONE  =null;
	
	public Long getPRIORITA() {
		return PRIORITA;
	}
	public void setPRIORITA(Long pRIORITA) {
		PRIORITA = pRIORITA;
	}
	public Long getMODELLO_PAGAMENTO() {
		return MODELLO_PAGAMENTO;
	}
	public void setMODELLO_PAGAMENTO(Long mODELLO_PAGAMENTO) {
		MODELLO_PAGAMENTO = mODELLO_PAGAMENTO;
	}
	public Long getPROG_DETTAGLIO() {
		return PROG_DETTAGLIO;
	}
	public void setPROG_DETTAGLIO(Long pROG_DETTAGLIO) {
		PROG_DETTAGLIO = pROG_DETTAGLIO;
	}
	public String getDISPONIBILITA_SERVIZIO() {
		return DISPONIBILITA_SERVIZIO;
	}
	public void setDISPONIBILITA_SERVIZIO(String dISPONIBILITA_SERVIZIO) {
		DISPONIBILITA_SERVIZIO = dISPONIBILITA_SERVIZIO;
	}
	public String getDESCRIZIONE_SERVIZIO() {
		return DESCRIZIONE_SERVIZIO;
	}
	public void setDESCRIZIONE_SERVIZIO(String dESCRIZIONE_SERVIZIO) {
		DESCRIZIONE_SERVIZIO = dESCRIZIONE_SERVIZIO;
	}
	public String getCOND_ECONOMICHE_MAX() {
		return COND_ECONOMICHE_MAX;
	}
	public void setCOND_ECONOMICHE_MAX(String cOND_ECONOMICHE_MAX) {
		COND_ECONOMICHE_MAX = cOND_ECONOMICHE_MAX;
	}
	public String getURL_INFO_CANALE() {
		return URL_INFO_CANALE;
	}
	public void setURL_INFO_CANALE(String uRL_INFO_CANALE) {
		URL_INFO_CANALE = uRL_INFO_CANALE;
	}
	public String getID_FLUSSO_PSP() {
		return ID_FLUSSO_PSP;
	}
	public void setID_FLUSSO_PSP(String iD_FLUSSO_PSP) {
		ID_FLUSSO_PSP = iD_FLUSSO_PSP;
	}
	public String getID_PSP() {
		return ID_PSP;
	}
	public void setID_PSP(String iD_PSP) {
		ID_PSP = iD_PSP;
	}
	public String getTIPO_VERSAMENTO() {
		return TIPO_VERSAMENTO;
	}
	public void setTIPO_VERSAMENTO(String tIPO_VERSAMENTO) {
		TIPO_VERSAMENTO = tIPO_VERSAMENTO;
	}
	public String getID_CANALE() {
		return ID_CANALE;
	}
	public void setID_CANALE(String iD_CANALE) {
		ID_CANALE = iD_CANALE;
	}
	public String getID_INTERMEDIARIO() {
		return ID_INTERMEDIARIO;
	}
	public void setID_INTERMEDIARIO(String iD_INTERMEDIARIO) {
		ID_INTERMEDIARIO = iD_INTERMEDIARIO;
	}
	
	public Date getDATA_REGISTRAZIONE() {
		return DATA_REGISTRAZIONE;
	}
	public void setDATA_REGISTRAZIONE(Date dATA_REGISTRAZIONE) {
		DATA_REGISTRAZIONE = dATA_REGISTRAZIONE;
	}
	public String getTIPO_VERSAMENTO_DESCRIZIONE() {
		
		if(TIPO_VERSAMENTO.equals(StTipoVersamento.AD.name()))
			TIPO_VERSAMENTO_DESCRIZIONE = "Addebito diretto";
		else if(TIPO_VERSAMENTO.equals(StTipoVersamento.BBT.name()))
			TIPO_VERSAMENTO_DESCRIZIONE = "Bonifico Bancario di Tesoreria" ;
		else if(TIPO_VERSAMENTO.equals(StTipoVersamento.CP.name()))
			TIPO_VERSAMENTO_DESCRIZIONE = "Carta di pagamento" ;      
		else if(TIPO_VERSAMENTO.equals(StTipoVersamento.OBEP.name()))
			TIPO_VERSAMENTO_DESCRIZIONE = "On-line banking epayment";
		else if(TIPO_VERSAMENTO.equals(StTipoVersamento.BP.name()))
			TIPO_VERSAMENTO_DESCRIZIONE = "Bollettino Postale";
		else if(TIPO_VERSAMENTO.equals(StTipoVersamento.PO.name()))
			TIPO_VERSAMENTO_DESCRIZIONE = "Pagamento attivato presso PSP";
		
		return TIPO_VERSAMENTO_DESCRIZIONE;
	}
	
	
	public String getMODELLO_PAGAMENTO_DESCRIZIONE() {
		
		if(MODELLO_PAGAMENTO.equals(Long.parseLong("0")))
			MODELLO_PAGAMENTO_DESCRIZIONE = "IMMEDIATO";
		else if(MODELLO_PAGAMENTO.equals(Long.parseLong("1")))
			MODELLO_PAGAMENTO_DESCRIZIONE = "IMMEDIATO MULTI BENEFICIARIO" ;
		else if(MODELLO_PAGAMENTO.equals(Long.parseLong("2")))
			MODELLO_PAGAMENTO_DESCRIZIONE = "DIFFERITO" ;      
		else if(MODELLO_PAGAMENTO.equals(Long.parseLong("3")))
			MODELLO_PAGAMENTO_DESCRIZIONE = "ATTIVATO PRESSO PSP";
	
		
		return MODELLO_PAGAMENTO_DESCRIZIONE;
	}
	
	public void setTIPO_VERSAMENTO_DESCRIZIONE(String tIPO_VERSAMENTO_DESCRIZIONE) {
		TIPO_VERSAMENTO_DESCRIZIONE = tIPO_VERSAMENTO_DESCRIZIONE;
	}
	public void setMODELLO_PAGAMENTO_DESCRIZIONE(
			String mODELLO_PAGAMENTO_DESCRIZIONE) {
		MODELLO_PAGAMENTO_DESCRIZIONE = mODELLO_PAGAMENTO_DESCRIZIONE;
	}
	@Override
	public String toString() {
		return "DettaglioPsp [PRIORITA=" + PRIORITA + ", MODELLO_PAGAMENTO="
				+ MODELLO_PAGAMENTO + ", PROG_DETTAGLIO=" + PROG_DETTAGLIO
				+ ", DATA_REGISTRAZIONE=" + DATA_REGISTRAZIONE
				+ ", DISPONIBILITA_SERVIZIO=" + DISPONIBILITA_SERVIZIO
				+ ", DESCRIZIONE_SERVIZIO=" + DESCRIZIONE_SERVIZIO
				+ ", COND_ECONOMICHE_MAX=" + COND_ECONOMICHE_MAX
				+ ", URL_INFO_CANALE=" + URL_INFO_CANALE + ", ID_FLUSSO_PSP="
				+ ID_FLUSSO_PSP + ", ID_PSP=" + ID_PSP + ", TIPO_VERSAMENTO="
				+ TIPO_VERSAMENTO + ", ID_CANALE=" + ID_CANALE
				+ ", ID_INTERMEDIARIO=" + ID_INTERMEDIARIO
				+ ", TIPO_VERSAMENTO_DESCRIZIONE="
				+ TIPO_VERSAMENTO_DESCRIZIONE + "]";
	}
	
	
	
	
	
	

}
