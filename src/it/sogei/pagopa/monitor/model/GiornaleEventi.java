package it.sogei.pagopa.monitor.model;

import java.io.Serializable;
import java.util.Date;

public class GiornaleEventi extends PageCondition implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final String DEFAULT_NDP_SOGGETTO = "NodoDeiPagamentiSPC";

	public static final String DEFAULT_VALUES_SEPARATOR = ";";
	public static final String DEFAULT_SOTTOTIPO_EVENTO_RICHIESTA = "req";
	public static final String DEFAULT_SOTTOTIPO_EVENTO_RISPOSTA = "rsp";

	public static final String DEFAULT_FIRMA = "0";

	public static final String COMPONENTE_SIPA = "StazioneIntermediarioPA";
	public static final String COMPONENTE_FESP = "FESP";
	public static final String COMPONENTE_WFESP = "WESP";

	public static final String DEFAULT_SOAP_ACTION = "SOAP_ACTION";
	public static final String DEFAULT_NO_SOAP_ACTION = "NO_SOAP_ACTION";
	public static final String DEFAULT_ESITO_OK = "OK";
	public static final String DEFAULT_ESITO_KO = "KO";

	public enum CategoriaEvento {
		INTERNO, INTERFACCIA
	}

	private Long ID_EVENTO = null;
	private Date DATA_ORA_EVENTO = null;
	private String ESITO = null;
	private String PARAM_INTERFACCIA = null;
	private String CANALE_PAGAMENTO = null;
	private String ID_STAZIONE_INT_PA = null;
	private String ID_EROGATORE = null;
	private String ID_FRUITORE = null;
	private String SOTTOTIPO_EVENTO = null;
	private String TIPO_EVENTO = null;
	private String CATEGORIA_EVENTO = null;
	private String COMPONENTE = null;
	private String TIPO_VERSAMENTO = null;
	private String ID_PSP = null;
	private String COD_CONTESTO = null;
	private String IUV = null;
	private String ID_DOMINIO = null;

	public Long getID_EVENTO() {
		return ID_EVENTO;
	}

	public void setID_EVENTO(Long iD_EVENTO) {
		ID_EVENTO = iD_EVENTO;
	}

	public Date getDATA_ORA_EVENTO() {
		return DATA_ORA_EVENTO;
	}

	public void setDATA_ORA_EVENTO(Date dATA_ORA_EVENTO) {
		DATA_ORA_EVENTO = dATA_ORA_EVENTO;
	}

	public String getESITO() {
		return ESITO;
	}

	public void setESITO(String eSITO) {
		ESITO = eSITO;
	}

	public String getPARAM_INTERFACCIA() {
		return PARAM_INTERFACCIA;
	}

	public void setPARAM_INTERFACCIA(String pARAM_INTERFACCIA) {
		PARAM_INTERFACCIA = pARAM_INTERFACCIA;
	}

	public String getCANALE_PAGAMENTO() {
		return CANALE_PAGAMENTO;
	}

	public void setCANALE_PAGAMENTO(String cANALE_PAGAMENTO) {
		CANALE_PAGAMENTO = cANALE_PAGAMENTO;
	}

	public String getID_STAZIONE_INT_PA() {
		return ID_STAZIONE_INT_PA;
	}

	public void setID_STAZIONE_INT_PA(String iD_STAZIONE_INT_PA) {
		ID_STAZIONE_INT_PA = iD_STAZIONE_INT_PA;
	}

	public String getID_EROGATORE() {
		return ID_EROGATORE;
	}

	public void setID_EROGATORE(String iD_EROGATORE) {
		ID_EROGATORE = iD_EROGATORE;
	}

	public String getID_FRUITORE() {
		return ID_FRUITORE;
	}

	public void setID_FRUITORE(String iD_FRUITORE) {
		ID_FRUITORE = iD_FRUITORE;
	}

	public String getSOTTOTIPO_EVENTO() {
		return SOTTOTIPO_EVENTO;
	}

	public void setSOTTOTIPO_EVENTO(String sOTTOTIPO_EVENTO) {
		SOTTOTIPO_EVENTO = sOTTOTIPO_EVENTO;
	}

	public String getTIPO_EVENTO() {
		return TIPO_EVENTO;
	}

	public void setTIPO_EVENTO(String tIPO_EVENTO) {
		TIPO_EVENTO = tIPO_EVENTO;
	}

	public String getCATEGORIA_EVENTO() {
		return CATEGORIA_EVENTO;
	}

	public void setCATEGORIA_EVENTO(String cATEGORIA_EVENTO) {
		CATEGORIA_EVENTO = cATEGORIA_EVENTO;
	}

	public String getCOMPONENTE() {
		return COMPONENTE;
	}

	public void setCOMPONENTE(String cOMPONENTE) {
		COMPONENTE = cOMPONENTE;
	}

	public String getTIPO_VERSAMENTO() {
		return TIPO_VERSAMENTO;
	}

	public void setTIPO_VERSAMENTO(String tIPO_VERSAMENTO) {
		TIPO_VERSAMENTO = tIPO_VERSAMENTO;
	}

	public String getID_PSP() {
		return ID_PSP;
	}

	public void setID_PSP(String iD_PSP) {
		ID_PSP = iD_PSP;
	}

	public String getCOD_CONTESTO() {
		return COD_CONTESTO;
	}

	public void setCOD_CONTESTO(String cOD_CONTESTO) {
		COD_CONTESTO = cOD_CONTESTO;
	}

	public String getIUV() {
		return IUV;
	}

	public void setIUV(String iUV) {
		IUV = iUV;
	}

	public String getID_DOMINIO() {
		return ID_DOMINIO;
	}

	public void setID_DOMINIO(String iD_DOMINIO) {
		ID_DOMINIO = iD_DOMINIO;
	}

	@Override
	public String toString() {
		return "GiornaleEventi [ID_EVENTO=" + ID_EVENTO + ", DATA_ORA_EVENTO="
				+ DATA_ORA_EVENTO + ", ESITO=" + ESITO + ", PARAM_INTERFACCIA="
				+ PARAM_INTERFACCIA + ", CANALE_PAGAMENTO=" + CANALE_PAGAMENTO
				+ ", ID_STAZIONE_INT_PA=" + ID_STAZIONE_INT_PA
				+ ", ID_EROGATORE=" + ID_EROGATORE + ", ID_FRUITORE="
				+ ID_FRUITORE + ", SOTTOTIPO_EVENTO=" + SOTTOTIPO_EVENTO
				+ ", TIPO_EVENTO=" + TIPO_EVENTO + ", CATEGORIA_EVENTO="
				+ CATEGORIA_EVENTO + ", COMPONENTE=" + COMPONENTE
				+ ", TIPO_VERSAMENTO=" + TIPO_VERSAMENTO + ", ID_PSP=" + ID_PSP
				+ ", COD_CONTESTO=" + COD_CONTESTO + ", IUV=" + IUV
				+ ", ID_DOMINIO=" + ID_DOMINIO + "]";
	}

}
