package it.sogei.pagopa.monitor.model;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;

public class PspCatalogo implements Serializable {

	@Override
	public String toString() {
		return "PspCatalogo [DATA_RICEZIONE=" + DATA_RICEZIONE + ", XML="
				+ Arrays.toString(XML) + "]";
	}
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Date DATA_RICEZIONE;
	private byte[] XML;
	
	public Date getDATA_RICEZIONE() {
		return DATA_RICEZIONE;
	}
	public void setDATA_RICEZIONE(Date dATA_RICEZIONE) {
		DATA_RICEZIONE = dATA_RICEZIONE;
	}
	public byte[] getXML() {
		return XML;
	}
	public void setXML(byte[] xML) {
		XML = xML;
	}
	
	
	

}
