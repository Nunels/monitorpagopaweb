package it.sogei.pagopa.monitor.model;

import java.io.Serializable;

public class Dominio implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String urlDominio;
	private String layout;
	private String content;
	
	private String idIntermediarioPA;
	private String idStazioneIntermediarioPA;
	private String deominazione;
	private String idDominio;
	private String pwdPA;
	private String idPsp;
	private String idIntermediarioPsp;
	private String idCanalePsp;
	private String tipoFirma;

	private String ibanAccredito = "IT39M0100003245348200005625";
	private String bicAccredito = "BITAITRRENT";
	private String datiRiscossione = "1/05625";

	private String ibanAddebito;
	private String bicAddebito;

	private boolean isBenchmark = false;
	
	private boolean wisp20 = false;


	public void setIdPsp(String idPsp) {
		this.idPsp = idPsp;
	}

	public void setIdIntermediarioPsp(String idIntermediarioPsp) {
		this.idIntermediarioPsp = idIntermediarioPsp;
	}

	public void setIdCanalePsp(String idCanalePsp) {
		this.idCanalePsp = idCanalePsp;
	}

	public String getIdIntermediarioPA() {
		return idIntermediarioPA;
	}

	public String getIdStazioneIntermediarioPA() {
		return idStazioneIntermediarioPA;
	}

	public String getIdDominio() {
		return idDominio;
	}

	public String getPwdPA() {
		return pwdPA;
	}

	public String getIdPsp() {
		return idPsp;
	}

	public String getIdIntermediarioPsp() {
		return idIntermediarioPsp;
	}

	public String getIdCanalePsp() {
		return idCanalePsp;
	}

	public String getTipoFirma() {
		return tipoFirma;
	}

	public String getDeominazione() {
		return deominazione;
	}

	public boolean isBenchmark() {
		return isBenchmark;
	}

	public String getIbanAccredito() {
		return ibanAccredito;
	}

	public String getBicAccredito() {
		return bicAccredito;
	}

	public String getDatiRiscossione() {
		return datiRiscossione;
	}

	public String getIbanAddebito() {
		return ibanAddebito;
	}

	public void setIbanAddebito(String ibanAddebito) {
		this.ibanAddebito = ibanAddebito;
	}

	public String getBicAddebito() {
		return bicAddebito;
	}

	public void setBicAddebito(String bicAddebito) {
		this.bicAddebito = bicAddebito;
	}

	
	public void setIdIntermediarioPA(String idIntermediarioPA) {
		this.idIntermediarioPA = idIntermediarioPA;
	}

	public void setIdStazioneIntermediarioPA(String idStazioneIntermediarioPA) {
		this.idStazioneIntermediarioPA = idStazioneIntermediarioPA;
	}

	public void setDeominazione(String deominazione) {
		this.deominazione = deominazione;
	}

	public void setIdDominio(String idDominio) {
		this.idDominio = idDominio;
	}

	public void setPwdPA(String pwdPA) {
		this.pwdPA = pwdPA;
	}

	public void setTipoFirma(String tipoFirma) {
		this.tipoFirma = tipoFirma;
	}

	public void setIbanAccredito(String ibanAccredito) {
		this.ibanAccredito = ibanAccredito;
	}

	public void setBicAccredito(String bicAccredito) {
		this.bicAccredito = bicAccredito;
	}

	public void setDatiRiscossione(String datiRiscossione) {
		this.datiRiscossione = datiRiscossione;
	}
	
	

	@Override
	public String toString() {
		return "Dominio [urlDominio=" + urlDominio + ", layout=" + layout
				+ ", content=" + content + ", idIntermediarioPA="
				+ idIntermediarioPA + ", idStazioneIntermediarioPA="
				+ idStazioneIntermediarioPA + ", deominazione=" + deominazione
				+ ", idDominio=" + idDominio + ", pwdPA=" + pwdPA + ", idPsp="
				+ idPsp + ", idIntermediarioPsp=" + idIntermediarioPsp
				+ ", idCanalePsp=" + idCanalePsp + ", tipoFirma=" + tipoFirma
				+ ", ibanAccredito=" + ibanAccredito + ", bicAccredito="
				+ bicAccredito + ", datiRiscossione=" + datiRiscossione
				+ ", ibanAddebito=" + ibanAddebito + ", bicAddebito="
				+ bicAddebito + ", isBenchmark=" + isBenchmark + ", wisp20="
				+ wisp20 + "]";
	}

	public boolean isWisp20() {
		return wisp20;
	}

	public void setWisp20(boolean wisp20) {
		this.wisp20 = wisp20;
	}

	public String getUrlDominio() {
		return urlDominio;
	}

	public void setUrlDominio(String urlDominio) {
		this.urlDominio = urlDominio;
	}

	public String getLayout() {
		return layout;
	}

	public void setLayout(String layout) {
		this.layout = layout;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}


}
