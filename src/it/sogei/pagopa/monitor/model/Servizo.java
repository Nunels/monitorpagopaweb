package it.sogei.pagopa.monitor.model;

import java.io.Serializable;

public class Servizo implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String SIGLA_SERVIZIO;
	
	public String getSIGLA_SERVIZIO() {
		return SIGLA_SERVIZIO;
	}
	public void setSIGLA_SERVIZIO(String sIGLA_SERVIZIO) {
		SIGLA_SERVIZIO = sIGLA_SERVIZIO;
	}
	

}
