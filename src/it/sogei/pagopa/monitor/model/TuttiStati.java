package it.sogei.pagopa.monitor.model;

import java.io.Serializable;

import javax.xml.rpc.ServiceException;

import org.apache.commons.lang3.ArrayUtils;

import it.sogei.pay.system.state.SogeiPaySystemException;


public class TuttiStati implements Serializable{
 
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public enum Stati {
		RPT_ATTIVATA,// RPT Attivata, con tracciato XML e da spedire
		RPT_ERRORE_INVIO_A_NODO, // RPT Inviata con eccezione. Non so se e' stata consegnata
		RPT_INVIO_A_NODO_FALLITO, // RPT Inviata con eccezione e verificato che al nodo non e' arrivata 
		RPT_RICEVUTA_NODO, 
		RPT_RIFIUTATA_NODO, 
		RPT_ACCETTATA_NODO, 
		RPT_RIFIUTATA_PSP, 
		RPT_ACCETTATA_PSP, 
		RPT_ERRORE_INVIO_A_PSP, 
		RPT_INVIATA_A_PSP, 
		RPT_DECORSI_TERMINI,
		RTP_ANNULLATA,
		RT_RICEVUTA_NODO,
		RT_RIFIUTATA_NODO,
		RT_ACCETTATA_NODO,
		RT_ACCETTATA_PA,
		RT_RIFIUTATA_PA,
		RT_ESITO_SCONOSCIUTO_PA;
	}
	
	public enum FirmaRichiesta {
	    CADES("1"),
	    XADES("3"),
	   // AVANZATA("4"),
	    NESSUNA("0");
	    
		private String value;

		FirmaRichiesta(String value) {
			this.value = value;
		}
		public String value() {
			return value;
		}
		
		public static FirmaRichiesta toEnum(String value) throws SogeiPaySystemException {
			
			// Workaround violazione delle specifiche
			if(value == null || value.length() == 0)
				return FirmaRichiesta.NESSUNA;
			
			if( "CADES".equals( value ))
				return FirmaRichiesta.CADES;
			
			if( "XADES".equals( value ))
				return FirmaRichiesta.XADES;
			
			throw new SogeiPaySystemException("Codifica inesistente per FirmaRichiesta. Valore fornito [" + value + "] valori possibili " + ArrayUtils.toString(FirmaRichiesta.values()));
		}
	}
	
	
	public enum StatoRendicontazione {
		NON_RENDICONTATO,
		RENDICONTATO,
		RENDICONTATO_PARZIALE; 
	}
	
	
	public enum EsitoPagamento {
		PAGAMENTO_ESEGUITO(0), 
		PAGAMENTO_NON_ESEGUITO(1),
		PAGAMENTO_PARZIALMENTE_ESEGUITO(2),
		DECORRENZA_TERMINI(3), 
		DECORRENZA_TERMINI_PARZIALE(4);

		private int value;

		EsitoPagamento(int value) {
			this.value = value;
		}

		public int value() {
			return value;
		}

		public static EsitoPagamento toEnum(String value) throws ServiceException {
			try {
				int v = Integer.parseInt(value);
				return toEnum(v);
			} catch (NumberFormatException e) {
				throw new ServiceException("Codifica inesistente per EsitoPagamento. Valore fornito [" + value + "] valori possibili " );
			}
		}

		public static EsitoPagamento toEnum(int value) throws ServiceException {
			for(EsitoPagamento p : EsitoPagamento.values()){
				if(p.value() == value)
					return p;
			}
			throw new ServiceException("Codifica inesistente per EsitoPagamento. Valore fornito [" + value + "] valori possibili ");
		}
	}
	
	
	
	

}
