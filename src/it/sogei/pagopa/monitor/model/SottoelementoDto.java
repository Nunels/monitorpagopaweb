package it.sogei.pagopa.monitor.model;

import java.util.List;

public class SottoelementoDto {

	private List<Sottoelemento> listSottoelemento;

	public List<Sottoelemento> getListSottoelemento() {
		return listSottoelemento;
	}

	public void setListSottoelemento(List<Sottoelemento> listSottoelemento) {
		this.listSottoelemento = listSottoelemento;
	}

}
