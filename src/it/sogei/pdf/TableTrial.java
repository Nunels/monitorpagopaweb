package it.sogei.pdf;

import it.sogei.pagopa.monitor.model.Elemento;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.AcroFields;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamper;
import com.lowagie.text.pdf.PdfWriter;

public class TableTrial {

	public static void main(String[] args) {

		TableTrial table = new TableTrial();

		table.createPdf();
	}

	private void createPdf() {

		List<Elemento> elementi = new ArrayList<>();
		Elemento elemento = new Elemento("nome", new Date(), "descrizione");
		elemento.setId(123L);

		for (int i = 0; i < 10; i++)
			elementi.add(elemento);

		String identify = new SimpleDateFormat("hhmmss").format(new Date());

		String dest = "C:/doganeprj/pagopa/template2/" + identify + ".pdf";

		PdfWriter docWriter = null;

		try {

			System.out.println("Inizio stampa");

			FileOutputStream fos = new FileOutputStream(dest);

			PdfReader pdfReader = new PdfReader(
					"C:/doganeprj/pagopa/template2/table.pdf");

			PdfStamper pdfStamper = new PdfStamper(pdfReader, fos);

			PdfContentByte pdfContentByte = pdfStamper.getOverContent(1);

			AcroFields acroFields = pdfStamper.getAcroFields();

			acroFields.setField("h[0]", "Trial");
			acroFields.setField("h[1]", "Trial");
			acroFields.setField("h[2]", "Trial");
			acroFields.setField("h[3]", "Trial");

			// float[] columnWidths = {1f, 2f, 2f, 2f};

			PdfPTable table = new PdfPTable(4);

			table.setTotalWidth(400);

			table.addCell("Id");
			table.addCell("Nome");
			table.addCell("Cognome");
			table.addCell("Data");
			table.setHeaderRows(1);

			for (int i = 0; i < elementi.size(); i++) {
				table.addCell(elementi.get(i).getId().toString());
				table.addCell(elementi.get(i).getNome());
				table.addCell(elementi.get(i).getDescrizione());
				table.addCell(new SimpleDateFormat("hhmmss").format(elementi
						.get(i).getData()));
			}

			// 1 Numero di righe da eliminare a partire dall'header
			// 2 Numero di righe da scrivere se numero negativo scrive tutte
			// 3 Posizione orizzontale
			// 4 Posizione verticale
			table.writeSelectedRows(0, elementi.size(), 130, 650,
					pdfContentByte);

			pdfStamper.setFormFlattening(true);
			pdfStamper.close();
			pdfReader.close();

			System.out.println("Fine stampa");

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
