package it.sogei.pdf;

import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.datamatrix.DataMatrixWriter;
import com.google.zxing.datamatrix.encoder.SymbolShapeHint;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.lowagie.text.BadElementException;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Image;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.AcroFields;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamper;
import com.lowagie.text.pdf.PushbuttonField;
import com.lowagie.text.pdf.AcroFields.Item;

public class AvvisoAnalogicoDownload {

	private static final Logger logger = Logger.getLogger(AvvisoAnalogicoDownload.class);
	
	//template modello
	public static final String TEMPLATE = "/doganeprj/pagopa/template/template_avviso_V1.pdf";
	
	public static String dest="";
	
	//Il logo dell'ente, se sviluppato in orizzontale, deve essere appoggiato all'angolo superiore destro dell'area prevista.
	//il logo deve essere vettoriale, monocromatico e su sfondo bianco. 
	public static final String LOGO_ENTE = "/doganeprj/pagopa/template/logo.png";
	
	//Codice avviso di 23 caratteri compresi gli spazi - le cifre sono raggruppate a gruppi di 4 separate da uno spazio
	private static String codiceAvviso = "0011 1111 6000 0015 78 ";
	private static String codiceAvvisoSJoined = "";
	
	//Causale del pagamento: max 60 caratteri compresi spazi
	//Nel caso di una sola riga,l'allineamento deve essere al bordo inferiore. Al massimo sono possibili due sole righe.
	private static String oggettoPagamento = "Tasse scolastiche anno di riferimento 2018, 2019, 2020, 2021"; 
	
	//Codice fiscale/PartitaIVA dell'Ente Creditore: max 16 caratteri, tutto maiuscolo
	private static String cfEnte = "AAABBB10X10X111D";
	
	//Denominazione dell'EC beneficiario del pagamento
	//Nel caso di una sola riga, l'allineamento deve essere al bordo superiore. Al massimo sono possibili tre sole righe.
	private static String enteCreditore = "SCUOLA DELL'INFANZA DI PESCARA";
	
	//Denominazione dell'unit� organizzativa che gestisce il pagamento
	//Nel caso di una sola riga, l'allineamento deve essere al bordo superiore. Al massimo sono possibili due sole righe.
	private static String settoreEnte = "UFFICIO RISCOSSIONI PESCARA01";
	
	//Riferimenti dei servizi informativi o di assistenza dell'EC destinati al cittadino
	//Nel caso di una sola riga, l'allineamento deve essere al bordo superiore. Al massimo sono possibili tre sole righe.
	private static String infoEnte = "UFFICIO CENTRALE RISCOSSIONI"; 
	
	//Codice fiscale del soggetto pagatore: max 16 caratteri, tutto maiuscolo
	private static String cfDestinatario = "BBBCCC10X10X111J"; 
	
	//Nome, cognome del soggetto pagatore: max 35 caratteri
	//Nel caso di una sola riga, l'allineamento deve essere al bordo superiore. Al massimo sono possibili 2 sole righe.
	private static String nomeCognomeDestinatario = "Mario Rossi";
	
	//Indirizzo del soggetto pagatore
	//Nel caso di una sola riga, l'allineamento deve essere al bordo superiore. Al massimo sono possibili due sole righe.
	private static String indirizzoDestinatario = "via Roma, 15 - 00040 Albano Laziale";
	
	//se nr rate > 1 il pagamento � rateale altrimenti unica soluzione
	//private static int numeroRate = 0;
	private static int numeroRate = 2;
	private static String pagamentoRateale = "Puoi pagare anche a rate";
	
	//importo del pagamento
	private static String importoRataUnica = "1.500,52";
	
	//data scadenza soluzione unica
	private static String dataRataUnica = "11/12/2018";
	
	//rate	
	private static String rate1="oppure in # rate (vedi pagina seguente). ";
	private static String rate2="La rateizzazione non prevede costi aggiuntivi.";
	
	//del tuo ente
	private static String delTuoEnte="del tuo Ente Creditore, "; 
	//private static String delTuoEnte="";
	
	//di Poste
	//private static String diPoste="di Poste, ";
	private static String diPoste="";
	
	//testo fisso della tua Banca
	private static String dellaTuaBanca="della tua Banca o degli";
	
	//alla_rata
	private static String allaRata="alla rata e ";
		
	//testo fisso canale di pagamento
	private static String canaleDiPagamento="al canale di pagamento che preferisci.";
	
	//Codice interbancario dell'EC in maiuscolo
	private static String cbill="B0JCH";
	
	//Numero del c/c postale dell�Ente Creditore
	private static String numeroCcEC="74988577";
		
	//Intestazione del c/c postale dell�Ente Creditore
	private static String intestatarioCcEC="Intestatario C/C Ente Creditore";
	
	public static void main(String[] args) throws Exception {
		stampaAvvisoAnalogico();
	}


	private static void stampaAvvisoAnalogico() {
		//readFieldsNames();
		writeFieldsNames();
		
	}


	private static void readFieldsNames() {
		try {
		    PdfReader pdfReader = new PdfReader(TEMPLATE);
		    AcroFields acroFields = pdfReader.getAcroFields();
		    HashMap<String,AcroFields.Item> fields = acroFields.getFields();
		    Set<Entry<String, Item>> entrySet = fields.entrySet();
		    for (Entry<String, Item> entry : entrySet) {
		        String key = entry.getKey();
		        System.out.println(key);		       
		        System.out.println("stampaAvvisoAnalogico -> readFieldsNames -> key: "+ key);
		    }
		} catch( Exception ex ) {
	    	ex.printStackTrace();
	    }		
	}


	private static void writeFieldsNames() {
		System.out.println("stampaAvvisoAnalogico -> writeFieldsNames -> Inizio operazione!!!");
		try {			
			PdfReader pdfReader = new PdfReader(TEMPLATE);
			
			String identify=codiceAvviso+"_"+new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
			dest=TEMPLATE.replace("template_", identify+"_");
			
			PdfStamper pdfStamper = new PdfStamper(pdfReader, new FileOutputStream(dest));				
			AcroFields acroFields = pdfStamper.getAcroFields();
			
			//avviso_analogico > avviso_page1 > oggetto_del_pagamento
			acroFields.setField("avviso_analogico[0].avviso_page1[0].oggetto_del_pagamento[0]", oggettoPagamento);
			
			/*//avviso_analogico > avviso_page1 > logo_ente
			PushbuttonField buttonField = acroFields.getNewPushbuttonFromField("avviso_analogico[0].avviso_page1[0].logo_ente[0]");
			buttonField.setLayout(PushbuttonField.LAYOUT_ICON_ONLY);
			buttonField.setProportionalIcon(true);
			buttonField.setImage(Image.getInstance(LOGO_ENTE));
			//acroFields.replacePushbuttonField("avviso_analogico[0].avviso_page1[0].logo_ente[0]", buttonField.getField());
			
			//avviso_analogico > avviso_page2 > logo_ente
			buttonField = acroFields.getNewPushbuttonFromField("avviso_analogico[0].avviso_page2[0].logo_ente[0]");
			buttonField.setLayout(PushbuttonField.LAYOUT_ICON_ONLY);
			buttonField.setProportionalIcon(true);
			buttonField.setImage(Image.getInstance(LOGO_ENTE));
			acroFields.replacePushbuttonField("avviso_analogico[0].avviso_page2[0].logo_ente[0]", buttonField.getField());
			
			//avviso_analogico > avviso_page3 > logo_ente
			buttonField = acroFields.getNewPushbuttonFromField("avviso_analogico[0].avviso_page3[0].logo_ente[0]");
			buttonField.setLayout(PushbuttonField.LAYOUT_ICON_ONLY);
			buttonField.setProportionalIcon(true);
			buttonField.setImage(Image.getInstance(LOGO_ENTE));
			acroFields.replacePushbuttonField("avviso_analogico[0].avviso_page3[0].logo_ente[0]", buttonField.getField());*/
			
			//avviso_analogico > avviso_page1 > cf_ente
			acroFields.setField("avviso_analogico[0].avviso_page1[0].cf_ente[0]", cfEnte);
			
			//avviso_analogico > avviso_page1 > cf_destinatario
			acroFields.setField("avviso_analogico[0].avviso_page1[0].cf_destinatario[0]", cfDestinatario);
			
			//avviso_analogico > avviso_page1 > ente_creditore
			acroFields.setField("avviso_analogico[0].avviso_page1[0].ente_creditore[0]",enteCreditore);
			
			//avviso_analogico > avviso_page1 > settore_ente
			acroFields.setField("avviso_analogico[0].avviso_page1[0].settore_ente[0]", settoreEnte);
			
			//avviso_analogico > avviso_page1 > info_ente
			acroFields.setField("avviso_analogico[0].avviso_page1[0].info_ente[0]", infoEnte);
			
			//avviso_analogico > avviso_page1 > nome_cognome_destinatario
			acroFields.setField("avviso_analogico[0].avviso_page1[0].nome_cognome_destinatario[0]", nomeCognomeDestinatario);
			
			//avviso_analogico > avviso_page1 > indirizzo_destinatario
			acroFields.setField("avviso_analogico[0].avviso_page1[0].indirizzo_destinatario[0]", indirizzoDestinatario);
						
			//avviso_analogico > avviso_page1 > importo_rata_unica			
			acroFields.setField("avviso_analogico[0].avviso_page1[0].importo_rata_unica[0]", importoRataUnica);
			
			//avviso_analogico > avviso_page1 > data_rata_unica (data scadenza unica soluzione)
			acroFields.setField("avviso_analogico[0].avviso_page1[0].data_rata_unica[0]", dataRataUnica);					
			
			String testoCanaleDiPagamento="";
			switch (numeroRate) {
				case 0:
				case 1:
					//avviso_analogico > avviso_page1 > rate[0]
					acroFields.setField("avviso_analogico[0].avviso_page1[0].rate[0]", ".");									
					break;
			default:
				//avviso_analogico > avviso_page1 > pagamento_rateale
				acroFields.setField("avviso_analogico[0].avviso_page1[0].pagamento_rateale[0]", pagamentoRateale);
				//avviso_analogico > avviso_page1 > rate[0]  e avviso_analogico > avviso_page1 > rate[1]
				rate1=rate1.replaceAll("#", String.valueOf(numeroRate));
				acroFields.setField("avviso_analogico[0].avviso_page1[0].rate[0]", rate1);
				acroFields.setField("avviso_analogico[0].avviso_page1[0].rate[1]", rate2);
				
				//avviso_analogico > avviso_page1 > alla_rata[0]
				testoCanaleDiPagamento+=allaRata;				
				break;
			}			
			
			//avviso_analogico > avviso_page1 > testo_ente_creditore
			String testoEnteCreditore="";
			if(!delTuoEnte.equals("")){
				testoEnteCreditore+=delTuoEnte;
			}
			if(!diPoste.equals("")){
				testoEnteCreditore+=diPoste;					
			}			
			testoEnteCreditore+=dellaTuaBanca;
			acroFields.setField("avviso_analogico[0].avviso_page1[0].testo_ente_creditore[0]", testoEnteCreditore);
			
			//avviso_analogico > avviso_page1 > testo_canale_pagamento
			testoCanaleDiPagamento+=canaleDiPagamento;
			acroFields.setField("avviso_analogico[0].avviso_page1[0].test_canale_pagamento[0]", testoCanaleDiPagamento);
			
			//avviso_analogico > avviso_page1 > data_rata_unica (data scadenza unica soluzione)
			acroFields.setField("avviso_analogico[0].avviso_page1[0].data_rata_unica[1]", dataRataUnica);
			
			acroFields.setField("avviso_analogico[0].avviso_page2[0].data_rata_unica[0]", dataRataUnica);
			acroFields.setField("avviso_analogico[0].avviso_page2[0].data_rata_unica[1]", dataRataUnica);
			acroFields.setField("avviso_analogico[0].avviso_page2[0].data_rata_unica[2]", dataRataUnica);
			acroFields.setField("avviso_analogico[0].avviso_page2[0].data_rata_unica[3]", dataRataUnica);
			
			acroFields.setField("avviso_analogico[0].avviso_page3[0].data_rata_unica[0]", dataRataUnica);
			acroFields.setField("avviso_analogico[0].avviso_page3[0].data_rata_unica[1]", dataRataUnica);
			acroFields.setField("avviso_analogico[0].avviso_page3[0].data_rata_unica[2]", dataRataUnica);
			acroFields.setField("avviso_analogico[0].avviso_page3[0].data_rata_unica[3]", dataRataUnica);
			acroFields.setField("avviso_analogico[0].avviso_page3[0].data_rata_unica[4]", dataRataUnica);
			acroFields.setField("avviso_analogico[0].avviso_page3[0].data_rata_unica[5]", dataRataUnica);
			
			
			//avviso_analogico > avviso_page1 > nome_cognome_destinatario			
			acroFields.setField("avviso_analogico[0].avviso_page1[0].nome_cognome_destinatario[1]", nomeCognomeDestinatario);
			
			acroFields.setField("avviso_analogico[0].avviso_page3[0].nome_cognome_destinatario[0]", nomeCognomeDestinatario);
			acroFields.setField("avviso_analogico[0].avviso_page3[0].nome_cognome_destinatario[1]", nomeCognomeDestinatario);
			acroFields.setField("avviso_analogico[0].avviso_page3[0].nome_cognome_destinatario[2]", nomeCognomeDestinatario);
			
			//avviso_analogico > avviso_page1 > importo_rata_unica			
			acroFields.setField("avviso_analogico[0].avviso_page1[0].importo_rata_unica[1]", importoRataUnica);
			
			//avviso_analogico > avviso_page1 > ente_creditore
			acroFields.setField("avviso_analogico[0].avviso_page1[0].ente_creditore[1]",enteCreditore);
			
			acroFields.setField("avviso_analogico[0].avviso_page2[0].ente_creditore[0]",enteCreditore);
			acroFields.setField("avviso_analogico[0].avviso_page2[0].ente_creditore[1]",enteCreditore);
			
			acroFields.setField("avviso_analogico[0].avviso_page3[0].ente_creditore[0]",enteCreditore);
			acroFields.setField("avviso_analogico[0].avviso_page3[0].ente_creditore[1]",enteCreditore);
			acroFields.setField("avviso_analogico[0].avviso_page3[0].ente_creditore[2]",enteCreditore);
			
			//avviso_analogico > avviso_page1 > oggetto_del_pagamento
			acroFields.setField("avviso_analogico[0].avviso_page1[0].oggetto_del_pagamento[1]", oggettoPagamento);
			acroFields.setField("avviso_analogico[0].avviso_page1[0].oggetto_del_pagamento[2]", oggettoPagamento);
			
			acroFields.setField("avviso_analogico[0].avviso_page2[0].oggetto_del_pagamento[0]", oggettoPagamento);
			acroFields.setField("avviso_analogico[0].avviso_page2[0].oggetto_del_pagamento[1]", oggettoPagamento);
			acroFields.setField("avviso_analogico[0].avviso_page2[0].oggetto_del_pagamento[2]", oggettoPagamento);
			acroFields.setField("avviso_analogico[0].avviso_page2[0].oggetto_del_pagamento[3]", oggettoPagamento);
			acroFields.setField("avviso_analogico[0].avviso_page2[0].oggetto_del_pagamento[4]", oggettoPagamento);
			
			acroFields.setField("avviso_analogico[0].avviso_page3[0].oggetto_del_pagamento[0]", oggettoPagamento);
			acroFields.setField("avviso_analogico[0].avviso_page3[0].oggetto_del_pagamento[1]", oggettoPagamento);
			acroFields.setField("avviso_analogico[0].avviso_page3[0].oggetto_del_pagamento[2]", oggettoPagamento);
			acroFields.setField("avviso_analogico[0].avviso_page3[0].oggetto_del_pagamento[3]", oggettoPagamento);
			acroFields.setField("avviso_analogico[0].avviso_page3[0].oggetto_del_pagamento[4]", oggettoPagamento);
			acroFields.setField("avviso_analogico[0].avviso_page3[0].oggetto_del_pagamento[5]", oggettoPagamento);
			acroFields.setField("avviso_analogico[0].avviso_page3[0].oggetto_del_pagamento[6]", oggettoPagamento);
			
			acroFields.setField("avviso_analogico[0].avviso_page2[0].nr1[0]", "1");
			acroFields.setField("avviso_analogico[0].avviso_page2[0].nr2[0]", "2");
			acroFields.setField("avviso_analogico[0].avviso_page2[0].nr3[0]", "3");
			acroFields.setField("avviso_analogico[0].avviso_page2[0].nr4[0]", "4");
			acroFields.setField("avviso_analogico[0].avviso_page2[0].nr5[0]", "5");
			
			acroFields.setField("avviso_analogico[0].avviso_page2[0].nr1[1]", "1");
			acroFields.setField("avviso_analogico[0].avviso_page2[0].nr2[1]", "2");
			acroFields.setField("avviso_analogico[0].avviso_page2[0].nr1[2]", "1");
			acroFields.setField("avviso_analogico[0].avviso_page2[0].nr2[2]", "2");
			
			acroFields.setField("avviso_analogico[0].avviso_page3[0].nr3[0]", "3");
			acroFields.setField("avviso_analogico[0].avviso_page3[0].nr3[1]", "3");
			acroFields.setField("avviso_analogico[0].avviso_page3[0].nr3[2]", "3");
			acroFields.setField("avviso_analogico[0].avviso_page3[0].nr4[0]", "4");
			acroFields.setField("avviso_analogico[0].avviso_page3[0].nr4[1]", "4");
			acroFields.setField("avviso_analogico[0].avviso_page3[0].nr4[2]", "4");
			acroFields.setField("avviso_analogico[0].avviso_page3[0].nr5[0]", "5");
			acroFields.setField("avviso_analogico[0].avviso_page3[0].nr5[1]", "5");
			acroFields.setField("avviso_analogico[0].avviso_page3[0].nr5[2]", "5");
			
			//avviso_analogico > avviso_page1 > QRCode_rata_unica
			String charset = "UTF-8";
		
			float[] positionLogo= acroFields.getFieldPositions("avviso_analogico[0].avviso_page1[0].logo_ente[0]");
			setImage(pdfStamper,positionLogo);
			
			float[] positionLogo1= acroFields.getFieldPositions("avviso_analogico[0].avviso_page2[0].logo_ente[0]");
			setImage(pdfStamper,positionLogo1);
			
			float[] positionLogo2= acroFields.getFieldPositions("avviso_analogico[0].avviso_page3[0].logo_ente[0]");
			setImage(pdfStamper,positionLogo2);
			
			float[] positionQRCode= acroFields.getFieldPositions("avviso_analogico[0].avviso_page1[0].qr_code[0]");
			
			float[] positionQRCode1= acroFields.getFieldPositions("avviso_analogico[0].avviso_page2[0].qr_code[0]");
			float[] positionQRCode2= acroFields.getFieldPositions("avviso_analogico[0].avviso_page2[0].qr_code[1]");
			
			float[] positionQRCode3= acroFields.getFieldPositions("avviso_analogico[0].avviso_page3[0].qr_code[0]");
			float[] positionQRCode4= acroFields.getFieldPositions("avviso_analogico[0].avviso_page3[0].qr_code[1]");
			float[] positionQRCode5= acroFields.getFieldPositions("avviso_analogico[0].avviso_page3[0].qr_code[2]");
			
			setImageQRCode(pdfStamper,positionQRCode);
			setImageQRCode(pdfStamper,positionQRCode1);
			setImageQRCode(pdfStamper,positionQRCode2);
			setImageQRCode(pdfStamper,positionQRCode3);
			setImageQRCode(pdfStamper,positionQRCode4);
			setImageQRCode(pdfStamper,positionQRCode5);
			
			
			float[] positiondataMatrix= acroFields.getFieldPositions("avviso_analogico[0].avviso_page1[0].data_matrix[0]");
			
			float[] positiondataMatrix1= acroFields.getFieldPositions("avviso_analogico[0].avviso_page2[0].data_matrix[0]");
			float[] positiondataMatrix2= acroFields.getFieldPositions("avviso_analogico[0].avviso_page2[0].data_matrix[1]");
			
			float[] positiondataMatrix3= acroFields.getFieldPositions("avviso_analogico[0].avviso_page3[0].data_matrix[0]");
			float[] positiondataMatrix4= acroFields.getFieldPositions("avviso_analogico[0].avviso_page3[0].data_matrix[1]");
			float[] positiondataMatrix5= acroFields.getFieldPositions("avviso_analogico[0].avviso_page3[0].data_matrix[2]");
			
			setImageDataMatrix(pdfStamper, positiondataMatrix);
			setImageDataMatrix(pdfStamper, positiondataMatrix1);
			setImageDataMatrix(pdfStamper, positiondataMatrix2);
			setImageDataMatrix(pdfStamper, positiondataMatrix3);
			setImageDataMatrix(pdfStamper, positiondataMatrix4);
			setImageDataMatrix(pdfStamper, positiondataMatrix5);
			
		    //avviso_analogico > avviso_page1 > cbill
			acroFields.setField("avviso_analogico[0].avviso_page1[0].cbill[0]", cbill);
			
			acroFields.setField("avviso_analogico[0].avviso_page2[0].cbill[0]", cbill);
			acroFields.setField("avviso_analogico[0].avviso_page2[0].cbill[1]", cbill);
		
			acroFields.setField("avviso_analogico[0].avviso_page3[0].cbill[0]", cbill);
			acroFields.setField("avviso_analogico[0].avviso_page3[0].cbill[1]", cbill);
			acroFields.setField("avviso_analogico[0].avviso_page3[0].cbill[2]", cbill);
			
			acroFields.setField("avviso_analogico[0].avviso_page1[0].autorizzazione[0]", "autorizzazato");
			
			acroFields.setField("avviso_analogico[0].avviso_page2[0].autorizzazione[0]", "autorizzazato");
			acroFields.setField("avviso_analogico[0].avviso_page2[0].autorizzazione[1]", "autorizzazato");
			
			acroFields.setField("avviso_analogico[0].avviso_page3[0].autorizzazione[0]", "autorizzazato");
			acroFields.setField("avviso_analogico[0].avviso_page3[0].autorizzazione[1]", "autorizzazato");
			acroFields.setField("avviso_analogico[0].avviso_page3[0].autorizzazione[2]", "autorizzazato");
			//avviso_analogico > avviso_page1 > codice_avviso
			acroFields.setField("avviso_analogico[0].avviso_page1[0].codice_avviso[0]", codiceAvviso);
			
			acroFields.setField("avviso_analogico[0].avviso_page1[0].codice_avviso[1]", codiceAvviso);
			
			acroFields.setField("avviso_analogico[0].avviso_page2[0].codice_avviso[0]", codiceAvviso);
			acroFields.setField("avviso_analogico[0].avviso_page2[0].codice_avviso[1]", codiceAvviso);
			acroFields.setField("avviso_analogico[0].avviso_page2[0].codice_avviso[2]", codiceAvviso);
			acroFields.setField("avviso_analogico[0].avviso_page2[0].codice_avviso[3]", codiceAvviso);
			
			acroFields.setField("avviso_analogico[0].avviso_page3[0].codice_avviso[0]", codiceAvviso);
			acroFields.setField("avviso_analogico[0].avviso_page3[0].codice_avviso[1]", codiceAvviso);
			acroFields.setField("avviso_analogico[0].avviso_page3[0].codice_avviso[2]", codiceAvviso);
			acroFields.setField("avviso_analogico[0].avviso_page3[0].codice_avviso[3]", codiceAvviso);
			acroFields.setField("avviso_analogico[0].avviso_page3[0].codice_avviso[4]", codiceAvviso);
			acroFields.setField("avviso_analogico[0].avviso_page3[0].codice_avviso[5]", codiceAvviso);
			
			//avviso_analogico > avviso_page1 > cf_ente
			acroFields.setField("avviso_analogico[0].avviso_page1[0].cf_ente[1]", cfEnte);
			
			acroFields.setField("avviso_analogico[0].avviso_page1[0].cf_ente[2]", cfEnte);
			
			acroFields.setField("avviso_analogico[0].avviso_page2[0].cf_ente[0]", cfEnte);
			acroFields.setField("avviso_analogico[0].avviso_page2[0].cf_ente[1]", cfEnte);
			acroFields.setField("avviso_analogico[0].avviso_page2[0].cf_ente[2]", cfEnte);
			acroFields.setField("avviso_analogico[0].avviso_page2[0].cf_ente[3]", cfEnte);
			
			acroFields.setField("avviso_analogico[0].avviso_page3[0].cf_ente[0]", cfEnte);
			acroFields.setField("avviso_analogico[0].avviso_page3[0].cf_ente[1]", cfEnte);
			acroFields.setField("avviso_analogico[0].avviso_page3[0].cf_ente[2]", cfEnte);
			acroFields.setField("avviso_analogico[0].avviso_page3[0].cf_ente[3]", cfEnte);
			acroFields.setField("avviso_analogico[0].avviso_page3[0].cf_ente[4]", cfEnte);
			acroFields.setField("avviso_analogico[0].avviso_page3[0].cf_ente[5]", cfEnte);
			
			//avviso_analogico > avviso_page1 > numero_cc_postale
			acroFields.setField("avviso_analogico[0].avviso_page1[0].numero_cc_postale[0]", numeroCcEC);
			
			acroFields.setField("avviso_analogico[0].avviso_page2[0].numero_cc_postale[0]", numeroCcEC);
			acroFields.setField("avviso_analogico[0].avviso_page2[0].numero_cc_postale[1]", numeroCcEC);
			
			acroFields.setField("avviso_analogico[0].avviso_page3[0].numero_cc_postale[0]", numeroCcEC);
			acroFields.setField("avviso_analogico[0].avviso_page3[0].numero_cc_postale[1]", numeroCcEC);
			acroFields.setField("avviso_analogico[0].avviso_page3[0].numero_cc_postale[2]", numeroCcEC);
			
			
			//avviso_analogico > avviso_page1 > intestatario_conto_corrente_postale
			acroFields.setField("avviso_analogico[0].avviso_page1[0].intestatario_conto_corrente_postale[0]", intestatarioCcEC);
			
			acroFields.setField("avviso_analogico[0].avviso_page1[0].nome_cognome_destinatario[2]", nomeCognomeDestinatario);
			
			acroFields.setField("avviso_analogico[0].avviso_page2[0].nome_cognome_destinatario[0]", nomeCognomeDestinatario);
			acroFields.setField("avviso_analogico[0].avviso_page2[0].nome_cognome_destinatario[1]", nomeCognomeDestinatario);
			acroFields.setField("avviso_analogico[0].avviso_page2[0].nome_cognome_destinatario[2]", nomeCognomeDestinatario);
			acroFields.setField("avviso_analogico[0].avviso_page2[0].nome_cognome_destinatario[3]", nomeCognomeDestinatario);
			
			acroFields.setField("avviso_analogico[0].avviso_page2[0].intestatario_conto_corrente_postale[0]", nomeCognomeDestinatario);
			acroFields.setField("avviso_analogico[0].avviso_page2[0].intestatario_conto_corrente_postale[1]", nomeCognomeDestinatario);
			
			acroFields.setField("avviso_analogico[0].avviso_page3[0].intestatario_conto_corrente_postale[0]", nomeCognomeDestinatario);
			acroFields.setField("avviso_analogico[0].avviso_page3[0].intestatario_conto_corrente_postale[1]", nomeCognomeDestinatario);
			acroFields.setField("avviso_analogico[0].avviso_page3[0].intestatario_conto_corrente_postale[2]", nomeCognomeDestinatario);
			
			acroFields.setField("avviso_analogico[0].avviso_page1[0].importo[0]", importoRataUnica);
			acroFields.setField("avviso_analogico[0].avviso_page1[0].importo[1]", importoRataUnica);
			
			acroFields.setField("avviso_analogico[0].avviso_page2[0].importo[0]", importoRataUnica);
			acroFields.setField("avviso_analogico[0].avviso_page2[0].importo[1]", importoRataUnica);
			acroFields.setField("avviso_analogico[0].avviso_page2[0].importo[2]", importoRataUnica);
			acroFields.setField("avviso_analogico[0].avviso_page2[0].importo[3]", importoRataUnica);
			
			acroFields.setField("avviso_analogico[0].avviso_page3[0].importo[0]", importoRataUnica);
			acroFields.setField("avviso_analogico[0].avviso_page3[0].importo[1]", importoRataUnica);
			acroFields.setField("avviso_analogico[0].avviso_page3[0].importo[2]", importoRataUnica);
			acroFields.setField("avviso_analogico[0].avviso_page3[0].importo[3]", importoRataUnica);
			acroFields.setField("avviso_analogico[0].avviso_page3[0].importo[4]", importoRataUnica);
			acroFields.setField("avviso_analogico[0].avviso_page3[0].importo[5]", importoRataUnica);
			
			//avviso_analogico > avviso_page1 > data_rata_unica (data scadenza unica soluzione)
			acroFields.setField("avviso_analogico[0].avviso_page1[0].data_rata_unica[2]", dataRataUnica);
			
			//avviso_analogico > avviso_page1 > importo_rata_unica			
			acroFields.setField("avviso_analogico[0].avviso_page1[0].importo_rata_unica[2]", importoRataUnica);
			
			pdfStamper.setFormFlattening(true);
			pdfStamper.close();
			pdfReader.close();
		} catch (IOException e) {
			System.err.println("stampaAvvisoAnalogico -> writeFieldsNames -> IOException: "+ e.getMessage());
			e.printStackTrace();
		} catch (DocumentException ex) {
			System.err.println("stampaAvvisoAnalogico -> writeFieldsNames -> DocumentException: "+ ex.getMessage());
			ex.printStackTrace();
		} catch (NumberFormatException nfe){
			System.err.println("stampaAvvisoAnalogico -> writeFieldsNames -> NumberFormatException: "+ nfe.getMessage());
			nfe.printStackTrace();
		}
		System.out.println("stampaAvvisoAnalogico -> writeFieldsNames -> Operazione terminata!!!");
	}

	public void getCodiceAvvisoJoined(String codAvviso) {
	    String[] avvisoSplitted = codAvviso.split(" ");
	    StringBuilder builder = new StringBuilder();
	    for (String value : avvisoSplitted) {
	        builder.append(value);
	    }
	    codiceAvvisoSJoined = builder.toString();
	}

	private static byte[] generateQRCode(String text, int width, int height) {
		
		QRCodeWriter qrCodeWriter = new QRCodeWriter();
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		try {
			BitMatrix bitMatrix = qrCodeWriter.encode(text, BarcodeFormat.QR_CODE, width, height);
			MatrixToImageWriter.writeToStream(bitMatrix, "png", byteArrayOutputStream);		
		} catch (WriterException we) {
			System.out.println("stampaAvvisoAnalogico -> generateQRCode -> WriterException: " + we.getMessage());
			we.printStackTrace();
		}catch (IOException ioe) {
			System.out.println("stampaAvvisoAnalogico -> generateQRCode -> IOException: " + ioe.getMessage());
			ioe.printStackTrace();
		}
		return byteArrayOutputStream.toByteArray();
	}
	
   private static byte[] generateDataMatrix(String text, int width, int height) {
		
		DataMatrixWriter dtMatrixWriter = new DataMatrixWriter();
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		try {
			HashMap hintMap = new HashMap();
			hintMap.put(EncodeHintType.DATA_MATRIX_SHAPE, SymbolShapeHint.FORCE_SQUARE);
			BitMatrix bitMatrix = dtMatrixWriter.encode(text, BarcodeFormat.DATA_MATRIX, width, height,hintMap);
			MatrixToImageWriter.writeToStream(bitMatrix, "png", byteArrayOutputStream);		
		} catch (IOException ioe) {
			System.out.println("stampaAvvisoAnalogico -> generateQRCode -> IOException: " + ioe.getMessage());
			ioe.printStackTrace();
		}
		return byteArrayOutputStream.toByteArray();
	}
   
   private static void setImageQRCode(PdfStamper pdfStamper,float[] positionQRCode) throws MalformedURLException, IOException, DocumentException{
	    
	    Rectangle rectangleQRCode = new Rectangle(positionQRCode[1], positionQRCode[2], positionQRCode[3], positionQRCode[4]);			
		Map<EncodeHintType, ErrorCorrectionLevel> hintMap = new HashMap<EncodeHintType, ErrorCorrectionLevel>();
	    hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
	    byte[] byteArrayQRCode=generateQRCode("PAGOPA|002|"+codiceAvviso+"|"+cfEnte+"|"+importoRataUnica,25,25);
	   
	    Image imageQRCode = Image.getInstance(byteArrayQRCode);
		imageQRCode.scaleToFit(rectangleQRCode.getWidth(), rectangleQRCode.getHeight());
		imageQRCode.setAbsolutePosition(positionQRCode[1] + (rectangleQRCode.getWidth() - imageQRCode.getScaledWidth()) / 2, positionQRCode[2] + (rectangleQRCode.getHeight() - imageQRCode.getScaledHeight()) / 2);
  

		PdfContentByte pdfContentByte = pdfStamper.getOverContent((int)positionQRCode[0]);
		pdfContentByte = pdfStamper.getOverContent((int)positionQRCode[0]); 
		pdfContentByte.addImage(imageQRCode);
		
   }
   
   private static void setImage(PdfStamper pdfStamper,float[] position) throws MalformedURLException, IOException, DocumentException{
	    
	    Rectangle rectangleQRCode = new Rectangle(position[1], position[2], position[3], position[4]);			
	    Map<EncodeHintType, ErrorCorrectionLevel> hintMap = new HashMap<EncodeHintType, ErrorCorrectionLevel>();
	    hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
	    Image imageQRCode = Image.getInstance(LOGO_ENTE);
		imageQRCode.scaleToFit(rectangleQRCode.getWidth(), rectangleQRCode.getHeight());
		imageQRCode.setAbsolutePosition(position[1] + (rectangleQRCode.getWidth() - imageQRCode.getScaledWidth())/2, position[2] + (rectangleQRCode.getHeight() - imageQRCode.getScaledHeight())/2);
   
 

		PdfContentByte pdfContentByte = pdfStamper.getOverContent((int)position[0]);
		pdfContentByte = pdfStamper.getOverContent((int)position[0]); 
		pdfContentByte.addImage(imageQRCode);
		
  }
   private static void setImageDataMatrix(PdfStamper pdfStamper,float[] positiondataMatrix) throws MalformedURLException, IOException, DocumentException{
	    
			
	   Rectangle dataMatrixCode = new Rectangle(positiondataMatrix[1], positiondataMatrix[2], positiondataMatrix[3], positiondataMatrix[4]);			
		Map<EncodeHintType, ErrorCorrectionLevel> hintMapMatrix = new HashMap<EncodeHintType, ErrorCorrectionLevel>();
		hintMapMatrix.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
	   byte[] dataMatrixCodeByte=generateDataMatrix("PAGOPA|002|",55, 55);
		
	   Image imageMatrixCode = Image.getInstance(dataMatrixCodeByte);
	   imageMatrixCode.scaleToFit(dataMatrixCode.getWidth(), dataMatrixCode.getHeight());
	   imageMatrixCode.setAbsolutePosition(positiondataMatrix[1], positiondataMatrix[2]);
	 
	   
		PdfContentByte pdfContentByteMAtrix = pdfStamper.getOverContent((int)positiondataMatrix[0]);
		pdfContentByteMAtrix = pdfStamper.getOverContent((int)positiondataMatrix[0]); 
		pdfContentByteMAtrix.addImage(imageMatrixCode);
	
   }
}