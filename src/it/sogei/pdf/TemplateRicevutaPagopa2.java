package it.sogei.pdf;

import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.lowagie.text.pdf.AcroFields;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamper;

public class TemplateRicevutaPagopa2 {

	public static final String TEMPLATE = "/doganeprj/pagopa/template_ricevuta_pagopa_2/template_.pdf";

	public static String dest = "";

	public static final String LOGO = "/doganeprj/pagopa/template_ricevuta_pagopa_2/entrate_riscossione.jpg";
	public static final String LOGO1 = "/doganeprj/pagopa/template_ricevuta_pagopa_2/logo.png";

	public static void main(String[] args) throws Exception {
		writeFieldsNames();
	}

	private static void writeFieldsNames() {
		System.out
				.println("stampa -> writeFieldsNames -> Inizio operazione!!!");

		try {
			PdfReader pdfReader = new PdfReader(TEMPLATE);
			String identify = new SimpleDateFormat("yyyyMMddhhmmss")
					.format(new Date());
			dest = "/doganeprj/pagopa/template_ricevuta_pagopa_2/template_ricevuta_pagopa_"
					+ identify + ".pdf";

			PdfStamper pdfStamper = new PdfStamper(pdfReader,
					new FileOutputStream(dest));
			AcroFields acroFields = pdfStamper.getAcroFields();

			//
			acroFields.setField("ricevutaPagoPa.form.pagamento",
					"Il pagamento � stato registrato correttamente");
			acroFields.setField("ricevutaPagoPa.form.intestatarioLabel",
					"Intestatario");
			acroFields.setField("ricevutaPagoPa.form.intestatarioValue[0]",
					"Mario Rossi");
			acroFields.setField("ricevutaPagoPa.form.intestatarioValue[1]",
					"ABCDFG34T54H234V");
			acroFields.setField("ricevutaPagoPa.form.dataLabel",
					"Data/ora Pagamento");
			acroFields.setField("ricevutaPagoPa.form.dataValue",
					"12-02-2020 23:12:54");
			acroFields.setField("ricevutaPagoPa.form.pspLabel", "PSP");
			acroFields.setField("ricevutaPagoPa.form.pspValue",
					"Agenzia delle Entrate - Riscossione");
			acroFields.setField("ricevutaPagoPa.form.iuvLabel", "IUV");
			acroFields.setField("ricevutaPagoPa.form.iuvValue",
					"345678987654345678");
			acroFields.setField("ricevutaPagoPa.form.ccpLabel", "CCP");
			acroFields.setField("ricevutaPagoPa.form.ccpValue",
					"15744339065771992011");
			acroFields.setField("ricevutaPagoPa.form.importoLabel",
					"Importo Totale");
			acroFields.setField("ricevutaPagoPa.form.importoValue",
					"� 1.234.567,12");

			acroFields.setField("ricevutaPagoPa.form.pspLegenda",
					"PSP: Prestatore Servizio di Pagamento");
			acroFields.setField("ricevutaPagoPa.form.iuvLegenda",
					"IUV: Identificativo Univoco del Versamento");
			acroFields.setField("ricevutaPagoPa.form.ccpLegenda",
					"CCP: Codice Contesto di Pagamento");
			acroFields.setField("ricevutaPagoPa.form.iurLegenda",
					"IUR: Identificativo Univoco Riscossione");

			acroFields.setField("ricevutaPagoPa.form.pagopaLegenda",
					"Per informazioni sul sistema pagoPA consulta");
			acroFields.setField("ricevutaPagoPa.form.pagopaLink",
					"https://www.pagopa.gov.it/");
			acroFields.setField("ricevutaPagoPa.form.footer[0]",
					"Agenzia delle Entrate");
			acroFields.setField("ricevutaPagoPa.form.footer[1]",
					"Sede legale: via Giorgione n. 106, 00147 Roma");
			acroFields.setField("ricevutaPagoPa.form.footer[2]",
					"Codice Fiscale: 06363391001");

			//
			acroFields.setField("ricevutaPagoPa.form.row1N", "1");
			acroFields.setField("ricevutaPagoPa.form.row1Iur", "12345");
			acroFields.setField("ricevutaPagoPa.form.row1Causale",
					"pagamento per servizio ");
			acroFields.setField("ricevutaPagoPa.form.row1Importo", "�1234,56");

			acroFields.setField("ricevutaPagoPa.form.row2N", "2");
			acroFields.setField("ricevutaPagoPa.form.row2Iur", "12345");
			acroFields.setField("ricevutaPagoPa.form.row2Causale",
					"pagamento per servizio ");
			acroFields.setField("ricevutaPagoPa.form.row2Importo", "�1234,56");

			acroFields.setField("ricevutaPagoPa.form.row3N", "3");
			acroFields.setField("ricevutaPagoPa.form.row3Iur", "12345");
			acroFields.setField("ricevutaPagoPa.form.row3Causale",
					"pagamento per servizio ");
			acroFields.setField("ricevutaPagoPa.form.row3Importo", "�1234,56");

			acroFields.setField("ricevutaPagoPa.form.row4N", "4");
			acroFields.setField("ricevutaPagoPa.form.row4Iur", "12345");
			acroFields.setField("ricevutaPagoPa.form.row4Causale",
					"pagamento per servizio ");
			acroFields.setField("ricevutaPagoPa.form.row4Importo", "�1234,56");

			acroFields.setField("ricevutaPagoPa.form.row5N", "5");
			acroFields.setField("ricevutaPagoPa.form.row5Iur", "12345");
			acroFields.setField("ricevutaPagoPa.form.row5Causale",
					"pagamento per servizio ");
			acroFields.setField("ricevutaPagoPa.form.row5Importo", "�1234,56");

			//

			pdfStamper.setFormFlattening(true);
			pdfStamper.close();
			pdfReader.close();

		} catch (Exception e) {
			System.err.println("stampa -> writeFieldsNames -> Exception: "
					+ e.getMessage());
			e.printStackTrace();
		}
		System.out
				.println("stampa -> writeFieldsNames -> Operazione terminata!!!");

	}

}
