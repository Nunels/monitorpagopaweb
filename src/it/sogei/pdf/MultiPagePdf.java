package it.sogei.pdf;

import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.google.zxing.EncodeHintType;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Image;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.AcroFields;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfCopy;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamper;

public class MultiPagePdf {

	public static final String TEMPLATE = "/doganeprj/pagopa/template/ricevuta_pagopaaa.pdf";
	public static String dest = "";

	public static final String LOGO = "/doganeprj/pagopa/template/entrate_riscossione.jpg";
	public static final String LOGO1 = "/doganeprj/pagopa/template/logo.png";

	public static void main(String[] args) throws Exception {
		stampaAvvisoAnalogico();
	}

	private static void stampaAvvisoAnalogico() throws DocumentException,
			IOException {

		String identify = new SimpleDateFormat("yyyyMMddhhmmss")
				.format(new Date());
		dest = TEMPLATE.replace("ricevuta_pagopa", "ricevuta_pagopa_"
				+ identify);

		Document pdfDocument = new Document();
		ByteArrayOutputStream pdfOutputStream = new ByteArrayOutputStream();
		PdfCopy copy = new PdfCopy(pdfDocument, pdfOutputStream);
		pdfDocument.open();

		for (int i = 1; i <= 3; i++) {
			PdfReader pdfReader = new PdfReader(writeFieldsNames(i, 3));
			copy.addPage(copy.getImportedPage(pdfReader, 1));
		}
		pdfDocument.close();

		OutputStream outputStream = new FileOutputStream(dest);
		outputStream.write(pdfOutputStream.toByteArray());
		outputStream.close();
		// writeFieldsNames();

	}

	private static byte[] writeFieldsNames(Integer numPag, Integer totPag) {
		System.out
				.println("stampa -> writeFieldsNames -> Inizio operazione!!!");
		ByteArrayOutputStream baos = new ByteArrayOutputStream();

		String identify = new SimpleDateFormat("yyyyMMddhhmmss")
				.format(new Date());
		String _dest = TEMPLATE.replace("ricevuta_pagopa", "ricevuta_pagopa__"
				+ identify);

		try {
			PdfReader pdfReader = new PdfReader(TEMPLATE);
			PdfStamper pdfStamper = new PdfStamper(pdfReader, baos);

			AcroFields acroFields = pdfStamper.getAcroFields();

			acroFields.setField("nome[0]", "Intestatario:");
			acroFields.setField("valore[0]", "Mario Rossi");

			acroFields.setField("nome[1]", "Codice Fiscale:");
			acroFields.setField("valore[1]", "DFGERT76J76H765N");

			String data = new SimpleDateFormat("dd MM hhhh hh:mm:ss")
					.format(new Date());
			acroFields.setField("nome[2]", "Data/Ora");
			acroFields.setField("valore[2]", data);

			acroFields.setField("nome[3]", "Transazione:");
			acroFields.setField("valore[3]", "23456789876543");

			acroFields.setField("nome[4]", "Agente:");
			acroFields.setField("valore[4]",
					"Agenzia delle Entrate - Riscossione");

			acroFields.setField("nome[5]", "CF Agente:");
			acroFields.setField("valore[5]", "DGTDBG56B34Y609B");

			acroFields.setField("nome[6]", "Psp:");
			acroFields.setField("valore[6]", "GTHNYDN - Banca di Roma");

			acroFields.setField("nome[7]", "Id Riscossione:");
			acroFields.setField("valore[7]", "12342134543646747865245624");

			acroFields.setField("nome[8]", "Iuv:�");
			acroFields.setField("valore[8]", "345678987654345678");

			acroFields.setField("nome[9]", "Codice doc:");
			acroFields.setField("valore[9]", "23456789098765432345678");

			acroFields.setField("nome[10]", "Importo:");
			acroFields.setField("valore[10]", "1.345.456,55");

			acroFields.setField("nome[11]", "Totale:");
			acroFields.setField("valore[11]", "1.345.456,55");

			acroFields
					.setField(
							"nota[0]",
							"identificativo univoco del versamento identificativo univoco "
									+ "del versamento identificativo univoco del versamento");
			acroFields.setField("num_nota", "1");

			Color color = new Color(21, 71, 122);
			acroFields.setFieldProperty("form[0].page[0].titolo[0]", "bgcolor",
					color, null);
			acroFields.setFieldProperty("form[0].page[0].titolo[1]", "bgcolor",
					color, null);
			acroFields.setFieldProperty("form[0].page[0].titolo[2]", "bgcolor",
					color, null);
			acroFields.setFieldProperty("form[0].page[0].titolo[0]",
					"textcolor", Color.WHITE, null);
			acroFields.setFieldProperty("form[0].page[0].titolo[1]",
					"textcolor", Color.WHITE, null);
			acroFields.setFieldProperty("form[0].page[0].titolo[2]",
					"textcolor", Color.WHITE, null);
			acroFields.setField("titolo[0]", "Ricevuta numero:123131312");
			acroFields.setField("titolo[1]", "Riepilogo");
			acroFields.setField("titolo[2]", "Transazione");

			acroFields.setField("info[0]", "Pagamento registrato.");
			acroFields.setField("info[1]", "Grazie.");
			acroFields.setField("info[2]", "Valido come ricevuta.");

			acroFields.setField("simbolo[0]", "�");
			acroFields.setField("simbolo[1]", "�");

			acroFields.setField("nota[1]", "Agenzia delle entrate-Riscossione");
			acroFields
					.setField(
							"nota[2]",
							"Agente della riscossione per tutti gli ambiti provinciali nazionali ad esclusione del territorio della Regione Siciliana");
			acroFields.setField("nota[3]",
					"Sede legale Via Giuseppe Grezar, 14 � 00142 Roma");
			acroFields
					.setField("nota[4]",
							"Iscritta al registro delle imprese di Roma, C. F. e P. IVA 13756881002");

			acroFields.setField("pag",
					numPag.toString() + "/" + totPag.toString());

			float[] positionLogo = acroFields.getFieldPositions("logo[0]");
			float[] positionLogo1 = acroFields.getFieldPositions("logo[1]");

			setImage(pdfStamper, positionLogo, LOGO);
			setImage(pdfStamper, positionLogo1, LOGO1);

			pdfStamper.setFormFlattening(true);
			pdfStamper.close();
			pdfReader.close();

		} catch (Exception e) {
			System.err.println("stampa -> writeFieldsNames -> Exception: "
					+ e.getMessage());
			e.printStackTrace();
		}
		System.out
				.println("stampa -> writeFieldsNames -> Operazione terminata!!!");
		return baos.toByteArray();

	}

	private static void setImage(PdfStamper pdfStamper, float[] position,
			String name) throws MalformedURLException, IOException,
			DocumentException {

		Rectangle rectangleQRCode = new Rectangle(position[1], position[2],
				position[3], position[4]);
		Map<EncodeHintType, ErrorCorrectionLevel> hintMap = new HashMap<EncodeHintType, ErrorCorrectionLevel>();
		hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
		Image imageQRCode = Image.getInstance(name);
		imageQRCode.scaleToFit(rectangleQRCode.getWidth(),
				rectangleQRCode.getHeight());
		imageQRCode.setAbsolutePosition(
				position[1]
						+ (rectangleQRCode.getWidth() - imageQRCode
								.getScaledWidth()) / 2,
				position[2]
						+ (rectangleQRCode.getHeight() - imageQRCode
								.getScaledHeight()) / 2);

		PdfContentByte pdfContentByte = pdfStamper
				.getOverContent((int) position[0]);
		pdfContentByte = pdfStamper.getOverContent((int) position[0]);
		pdfContentByte.addImage(imageQRCode);

	}

}
