package it.sogei.pdf;

import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;

import com.lowagie.text.pdf.AcroFields;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamper;

public class BollettinoDownload {

	private static final Logger logger = Logger.getLogger(ADMPayDownload.class);

	public static final String TEMPLATE = "/doganeprj/pagopa/template/bollettino.pdf";

	public static String dest = "";

	private static String codice = "0011 1111 6000 0015 78 ";
	private static String codiceJoined = "";

	public static void main(String[] args) throws Exception {
		stampaAvvisoAnalogico();
	}

	private static void stampaAvvisoAnalogico() {
		// readFieldsNames();
		writeFieldsNames();

	}

	private static void writeFieldsNames() {
		System.out
				.println("stampaADMPay -> writeFieldsNames -> Inizio operazione!!!");
		try {
			PdfReader pdfReader = new PdfReader(TEMPLATE);

			String identify = new SimpleDateFormat("yyyyMMddhhmmss")
					.format(new Date());
			dest = TEMPLATE.replace("bollettino", "bollettino_" + identify);

			PdfStamper pdfStamper = new PdfStamper(pdfReader,
					new FileOutputStream(dest));
			AcroFields acroFields = pdfStamper.getAcroFields();

			acroFields.setField("TextField1", "1234567890");
			acroFields.setField("TextField13", "1234567890");
			acroFields.setField("TextField2", "1234");
			acroFields.setField("TextField14", "1234");
			acroFields.setField("TextField3", "56");
			acroFields.setField("TextField15", "56");
			acroFields.setField("TextField25", "\t milleduecentotrentaquattro");
			acroFields.setField("TextField26", "\t milleduecentotrentaquattro");
			acroFields.setField("TextField4[0]", "Comune di Roma");
			acroFields.setField("TextField16[0]", "\t Comune di Roma");

			acroFields.setField("TextField4[1]",
					"\t \t \t \t Servizio di Tesoreria ");
			acroFields.setField("TextField16[1]", "\t Servizio di Tesoreria ");

			acroFields.setField("TextField6[1]", "Pagamento per servizio");
			acroFields.setField("TextField18[0]", "Pagamento per servizio ");

			acroFields.setField("TextField6[0]",
					"\t \t \t \t Manutenzione fabbricato");
			acroFields.setField("TextField18[1]", "Manutenzione fabbricato");

			acroFields.setField("TextField8[1]", "\t Mario");
			acroFields.setField("TextField20[1]", "\t Mario ");
			acroFields.setField("TextField8[0]", "\t Rossi");
			acroFields.setField("TextField20[0]", "\t Rossi");

			acroFields.setField("TextField10", "Via Roma, 22");
			acroFields.setField("TextField28", "\t Via Roma, 22");
			acroFields.setField("TextField11", "00155");
			acroFields.setField("TextField23", "\t 00155");
			acroFields.setField("TextField12", "Milano");
			acroFields.setField("TextField24", "\t Milano");

			pdfStamper.setFormFlattening(true);
			pdfStamper.close();
			pdfReader.close();
		} catch (Exception e) {
			System.err
					.println("stampaADMPay -> writeFieldsNames -> Exception: "
							+ e.getMessage());
			e.printStackTrace();
		}
		System.out
				.println("stampaADMPay -> writeFieldsNames -> Operazione terminata!!!");

	}
}
