package it.sogei.pdf;

import it.sogei.pagopa.monitor.model.Elemento;

import java.awt.Color;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.lowagie.text.pdf.AcroFields;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamper;

public class TablePdf {

	public static final String TEMPLATE = "/doganeprj/pagopa/template1/table.pdf";

	public static String dest = "";

	public static void main(String[] args) throws Exception {
		stampaAvvisoAnalogico();
	}

	private static void stampaAvvisoAnalogico() {
		Elemento elemento = new Elemento("Mario Rossi", new Date(),
				"descrizione");
		elemento.setId(123L);
		List<Elemento> lista = new ArrayList<>();

		for (int i = 0; i < 10; i++) {
			lista.add(elemento);
		}

		writeFieldsNames(lista);
	}

	private static void writeFieldsNames(List<Elemento> lista) {
		System.out
				.println("stampa -> writeFieldsNames -> Inizio operazione!!!");

		try {

			PdfReader pdfReader = new PdfReader(TEMPLATE);

			String identify = new SimpleDateFormat("yyyyMMddhhmmss")
					.format(new Date());
			dest = TEMPLATE.replace("table", "table_" + identify);

			PdfStamper pdfStamper = new PdfStamper(pdfReader,
					new FileOutputStream(dest));
			AcroFields acroFields = pdfStamper.getAcroFields();

			acroFields.setFieldProperty("form[0].subform[0].header0[0]",
					"bordercolor", Color.BLACK, null);
			acroFields.setFieldProperty("form[0].subform[0].header1[0]",
					"bordercolor", Color.BLACK, null);
			acroFields.setFieldProperty("form[0].subform[0].header2[0]",
					"bordercolor", Color.BLACK, null);
			acroFields.setFieldProperty("form[0].subform[0].header3[0]",
					"bordercolor", Color.BLACK, null);

			acroFields.setField("header0[0]", "Id");
			acroFields.setField("header1[0]", "Nome");
			acroFields.setField("header2[0]", "Data");
			acroFields.setField("header3[0]", "Descrizione");

			for (int i = 0; i < lista.size(); i++) {
				Integer j = 1;
				acroFields
						.setFieldProperty(
								"form[0].subform[0].col" + j.toString() + "["
										+ i + "]", "bordercolor", Color.BLACK,
								null);
				acroFields.setField("form[0].subform[0].col" + j + "[" + i
						+ "]", lista.get(i).getId().toString());

				j++;
				acroFields
						.setFieldProperty(
								"form[0].subform[0].col" + j.toString() + "["
										+ i + "]", "bordercolor", Color.BLACK,
								null);
				acroFields.setField("form[0].subform[0].col" + j + "[" + i
						+ "]", lista.get(i).getNome());

				j++;
				acroFields
						.setFieldProperty(
								"form[0].subform[0].col" + j.toString() + "["
										+ i + "]", "bordercolor", Color.BLACK,
								null);
				acroFields.setField("form[0].subform[0].col" + j + "[" + i
						+ "]", lista.get(i).getData().toString());

				j++;
				acroFields
						.setFieldProperty(
								"form[0].subform[0].col" + j.toString() + "["
										+ i + "]", "bordercolor", Color.BLACK,
								null);
				acroFields.setField("form[0].subform[0].col" + j + "[" + i
						+ "]", lista.get(i).getDescrizione());

			}

			pdfStamper.setFormFlattening(true);
			pdfStamper.close();
			pdfReader.close();

		} catch (Exception e) {
			System.err.println("stampa -> writeFieldsNames -> Exception: "
					+ e.getMessage());
			e.printStackTrace();
		}
		System.out
				.println("stampa -> writeFieldsNames -> Operazione terminata!!!");
	}
}
