package it.sogei.pdf;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.google.zxing.EncodeHintType;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Image;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.AcroFields;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamper;

public class ADMPayDownload {

	private static final Logger logger = Logger.getLogger(ADMPayDownload.class);

	public static final String TEMPLATE = "/doganeprj/pagopa/template/templateADMPay.pdf";
	public static String dest = "";

	// Il logo dell'ente, se sviluppato in orizzontale, deve essere appoggiato
	// all'angolo superiore destro dell'area prevista.
	// il logo deve essere vettoriale, monocromatico e su sfondo bianco.
	public static final String LOGO_ENTE = "/doganeprj/pagopa/template/LogoADM.jpg";
	public static final String LOGO = "/doganeprj/pagopa/template/logo.png";

	// Codice avviso di 23 caratteri compresi gli spazi - le cifre sono
	// raggruppate a gruppi di 4 separate da uno spazio
	private static String codice = "0011 1111 6000 0015 78 ";
	private static String codiceJoined = "";

	public static final String singoloImportoPagato = "1.500.500,50";
	public static final String riferimentoDataRichiesta = "20/12/2030";
	public static final String identificativoDominio = "AAABBB10X10X111D";
	public static final String codiceContestoPagamento = "0011 1111 6000 0015 78";
	public static final String identificativoUnivocoVersamento = "AAABBB10X10X111D";
	public static final String codiceIdentificativoUnivoco = "0011 1111 6000 0015 78";
	public static final String identificativoUnivocoRiscossione = "AAABBB10X10X111D";
	public static final String dataOperazione = "20/12/2030";

	public static final String cell1 = "No.233";

	public static void main(String[] args) throws Exception {
		stampaAvvisoAnalogico();
	}

	private static void stampaAvvisoAnalogico() {
		// readFieldsNames();
		writeFieldsNames();

	}

	private static void writeFieldsNames() {
		System.out
				.println("stampaADMPay -> writeFieldsNames -> Inizio operazione!!!");
		try {
			PdfReader pdfReader = new PdfReader(TEMPLATE);

			String identify = codice + "_"
					+ new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
			dest = TEMPLATE.replace("templateADMPay", identify + "_");

			PdfStamper pdfStamper = new PdfStamper(pdfReader,
					new FileOutputStream(dest));
			AcroFields acroFields = pdfStamper.getAcroFields();

			acroFields.setField("singoloImportoPagato", singoloImportoPagato);
			acroFields.setField("riferimentoDataRichiesta",
					riferimentoDataRichiesta);
			acroFields.setField("identificativoDominio", identificativoDominio);
			acroFields.setField("codiceContestoPagamento",
					codiceContestoPagamento);
			acroFields.setField("identificativoUnivocoVersamento",
					identificativoUnivocoVersamento);
			acroFields.setField("codiceIdentificativoUnivoco",
					codiceIdentificativoUnivoco);
			acroFields.setField("identificativoUnivocoRiscossione",
					identificativoUnivocoRiscossione);
			acroFields.setField("dataOperazione", dataOperazione);

			acroFields.setField("HeaderRow.Cell1", "Num.");
			acroFields.setField("HeaderRow.Cell2", "Beneficiario");
			acroFields.setField("HeaderRow.Cell3", "Importo");
			acroFields.setField("HeaderRow.Cell4", "Data");

			acroFields.setField("Row1.Cell1", cell1);
			acroFields.setField("Row1.Cell2", "Mario Rossi");
			acroFields.setField("Row1.Cell3", "�2.534.234,22");
			acroFields.setField("Row1.Cell4", "RM 20/12/2030");
			acroFields.setField("Row2.Cell1", "---");
			acroFields.setField("Row2.Causale", "Pagamento per servizio");

			acroFields.setField("Row3.Cell1", cell1);
			acroFields.setField("Row3.Cell2", "Mario Rossi");
			acroFields.setField("Row3.Cell3", "�2.534.234,22");
			acroFields.setField("Row3.Cell4", "RM 20/12/2030");
			acroFields.setField("Row4.Cell1", "---");
			acroFields.setField("Row4.Causale", "Pagamento per servizio");

			acroFields.setField("Row5.Cell1", cell1);
			acroFields.setField("Row5.Cell2", "Mario Rossi");
			acroFields.setField("Row5.Cell3", "�2.534.234,22");
			acroFields.setField("Row5.Cell4", "RM 20/12/2030");
			acroFields.setField("Row6.Cell1", "---");
			acroFields.setField("Row6.Causale", "Pagamento per servizio");

			acroFields.setField("Row7.Cell1", cell1);
			acroFields.setField("Row7.Cell2", "Mario Rossi");
			acroFields.setField("Row7.Cell3", "�2.534.234,22");
			acroFields.setField("Row7.Cell4", "RM 20/12/2030");
			acroFields.setField("Row8.Cell1", "---");
			acroFields.setField("Row8.Causale", "Pagamento per servizio");

			acroFields.setField("Row9.Cell1", cell1);
			acroFields.setField("Row9.Cell2", "Mario Rossi");
			acroFields.setField("Row9.Cell3", "�2.534.234,22");
			acroFields.setField("Row9.Cell4", "RM 20/12/2030");
			acroFields.setField("Row10.Cell1", "---");
			acroFields.setField("Row10.Causale", "Pagamento per servizio");

			String charset = "UTF-8";
			float[] positionLogo = acroFields.getFieldPositions("logo");
			float[] positionLogoEnte = acroFields
					.getFieldPositions("logo_ente");

			setImage(pdfStamper, positionLogoEnte, LOGO_ENTE);
			setImage(pdfStamper, positionLogo, LOGO);

			pdfStamper.setFormFlattening(true);
			pdfStamper.close();
			pdfReader.close();
		} catch (Exception e) {
			System.err
					.println("stampaADMPay -> writeFieldsNames -> Exception: "
							+ e.getMessage());
			e.printStackTrace();
		}
		System.out
				.println("stampaADMPay -> writeFieldsNames -> Operazione terminata!!!");

	}

	private static void setImage(PdfStamper pdfStamper, float[] position,
			String name) throws MalformedURLException, IOException,
			DocumentException {

		Rectangle rectangleQRCode = new Rectangle(position[1], position[2],
				position[3], position[4]);
		Map<EncodeHintType, ErrorCorrectionLevel> hintMap = new HashMap<EncodeHintType, ErrorCorrectionLevel>();
		hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
		Image imageQRCode = Image.getInstance(name);
		imageQRCode.scaleToFit(rectangleQRCode.getWidth(),
				rectangleQRCode.getHeight());
		imageQRCode.setAbsolutePosition(
				position[1]
						+ (rectangleQRCode.getWidth() - imageQRCode
								.getScaledWidth()) / 2,
				position[2]
						+ (rectangleQRCode.getHeight() - imageQRCode
								.getScaledHeight()) / 2);

		PdfContentByte pdfContentByte = pdfStamper
				.getOverContent((int) position[0]);
		pdfContentByte = pdfStamper.getOverContent((int) position[0]);
		pdfContentByte.addImage(imageQRCode);

	}

}
