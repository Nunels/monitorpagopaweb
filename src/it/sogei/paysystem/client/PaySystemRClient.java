package it.sogei.paysystem.client;

import it.sogei.data.DataUtilFactory;
import it.sogei.ssi.util.AppConf;
import it.sogei.util.rest.client.ClientRest;
import it.sogei.util.rest.client.Response;
import it.sogei.util.rest.client.impl.ClientHttpConnectionUrlRest;

import java.net.MalformedURLException;
import java.util.Hashtable;

public class PaySystemRClient {

	private String hostService;
	// private static String pathService =
	// "SogeiPaySystemMonitorWeb/services/PaySystemMonitorService";
	private static String sslContextProtocol = "TLS";

	// private TLSSocketFactory ssl = null;

	public final static String POST_METHOD = "POST";

	public ClientRest client = null;

	public enum MediaType {
		json("application/json"), xml("application/xml");

		private String mediaType = null;

		MediaType(String mediaType) {
			this.mediaType = mediaType;
		}

		public String value() {
			return this.mediaType;
		}

	}

	private String mediaType = null;
	private Hashtable<String, String> header;

	//
	public PaySystemRClient(String pathService) {
		hostService = AppConf.getConfig().getHostService();
		try {
			client = new ClientHttpConnectionUrlRest(hostService, pathService);
		} catch (MalformedURLException e1) {
			e1.printStackTrace();
		}
	}

	public PaySystemRClient(String contentType, String pathService) {

		hostService = AppConf.getConfig().getHostService();

		this.mediaType = contentType;
		try {
			client = new ClientHttpConnectionUrlRest(hostService, pathService);
		} catch (MalformedURLException e1) {
			e1.printStackTrace();
		}
	}

	public PaySystemRClient(String contentType, String pathService,
			Hashtable<String, String> header) {
		this.header = header;
		hostService = AppConf.getConfig().getHostService();

		this.mediaType = contentType;
		try {
			client = new ClientHttpConnectionUrlRest(hostService, pathService);
		} catch (MalformedURLException e1) {
			e1.printStackTrace();
		}
	}

	public PaySystemRClient(String contentType, Hashtable<String, String> header) {
		this.header = header;
		hostService = AppConf.getConfig().getHostService();

		this.mediaType = contentType;
		try {
			client = new ClientHttpConnectionUrlRest("http://localhost:9080",
					"DBService/services/DBRService");
		} catch (MalformedURLException e1) {
			e1.printStackTrace();
		}
	}

	public PaySystemRClient(String contentType,
			Hashtable<String, String> header, int num) {
		this.header = header;
		hostService = AppConf.getConfig().getHostService();

		this.mediaType = contentType;
		try {

			client = new ClientHttpConnectionUrlRest("http://localhost:9080",
					"DbWeb/services/DbRService");

		} catch (MalformedURLException e1) {
			e1.printStackTrace();
		}
	}

	public <T> Response restGlobalCall(String contentType, String language,
			T input, String nomeServizio) throws Exception {

		Response response = null;

		try {

			String inputString = input != null ? trasform(input) : "";

			Hashtable<String, String> headerParams = new Hashtable<String, String>();
			headerParams.put("Accept-Language", language);
			headerParams.put("Content-Type", contentType);

			if (this.header != null) {
				headerParams.putAll(header);
			}

			response = client.post(nomeServizio, contentType, headerParams,
					inputString, contentType);

			checkResponse(response);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return response;
	}

	//
	// public <T> javax.ws.rs.core.Response _restGlobalCall(String pathService,
	// String contentType, String language, T input, String nomeServizio)
	// throws Exception {
	//
	// javax.ws.rs.core.Response response = null;
	//
	// try {
	//
	// String inputString = input != null ? trasform(input) : "";
	//
	// Hashtable<String, String> headerParams = new Hashtable<String, String>();
	// headerParams.put("Accept-Language", language);
	// headerParams.put("Content-Type", contentType);
	//
	// if (this.header != null) {
	// headerParams.putAll(header);
	// }
	// ClientHttpConnectionUrlRest clientR = new ClientHttpConnectionUrlRest(
	// "http://localhost:9080",
	// "ElementiTest/services/TestElementiRService");
	// // response = client.post(nomeServizio, contentType, headerParams,
	// // inputString, contentType);
	// ClientHttpConnectionUrlRest c = new ClientHttpConnectionUrlRest(
	// "http://localhost:9080",
	// "ElementiTest/services/TestElementiRService");
	// HttpURLConnection connection = c.generateConnection(POST_METHOD,
	// nomeServizio, contentType, headerParams, inputString,
	// contentType);
	// // checkResponse(response);
	//
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	//
	// return response;
	// }

	//

	private <T> void check(T object) throws Exception {
		if (object == null) {
			throw new Exception("Errore object null");
		}
	}

	private void checkResponse(Response response) throws Exception {
		if (response == null) {
			throw new Exception("Errore impossibile interpretare la response");
		}

		if (response.getStatus() != 200 && response.getStatus() < 500
				&& response.getStatus() != 400) {
			throw new Exception(
					"Errore nella chiamata al servizio, codice di ritorno : "
							+ response.getStatus());
		}

	}

	private <T> String trasform(T object) throws Exception {

		String requestString = null;

		if (mediaType.equals(MediaType.json.value()))
			requestString = new String(
					DataUtilFactory.trasformObjectToJsonString(object));
		else if (mediaType.equals(MediaType.xml.value())) {
			requestString = new String(
					DataUtilFactory.trasformObjectToXmlString(object));
		} else {
			throw new Exception("Errore MediaType non consentito");
		}

		return requestString;
	}

	public <T> T populate(T object, byte[] bytes) throws Exception {

		T result = null;

		if (mediaType.equals(MediaType.json.value()))
			result = DataUtilFactory.populateObjectFromJsonWithRootNode(object,
					bytes);
		else if (mediaType.equals(MediaType.xml.value())) {
			result = DataUtilFactory.populateObjectFromXml(object, bytes);
		} else {
			throw new Exception("Errore MediaType non consentito");
		}

		check(result);

		return result;
	}

	/*
	 * public static void main(String[] args){
	 * 
	 * 
	 * String certPath =
	 * "C:\\Users\\vnicoletti\\Desktop\\n\\DOG-SA-0108_VAL.pfx"; String certPwd
	 * = "KXG6NlDF"; String certType = "PKCS12";
	 * 
	 * String contentType = "application/json"; String acceptLanguage = "it";
	 * 
	 * 
	 * 
	 * FlussoRendicontazione request = new FlussoRendicontazione();
	 * FiltroFlussoRendicontazione filtroFlussoRendicontazione=new
	 * FiltroFlussoRendicontazione(); Flusso flusso=new Flusso();
	 * Rendicontazione rendicontazione=new Rendicontazione();
	 * rendicontazione.setFlusso(flusso);
	 * filtroFlussoRendicontazione.setRendicontazione(rendicontazione);
	 * filtroFlussoRendicontazione.setDa(1);
	 * filtroFlussoRendicontazione.setA(25);
	 * request.setFiltroFlussoRendicontazione(filtroFlussoRendicontazione);
	 * 
	 * 
	 * PaySystemRClient c=new PaySystemRClient(contentType); Response response =
	 * null; try {
	 * 
	 * response =
	 * c.restGlobalCall(contentType,acceptLanguage,request,"getRendicontati");
	 * 
	 * request=c.populate(request, response.getBodyBytes());
	 * 
	 * } catch (Exception e) { // TODO Blocco catch generato automaticamente
	 * e.printStackTrace(); } }
	 */

	/*
	 * public NpadRClient(String hostService, String pathService, String
	 * sslContextProtocol, String certPath, String certPwd, String
	 * certType,MediaType mediaType) {
	 * 
	 * this.mediaType = mediaType.value();
	 * 
	 * KeyStore keyStore;
	 * 
	 * KeyManagerFactory keyManagerFactory = null; try { keyStore =
	 * KeyStore.getInstance(certType);
	 * 
	 * 
	 * keyStore.load(new FileInputStream(new File(certPath)),
	 * certPwd.toCharArray());
	 * 
	 * keyManagerFactory =
	 * KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
	 * keyManagerFactory.init(keyStore, certPwd.toCharArray());
	 * 
	 * 
	 * 
	 * ssl = new
	 * TLSSocketFactory(sslContextProtocol,keyManagerFactory.getKeyManagers(),
	 * null) ;
	 * 
	 * 
	 * 
	 * } catch (KeyStoreException | NoSuchAlgorithmException e1) { // TODO
	 * Auto-generated catch block e1.printStackTrace(); } catch
	 * (CertificateException e) { // TODO Auto-generated catch block
	 * e.printStackTrace(); } catch (FileNotFoundException e) { // TODO
	 * Auto-generated catch block e.printStackTrace(); } catch (IOException e) {
	 * // TODO Auto-generated catch block e.printStackTrace(); } catch
	 * (UnrecoverableKeyException e) { // TODO Auto-generated catch block
	 * e.printStackTrace(); } catch (KeyManagementException e) { // TODO
	 * Auto-generated catch block e.printStackTrace(); }
	 * 
	 * 
	 * try {
	 * 
	 * client = ClientRestFactory.getClient(ClientRestType.NATIVE, hostService,
	 * pathService,ssl); } catch (MalformedURLException e) { // TODO
	 * Auto-generated catch block e.printStackTrace(); } }
	 */

}
