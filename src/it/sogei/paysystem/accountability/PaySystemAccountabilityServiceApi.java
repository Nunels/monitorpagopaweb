package it.sogei.paysystem.accountability;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import it.sogei.pay.system.model.external.FlussoData;
import it.sogei.pay.system.model.external.rendicontazione.FlussoRendicontazione;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
@Path("/")
@Api(value = "/", description = "")
public interface PaySystemAccountabilityServiceApi  {

    @POST
    @Path("/PaySystemAccountabilityService/getNonRendicontati")
    @Consumes({ "application/xml", "application/json" })
    @Produces({ "application/xml", "application/json" })
    @ApiOperation(value = "", tags={  })
    public FlussoData getNonRendicontati(@HeaderParam("Content-Type") String contentType, @HeaderParam("Accept-Language") String acceptLanguage, FlussoRendicontazione body);

    @POST
    @Path("/PaySystemAccountabilityService/getRendicontati")
    @Consumes({ "application/xml", "application/json" })
    @Produces({ "application/xml", "application/json" })
    @ApiOperation(value = "", tags={  })
    public FlussoRendicontazione getRendicontati(@HeaderParam("Content-Type") String contentType, @HeaderParam("Accept-Language") String acceptLanguage, FlussoRendicontazione body);

    @POST
    @Path("/PaySystemAccountabilityService/welcomeTest")
    @Consumes({ "application/xml", "application/json" })
    @Produces({ "application/xml", "application/json" })
    @ApiOperation(value = "", tags={  })
    public boolean welcomeTest(@HeaderParam("Content-Type") String contentType, @HeaderParam("Accept-Language") String acceptLanguage);

}

