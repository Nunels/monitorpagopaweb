package it.sogei.paysystem.monitor;

import java.util.List;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import it.sogei.pay.system.model.external.FlussoData;
import it.sogei.pay.system.model.external.giornale.FlussoGiornaleEventi;
import it.sogei.pay.system.model.external.giornale.GiornaleEventi;
import it.sogei.pay.system.model.external.servizi.ServiziData;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Path("/")
@Api(value = "/", description = "")
public interface PaySystemMonitorServiceApi  {

    @POST
    @Path("/PaySystemMonitorService/getFlussi")
    @Consumes({ "application/xml", "application/json" })
    @Produces({ "application/xml", "application/json" })
    @ApiOperation(value = "", tags={  })
    public  FlussoData getFlussi(@HeaderParam("Content-Type") String contentType, @HeaderParam("Accept-Language") String acceptLanguage, FlussoData body);

    @POST
    @Path("/PaySystemMonitorService/getGiornale")
    @Consumes({ "application/xml", "application/json" })
    @Produces({ "application/xml", "application/json" })
    @ApiOperation(value = "", tags={  })
    public  FlussoGiornaleEventi getGiornale(@HeaderParam("Content-Type") String contentType, @HeaderParam("Accept-Language") String acceptLanguage, FlussoGiornaleEventi body);

    @POST
    @Path("/PaySystemMonitorService/getGiornalePerPagamento")
    @Consumes({ "application/xml", "application/json" })
    @Produces({ "application/xml", "application/json" })
    @ApiOperation(value = "", tags={  })
    public  List<GiornaleEventi> getGiornalePerPagamento(@HeaderParam("Content-Type") String contentType, @HeaderParam("Accept-Language") String acceptLanguage, GiornaleEventi body);
    
    @POST
    @Path("/PaySystemMonitorService/getServizi")
    @Consumes({ "application/xml", "application/json" })
    @Produces({ "application/xml", "application/json" })
    @ApiOperation(value = "", tags={  })
    public  ServiziData getServizi(@HeaderParam("Content-Type") String contentType, @HeaderParam("Accept-Language") String acceptLanguage);
    
    @POST
    @Path("/PaySystemMonitorService/welcomeTest")
    @Consumes({ "application/xml", "application/json" })
    @Produces({ "application/xml", "application/json" })
    @ApiOperation(value = "", tags={  })
    public boolean welcomeTest(@HeaderParam("Content-Type") String contentType, @HeaderParam("Accept-Language") String acceptLanguage);
}

