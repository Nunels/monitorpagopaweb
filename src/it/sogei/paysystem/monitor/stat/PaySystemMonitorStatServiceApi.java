package it.sogei.paysystem.monitor.stat;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import it.sogei.pay.system.model.external.stat.Filtro;
import it.sogei.pay.system.model.external.stat.Result;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Path("/")
@Api(value = "/", description = "")
public interface PaySystemMonitorStatServiceApi  {

    @POST
    @Path("/PaySystemMonitorStatService/numeroTransazioniPerGiorno")
    @Consumes({ "application/xml", "application/json" })
    @Produces({ "application/xml", "application/json" })
    @ApiOperation(value = "", tags={  })
    public List<Result> numeroTransazioniPerGiorno(@HeaderParam("Content-Type") String contentType, @HeaderParam("Accept-Language") String acceptLanguage, Filtro body);

    @POST
    @Path("/PaySystemMonitorStatService/numeroTransazioniPerMinuto")
    @Consumes({ "application/xml", "application/json" })
    @Produces({ "application/xml", "application/json" })
    @ApiOperation(value = "", tags={  })
    public List<Result> numeroTransazioniPerMinuto(@HeaderParam("Content-Type") String contentType, @HeaderParam("Accept-Language") String acceptLanguage, Filtro body);

    @POST
    @Path("/PaySystemMonitorStatService/numeroTransazioniPerOra")
    @Consumes({ "application/xml", "application/json" })
    @Produces({ "application/xml", "application/json" })
    @ApiOperation(value = "", tags={  })
    public List<Result> numeroTransazioniPerOra(@HeaderParam("Content-Type") String contentType, @HeaderParam("Accept-Language") String acceptLanguage, Filtro body);

    @POST
    @Path("/PaySystemMonitorStatService/numeroTransazioniServizioPerGiorno")
    @Consumes({ "application/xml", "application/json" })
    @Produces({ "application/xml", "application/json" })
    @ApiOperation(value = "", tags={  })
    public List<Result> numeroTransazioniServizioPerGiorno(@HeaderParam("Content-Type") String contentType, @HeaderParam("Accept-Language") String acceptLanguage, Filtro body);

    @POST
    @Path("/PaySystemMonitorStatService/numeroTransazioniServizioPerMinuto")
    @Consumes({ "application/xml", "application/json" })
    @Produces({ "application/xml", "application/json" })
    @ApiOperation(value = "", tags={  })
    public List<Result> numeroTransazioniServizioPerMinuto(@HeaderParam("Content-Type") String contentType, @HeaderParam("Accept-Language") String acceptLanguage, Filtro body);

    @POST
    @Path("/PaySystemMonitorStatService/numeroTransazioniServizioPerOra")
    @Consumes({ "application/xml", "application/json" })
    @Produces({ "application/xml", "application/json" })
    @ApiOperation(value = "", tags={  })
    public List<Result> numeroTransazioniServizioPerOra(@HeaderParam("Content-Type") String contentType, @HeaderParam("Accept-Language") String acceptLanguage, Filtro body);

    @POST
    @Path("/PaySystemMonitorStatService/totaleGlobale")
    @Consumes({ "application/xml", "application/json" })
    @Produces({ "application/xml", "application/json" })
    @ApiOperation(value = "", tags={  })
    public List<Result> totaleGlobale(@HeaderParam("Content-Type") String contentType, @HeaderParam("Accept-Language") String acceptLanguage, Filtro body);

    @POST
    @Path("/PaySystemMonitorStatService/totaleGlobaleAtteso")
    @Consumes({ "application/xml", "application/json" })
    @Produces({ "application/xml", "application/json" })
    @ApiOperation(value = "", tags={  })
    public List<Result> totaleGlobaleAtteso(@HeaderParam("Content-Type") String contentType, @HeaderParam("Accept-Language") String acceptLanguage, Filtro body);

    @POST
    @Path("/PaySystemMonitorStatService/totalePerAnno")
    @Consumes({ "application/xml", "application/json" })
    @Produces({ "application/xml", "application/json" })
    @ApiOperation(value = "", tags={  })
    public List<Result> totalePerAnno(@HeaderParam("Content-Type") String contentType, @HeaderParam("Accept-Language") String acceptLanguage, Filtro body);

    @POST
    @Path("/PaySystemMonitorStatService/totalePerGiorno")
    @Consumes({ "application/xml", "application/json" })
    @Produces({ "application/xml", "application/json" })
    @ApiOperation(value = "", tags={  })
    public List<Result> totalePerGiorno(@HeaderParam("Content-Type") String contentType, @HeaderParam("Accept-Language") String acceptLanguage, Filtro body);

    @POST
    @Path("/PaySystemMonitorStatService/totalePerMese")
    @Consumes({ "application/xml", "application/json" })
    @Produces({ "application/xml", "application/json" })
    @ApiOperation(value = "", tags={  })
    public List<Result> totalePerMese(@HeaderParam("Content-Type") String contentType, @HeaderParam("Accept-Language") String acceptLanguage, Filtro body);

    @POST
    @Path("/PaySystemMonitorStatService/totalePerServizio")
    @Consumes({ "application/xml", "application/json" })
    @Produces({ "application/xml", "application/json" })
    @ApiOperation(value = "", tags={  })
    public List<Result> totalePerServizio(@HeaderParam("Content-Type") String contentType, @HeaderParam("Accept-Language") String acceptLanguage, Filtro body);

    @POST
    @Path("/PaySystemMonitorStatService/totalePerSessioneCarrello")
    @Consumes({ "application/xml", "application/json" })
    @Produces({ "application/xml", "application/json" })
    @ApiOperation(value = "", tags={  })
    public List<Result> totalePerSessioneCarrello(@HeaderParam("Content-Type") String contentType, @HeaderParam("Accept-Language") String acceptLanguage, Filtro body);

    @POST
    @Path("/PaySystemMonitorStatService/totaleServizioPerAnno")
    @Consumes({ "application/xml", "application/json" })
    @Produces({ "application/xml", "application/json" })
    @ApiOperation(value = "", tags={  })
    public List<Result> totaleServizioPerAnno(@HeaderParam("Content-Type") String contentType, @HeaderParam("Accept-Language") String acceptLanguage, Filtro body);

    @POST
    @Path("/PaySystemMonitorStatService/totaleServizioPerGiorno")
    @Consumes({ "application/xml", "application/json" })
    @Produces({ "application/xml", "application/json" })
    @ApiOperation(value = "", tags={  })
    public List<Result> totaleServizioPerGiorno(@HeaderParam("Content-Type") String contentType, @HeaderParam("Accept-Language") String acceptLanguage, Filtro body);

    @POST
    @Path("/PaySystemMonitorStatService/totaleServizioPerMese")
    @Consumes({ "application/xml", "application/json" })
    @Produces({ "application/xml", "application/json" })
    @ApiOperation(value = "", tags={  })
    public List<Result> totaleServizioPerMese(@HeaderParam("Content-Type") String contentType, @HeaderParam("Accept-Language") String acceptLanguage, Filtro body);

    
    @POST
    @Path("/PaySystemMonitorStatRService/welcomeTest")
    @Consumes({ "application/xml", "application/json" })
    @Produces({ "application/xml", "application/json" })
    @ApiOperation(value = "", tags={  })
    public boolean welcomeTest(@HeaderParam("Content-Type") String contentType, @HeaderParam("Accept-Language") String acceptLanguage);
}

