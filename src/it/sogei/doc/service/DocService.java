package it.sogei.doc.service;

import it.sogei.doc.api.PaySystemDocumentRServiceApiTest;
import it.sogei.doc.model.Ricevuta;
import it.sogei.doc.model.RicevutaDto;
import it.sogei.doc.model.SpsRichiesta;
import it.sogei.pagopa.monitor.service.TestService;
import it.sogei.pay.system.model.external.doc.AvvisoPagamentoDoc;
import it.sogei.pay.system.model.external.doc.Richiesta;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.apache.log4j.Logger;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

@ManagedBean
@SessionScoped
public class DocService implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static Logger log = Logger.getLogger(TestService.class);

	private static final String contentTypeJson = "application/json";
	private static final String acceptLanguage = "it";

	private String codContesto = "DOPCA201607110000000000000000000045";
	private String iuv = "RF5000000000000000000045";
	private String idDominio = "97210890584";
	private AvvisoPagamentoDoc avvisoPagamentoDoc;
	private AvvisoPagamentoDoc avvisoPagamentoDoc_;
	private SpsRichiesta richiestaSelezionata = new SpsRichiesta();

	private List<SpsRichiesta> richiesteSelezionate = new ArrayList<>();
	private List<Ricevuta> ricevuteSelezionate = new ArrayList<>();

	private List<SpsRichiesta> listaRichieste = new ArrayList<>();

	private PaySystemDocumentRServiceApiTest api = new PaySystemDocumentRServiceApiTest();

	private StreamedContent file;
	private StreamedContent file_;
	private StreamedContent file__;
	InputStream stream;
	InputStream stream_;
	InputStream stream__;

	@PostConstruct
	private void init() {
		if (api.welcomeTest(contentTypeJson, acceptLanguage)) {
			listaRichieste = api.getRichieste(contentTypeJson, acceptLanguage);
		}
	}

	public void download() {
		avvisoPagamentoDoc = new AvvisoPagamentoDoc();
		Richiesta richiesta = new Richiesta();
		richiesta.setCodCcontesto(codContesto);
		richiesta.setIdDominio(idDominio);
		richiesta.setIuv(iuv);
		avvisoPagamentoDoc.setRichiesta(richiesta);

		avvisoPagamentoDoc = api.recuperaAvvisoPagamento(contentTypeJson,
				avvisoPagamentoDoc, acceptLanguage);

		codContesto = null;
		idDominio = null;
		iuv = null;

	}

	public void downloadSelected() {
		avvisoPagamentoDoc_ = new AvvisoPagamentoDoc();
		Richiesta richiesta = new Richiesta();
		richiesta.setCodCcontesto(richiestaSelezionata.getCodContesto());
		richiesta.setIdDominio(richiestaSelezionata.getIdDominio());
		richiesta.setIuv(richiestaSelezionata.getIuv());
		avvisoPagamentoDoc_.setRichiesta(richiesta);
		avvisoPagamentoDoc_ = api.recuperaAvvisoPagamento(contentTypeJson,
				avvisoPagamentoDoc_, acceptLanguage);

		richiestaSelezionata = new SpsRichiesta();
		codContesto = null;
		idDominio = null;
		iuv = null;
	}

	public RicevutaDto download_() {
		RicevutaDto dto = new RicevutaDto();
		List<Ricevuta> lista = new ArrayList<>();

		for (int i = 0; i < richiesteSelezionate.size(); i++) {
			System.out.println("down"
					+ richiesteSelezionate.get(i).getCodContesto());
			Ricevuta ricevuta = new Ricevuta();
			SpsRichiesta richiesta = richiesteSelezionate.get(i);
			ricevuta.setCodContesto(richiesta.getCodContesto());
			ricevuta.setIdDominio(richiesta.getIdDominio());
			ricevuta.setIuv(richiesta.getIuv());
			lista.add(ricevuta);
		}
		dto.setListaRicevute(lista);

		dto = api.getPdfRicevute(contentTypeJson, acceptLanguage, dto);

		// richiesteSelezionate = new ArrayList<>();

		return dto;
	}

	public String getCodContesto() {
		return codContesto;
	}

	public void setCodContesto(String codContesto) {
		this.codContesto = codContesto;
	}

	public String getIuv() {
		return iuv;
	}

	public void setIuv(String iuv) {
		this.iuv = iuv;
	}

	public String getIdDominio() {
		return idDominio;
	}

	public void setIdDominio(String idDominio) {
		this.idDominio = idDominio;
	}

	public AvvisoPagamentoDoc getAvvisoPagamentoDoc() {
		return avvisoPagamentoDoc;
	}

	public void setAvvisoPagamentoDoc(AvvisoPagamentoDoc avvisoPagamentoDoc) {
		this.avvisoPagamentoDoc = avvisoPagamentoDoc;
	}

	public StreamedContent getFile() {
		download();

		stream = new ByteArrayInputStream(avvisoPagamentoDoc
				.getAvvisoPagamento().getPdfDoc());
		file = new DefaultStreamedContent(stream, "application/pdf",
				avvisoPagamentoDoc.getAvvisoPagamento().getNomeFile() + ".pdf");
		System.out.println(avvisoPagamentoDoc.getAvvisoPagamento().getPdfDoc());
		System.out.println("Tentativo download");
		return file;
	}

	public void setFile(StreamedContent file) {
		this.file = file;
	}

	public List<SpsRichiesta> getListaRichieste() {
		return listaRichieste;
	}

	public void setListaRichieste(List<SpsRichiesta> listaRichieste) {
		this.listaRichieste = listaRichieste;
	}

	public SpsRichiesta getRichiestaSelezionata() {
		return richiestaSelezionata;
	}

	public void setRichiestaSelezionata(SpsRichiesta richiestaSelezionata) {
		this.richiestaSelezionata = richiestaSelezionata;
	}

	public StreamedContent getFile_() {
		downloadSelected();

		stream_ = new ByteArrayInputStream(avvisoPagamentoDoc_
				.getAvvisoPagamento().getPdfDoc());
		file_ = new DefaultStreamedContent(stream_, "application/pdf",
				avvisoPagamentoDoc_.getAvvisoPagamento().getNomeFile() + ".pdf");

		return file_;
	}

	public void setFile_(StreamedContent file_) {
		this.file_ = file_;
	}

	public AvvisoPagamentoDoc getAvvisoPagamentoDoc_() {
		return avvisoPagamentoDoc_;
	}

	public void setAvvisoPagamentoDoc_(AvvisoPagamentoDoc avvisoPagamentoDoc_) {
		this.avvisoPagamentoDoc_ = avvisoPagamentoDoc_;
	}

	public List<Ricevuta> getRicevuteSelezionate() {
		return ricevuteSelezionate;
	}

	public void setRicevuteSelezionate(List<Ricevuta> ricevuteSelezionate) {
		this.ricevuteSelezionate = ricevuteSelezionate;
	}

	public List<SpsRichiesta> getRichiesteSelezionate() {
		return richiesteSelezionate;
	}

	public void setRichiesteSelezionate(List<SpsRichiesta> richiesteSelezionate) {
		this.richiesteSelezionate = richiesteSelezionate;
	}

	public StreamedContent getFile__() {
		if (listaRichieste.size() == 0) {
			System.exit(0);

		}
		RicevutaDto dto = download_();

		stream__ = new ByteArrayInputStream(dto.getPdf());
		file__ = new DefaultStreamedContent(stream__, "application/pdf",
				"ricevute" + ".pdf");

		return file__;
	}

	public void setFile__(StreamedContent file__) {
		this.file__ = file__;
	}

}
