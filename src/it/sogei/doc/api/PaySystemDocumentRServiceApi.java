package it.sogei.doc.api;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import it.sogei.doc.model.RicevutaDto;
import it.sogei.doc.model.SpsRichiesta;
import it.sogei.pay.system.model.external.doc.AvvisoPagamentoDoc;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Path("/")
@Api(value = "/", description = "")
public interface PaySystemDocumentRServiceApi {

	@POST
	@Path("/DbRService/recuperaAvvisoPagamento")
	@Consumes({ "application/xml", "application/json" })
	@Produces({ "application/xml", "application/json" })
	@ApiOperation(value = "", tags = {})
	public AvvisoPagamentoDoc recuperaAvvisoPagamento(
			@HeaderParam("Content-Type") String contentType,
			AvvisoPagamentoDoc body,
			@HeaderParam("Accept-Language") String acceptLanguage);

	@POST
	@Path("/DbRService/welcomeTest")
	@Consumes({ "application/xml", "application/json" })
	@Produces({ "application/xml", "application/json" })
	@ApiOperation(value = "", tags = {})
	public boolean welcomeTest(@HeaderParam("Content-Type") String contentType,
			@HeaderParam("Accept-Language") String acceptLanguage);

	@POST
	@Path("/DbRService/getRichieste")
	@Consumes({ "application/xml", "application/json" })
	@Produces({ "application/xml", "application/json" })
	@ApiOperation(value = "", tags = {})
	public List<SpsRichiesta> getRichieste(
			@HeaderParam("Content-Type") String contentType,
			@HeaderParam("Accept-Language") String acceptLanguage);

	@POST
	@Path("/DbRService/getPdfRicevute")
	@Consumes({ "application/xml", "application/json" })
	@Produces({ "application/xml", "application/json" })
	@ApiOperation(value = "", tags = {})
	public RicevutaDto getPdfRicevute(
			@HeaderParam("Content-Type") String contentType,
			@HeaderParam("Accept-Language") String acceptLanguage,
			RicevutaDto dto);
}
