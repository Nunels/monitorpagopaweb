package it.sogei.doc.model;

import java.util.Date;

public class Ricevuta {

	private Integer idRichiesta;
	private String idDominio;
	private String idVersante;
	private String idPagatore;
	private Date dataRichiesta;
	private Double importoTot;
	private String iuv;
	private String codContesto;
	private String stato;
	private String url;

	public Integer getIdRichiesta() {
		return idRichiesta;
	}

	public void setIdRichiesta(Integer idRichiesta) {
		this.idRichiesta = idRichiesta;
	}

	public String getIdDominio() {
		return idDominio;
	}

	public void setIdDominio(String idDominio) {
		this.idDominio = idDominio;
	}

	public String getIdVersante() {
		return idVersante;
	}

	public void setIdVersante(String idVersante) {
		this.idVersante = idVersante;
	}

	public String getIdPagatore() {
		return idPagatore;
	}

	public void setIdPagatore(String idPagatore) {
		this.idPagatore = idPagatore;
	}

	public Date getDataRichiesta() {
		return dataRichiesta;
	}

	public void setDataRichiesta(Date dataRichiesta) {
		this.dataRichiesta = dataRichiesta;
	}

	public Double getImportoTot() {
		return importoTot;
	}

	public void setImportoTot(Double importoTot) {
		this.importoTot = importoTot;
	}

	public String getIuv() {
		return iuv;
	}

	public void setIuv(String iuv) {
		this.iuv = iuv;
	}

	public String getCodContesto() {
		return codContesto;
	}

	public void setCodContesto(String codContesto) {
		this.codContesto = codContesto;
	}

	public String getStato() {
		return stato;
	}

	public void setStato(String stato) {
		this.stato = stato;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}
