package it.sogei.doc.model;

import java.util.List;

public class RicevutaDto {
	private List<Ricevuta> listaRicevute;

	public List<Ricevuta> getListaRicevute() {
		return listaRicevute;
	}

	public void setListaRicevute(List<Ricevuta> listaRicevute) {
		this.listaRicevute = listaRicevute;
	}

	private byte[] pdf;

	public byte[] getPdf() {
		return pdf;
	}

	public void setPdf(byte[] pdf) {
		this.pdf = pdf;
	}
}
