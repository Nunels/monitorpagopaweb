package it.sogei.doc.model;

import java.util.Date;

public class SpsRichiesta {

	private Integer idRichiesta;
	private String idVersante;
	private String idPagatore;
	private String idDominio;
	private Date dataRichiesta;
	private String codContesto;
	private String iuv;

	private byte[] doc;
	private String nome_file;

	public Integer getIdRichiesta() {
		return idRichiesta;
	}

	public void setIdRichiesta(Integer idRichiesta) {
		this.idRichiesta = idRichiesta;
	}

	public String getIdVersante() {
		return idVersante;
	}

	public void setIdVersante(String idVersante) {
		this.idVersante = idVersante;
	}

	public String getIdPagatore() {
		return idPagatore;
	}

	public void setIdPagatore(String idPagatore) {
		this.idPagatore = idPagatore;
	}

	public String getIdDominio() {
		return idDominio;
	}

	public void setIdDominio(String idDominio) {
		this.idDominio = idDominio;
	}

	public Date getDataRichiesta() {
		return dataRichiesta;
	}

	public void setDataRichiesta(Date dataRichiesta) {
		this.dataRichiesta = dataRichiesta;
	}

	public String getCodContesto() {
		return codContesto;
	}

	public void setCodContesto(String codContesto) {
		this.codContesto = codContesto;
	}

	public String getIuv() {
		return iuv;
	}

	public void setIuv(String iuv) {
		this.iuv = iuv;
	}

	public byte[] getDoc() {
		return doc;
	}

	public void setDoc(byte[] doc) {
		this.doc = doc;
	}

	public String getNome_file() {
		return nome_file;
	}

	public void setNome_file(String nome_file) {
		this.nome_file = nome_file;
	}

}
