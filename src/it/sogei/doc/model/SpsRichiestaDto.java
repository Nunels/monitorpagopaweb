package it.sogei.doc.model;

import java.util.List;

public class SpsRichiestaDto {

	private List<SpsRichiesta> listaRichieste;

	public List<SpsRichiesta> getListaRichieste() {
		return listaRichieste;
	}

	public void setListaRichieste(List<SpsRichiesta> listaRichieste) {
		this.listaRichieste = listaRichieste;
	}

}
