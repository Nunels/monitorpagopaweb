package it.sogei.test;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import it.sogei.pagopa.monitor.model.Elemento;
import it.sogei.pagopa.monitor.model.FiltroElemento;
import it.sogei.pagopa.monitor.model.Sottoelemento;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;

@Path("/")
@Api(value = "/", description = "")
public interface TestElementiServiceApi {

	@POST
	@Path("/DBRService/getElementi")
	@Consumes({ "application/xml", "application/json" })
	@Produces({ "application/xml", "application/json" })
	@ApiOperation(value = "", tags = {})
	public List<Elemento> getElementi(
			@HeaderParam("Content-Type") String contentType,
			@HeaderParam("Accept-Language") String acceptLanguage);

	@POST
	@Path("/DBRService/welcomeTest")
	@Consumes({ "application/xml", "application/json" })
	@Produces({ "application/xml", "application/json" })
	@ApiOperation(value = "", tags = {})
	public boolean welcomeTest(@HeaderParam("Content-Type") String contentType,
			@HeaderParam("Accept-Language") String acceptLanguage);

	@POST
	@Path("/TestElementiService/getDettagli")
	@Consumes({ "application/xml", "application/json" })
	@Produces({ "application/xml", "application/json" })
	@ApiOperation(value = "", tags = {})
	public Elemento getDettagli(
			@HeaderParam("Content-Type") String contentType,
			@HeaderParam("Accept-Language") String acceptLanguage, Elemento body);

	@POST
	@Path("TestElementiService/generaElementi")
	@Consumes({ "application/xml", "application/json" })
	@Produces({ "application/xml", "application/json" })
	@ApiOperation(value = "", tags = {})
	public List<Elemento> generaElementi(
			@HeaderParam("Content-Type") String contentType,
			@HeaderParam("Accept-Language") String acceptLanguage,
			Integer numeroElementi);

	@POST
	@Path("/DBRService/deleteElemento")
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public boolean deleteElemento(
			@HeaderParam(HttpHeaders.CONTENT_TYPE) String type,
			@HeaderParam(HttpHeaders.ACCEPT_LANGUAGE) String language,
			Elemento elemento);

	@POST
	@Path("/DBRService/updateElemento")
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public boolean updateElemento(
			@HeaderParam(HttpHeaders.CONTENT_TYPE) String type,
			@HeaderParam(HttpHeaders.ACCEPT_LANGUAGE) String language,
			Elemento elemento);

	@POST
	@Path("/DBRService/addElemento")
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public boolean addElemento(
			@HeaderParam(HttpHeaders.CONTENT_TYPE) String type,
			@HeaderParam(HttpHeaders.ACCEPT_LANGUAGE) String language,
			Elemento elemento);

	@POST
	@Path("/DBRService/getSottoelementiById")
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public List<Sottoelemento> getSottoelementiById(
			@HeaderParam(HttpHeaders.CONTENT_TYPE) String type,
			@HeaderParam(HttpHeaders.ACCEPT_LANGUAGE) String language,
			Elemento elemento);

	@POST
	@Path("/DBRService/getElementiFilter")
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public List<Elemento> getElementiFilter(
			@HeaderParam(HttpHeaders.CONTENT_TYPE) String type,
			@HeaderParam(HttpHeaders.ACCEPT_LANGUAGE) String language,
			FiltroElemento filtroElemento);
}
