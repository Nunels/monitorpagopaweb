package it.sogei.test;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Elemento", propOrder = { "id", "nome", "data" })
@XmlRootElement(name = "Elemento")
@ApiModel(value = "Elemento", description = "it.sogei.pay.system.model.external.stat.Result.")
public class Elemento implements Serializable {

	private static final long serialVersionUID = 1L;

	@XmlElement(name = "id", required = true)
	@ApiModelProperty(example = "null", required = true, value = "")
	private Long id = null;

	@XmlElement(name = "nome", required = true)
	@ApiModelProperty(example = "null", required = true, value = "")
	private String nome = null;

	@XmlElement(name = "data", required = true)
	@ApiModelProperty(example = "null", required = true, value = "")
	private javax.xml.datatype.XMLGregorianCalendar data = null;

	/**
	 * Get id
	 * 
	 * @return id
	 **/
	@ApiModelProperty("id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Elemento id(Long id) {
		this.id = id;
		return this;
	}

	/**
	 * Get nome
	 * 
	 * @return nome
	 **/
	@ApiModelProperty("nome")
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Elemento nome(String nome) {
		this.nome = nome;
		return this;
	}

	/**
	 * Get data
	 * 
	 * @return data
	 **/
	@ApiModelProperty("data")
	public javax.xml.datatype.XMLGregorianCalendar getData() {
		return data;
	}

	public void setData(javax.xml.datatype.XMLGregorianCalendar data) {
		this.data = data;
	}

	public Elemento data(javax.xml.datatype.XMLGregorianCalendar data) {
		this.data = data;
		return this;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class Elemento {\n");

		sb.append("    id: ").append(toIndentedString(id)).append("\n");
		sb.append("    nome: ").append(toIndentedString(nome)).append("\n");
		sb.append("    data: ").append(toIndentedString(data)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private static String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}
